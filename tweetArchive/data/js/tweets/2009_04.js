Grailbird.data.tweets_2009_04 = 
 [ {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "1594270091",
  "text" : "really great article about twitter and music piracy.  Ah, moods. http:\/\/is.gd\/u5rs",
  "id" : 1594270091,
  "created_at" : "2009-04-23 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Sherbow",
      "screen_name" : "MusicBizGuy",
      "indices" : [ 0, 12 ],
      "id_str" : "14638514",
      "id" : 14638514
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "1593240627",
  "geo" : { },
  "id_str" : "1594285666",
  "in_reply_to_user_id" : 14638514,
  "text" : "@MusicBizGuy  I feel like giving away music is hardly a decision. Makes me sad to see that, among ppl who paid, Radiohead's rec sold 4 $6",
  "id" : 1594285666,
  "in_reply_to_status_id" : 1593240627,
  "created_at" : "2009-04-23 00:00:00 +0000",
  "in_reply_to_screen_name" : "MusicBizGuy",
  "in_reply_to_user_id_str" : "14638514",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "1594329033",
  "text" : "Myspace to undergo ownership change: http:\/\/is.gd\/u5x3",
  "id" : 1594329033,
  "created_at" : "2009-04-23 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Matthew Griffin",
      "screen_name" : "mirificam",
      "indices" : [ 0, 10 ],
      "id_str" : "24457600",
      "id" : 24457600
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "1585523521",
  "geo" : { },
  "id_str" : "1595064029",
  "in_reply_to_user_id" : 24457600,
  "text" : "@mirificam Just came across your design blog via google. Great stuff for a learning designer!",
  "id" : 1595064029,
  "in_reply_to_status_id" : 1585523521,
  "created_at" : "2009-04-23 00:00:00 +0000",
  "in_reply_to_screen_name" : "mirificam",
  "in_reply_to_user_id_str" : "24457600",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "1575769627",
  "text" : "techdirt delivers again. Cool article about piracy: http:\/\/is.gd\/tEEk",
  "id" : 1575769627,
  "created_at" : "2009-04-21 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "1544250778",
  "text" : "Pirate Bay found guilty. Understandable, yet unfortunate. http:\/\/www.billboard.biz\/bbbiz\/index.jsp",
  "id" : 1544250778,
  "created_at" : "2009-04-17 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "1533866021",
  "text" : "bogged down at this point in semester.",
  "id" : 1533866021,
  "created_at" : "2009-04-16 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "1522191680",
  "text" : "take your monkeys and eye-patches  and scram",
  "id" : 1522191680,
  "created_at" : "2009-04-15 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kim Stolz",
      "screen_name" : "KimmyStolz",
      "indices" : [ 0, 11 ],
      "id_str" : "21706987",
      "id" : 21706987
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "1506747152",
  "in_reply_to_user_id" : 21706987,
  "text" : "@KimmyStolz I would disagree about modern accountability.  Less intimate or romantic perhaps. In an e-world accountability seems  given.",
  "id" : 1506747152,
  "created_at" : "2009-04-13 00:00:00 +0000",
  "in_reply_to_screen_name" : "KimmyStolz",
  "in_reply_to_user_id_str" : "21706987",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kim Stolz",
      "screen_name" : "KimmyStolz",
      "indices" : [ 0, 11 ],
      "id_str" : "21706987",
      "id" : 21706987
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "1506796373",
  "in_reply_to_user_id" : 21706987,
  "text" : "@KimmyStolz Also, great article!",
  "id" : 1506796373,
  "created_at" : "2009-04-13 00:00:00 +0000",
  "in_reply_to_screen_name" : "KimmyStolz",
  "in_reply_to_user_id_str" : "21706987",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gabe Alonso",
      "screen_name" : "gabealonso",
      "indices" : [ 0, 11 ],
      "id_str" : "10235762",
      "id" : 10235762
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "1506888159",
  "geo" : { },
  "id_str" : "1506994392",
  "in_reply_to_user_id" : 10235762,
  "text" : "@gabealonso What do you think the best thing young artists can do to create a sort of brand is? Your blog's great.",
  "id" : 1506994392,
  "in_reply_to_status_id" : 1506888159,
  "created_at" : "2009-04-13 00:00:00 +0000",
  "in_reply_to_screen_name" : "gabealonso",
  "in_reply_to_user_id_str" : "10235762",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "1507027418",
  "text" : "still finding footing in this twittersphere",
  "id" : 1507027418,
  "created_at" : "2009-04-13 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Artists House Music",
      "screen_name" : "artistshouse",
      "indices" : [ 0, 13 ],
      "id_str" : "16610491",
      "id" : 16610491
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "1509393423",
  "geo" : { },
  "id_str" : "1509495599",
  "in_reply_to_user_id" : 16610491,
  "text" : "@artistshouse  I find it difficult to form effective commun. How do I speak to many personally, without bothering. Not easy.",
  "id" : 1509495599,
  "in_reply_to_status_id" : 1509393423,
  "created_at" : "2009-04-13 00:00:00 +0000",
  "in_reply_to_screen_name" : "artistshouse",
  "in_reply_to_user_id_str" : "16610491",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Artists House Music",
      "screen_name" : "artistshouse",
      "indices" : [ 0, 13 ],
      "id_str" : "16610491",
      "id" : 16610491
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "1509553386",
  "geo" : { },
  "id_str" : "1509567843",
  "in_reply_to_user_id" : 16610491,
  "text" : "@artistshouse  it's just a tough balancing act with big email lists and the desire to feel authentic.  great wrap-ups!",
  "id" : 1509567843,
  "in_reply_to_status_id" : 1509553386,
  "created_at" : "2009-04-13 00:00:00 +0000",
  "in_reply_to_screen_name" : "artistshouse",
  "in_reply_to_user_id_str" : "16610491",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "sambarouge",
      "screen_name" : "sambarouge",
      "indices" : [ 0, 11 ],
      "id_str" : "15198430",
      "id" : 15198430
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "1510568050",
  "geo" : { },
  "id_str" : "1510697386",
  "in_reply_to_user_id" : 15198430,
  "text" : "@sambarouge that must be a trident on your background...",
  "id" : 1510697386,
  "in_reply_to_status_id" : 1510568050,
  "created_at" : "2009-04-13 00:00:00 +0000",
  "in_reply_to_screen_name" : "sambarouge",
  "in_reply_to_user_id_str" : "15198430",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "1510754422",
  "text" : "Tunecore's Jeff Price has head in the right place. A biz between models breeds opportunity. This interview's great:  http:\/\/is.gd\/scA8",
  "id" : 1510754422,
  "created_at" : "2009-04-13 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Artists House Music",
      "screen_name" : "artistshouse",
      "indices" : [ 0, 13 ],
      "id_str" : "16610491",
      "id" : 16610491
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "1503885982",
  "geo" : { },
  "id_str" : "1504923583",
  "in_reply_to_user_id" : 16610491,
  "text" : "@artistshouse  nice post there; truly beneficial to artists who will listen.  I certainly will be thinking twice when I perform.",
  "id" : 1504923583,
  "in_reply_to_status_id" : 1503885982,
  "created_at" : "2009-04-12 00:00:00 +0000",
  "in_reply_to_screen_name" : "artistshouse",
  "in_reply_to_user_id_str" : "16610491",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Molly",
      "screen_name" : "Mollyinfolode",
      "indices" : [ 0, 14 ],
      "id_str" : "18380653",
      "id" : 18380653
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "1497506212",
  "in_reply_to_user_id" : 18380653,
  "text" : "@Mollyinfolode Do you see the music biz going subscription? How soon?",
  "id" : 1497506212,
  "created_at" : "2009-04-11 00:00:00 +0000",
  "in_reply_to_screen_name" : "Mollyinfolode",
  "in_reply_to_user_id_str" : "18380653",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Molly",
      "screen_name" : "Mollyinfolode",
      "indices" : [ 0, 14 ],
      "id_str" : "18380653",
      "id" : 18380653
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "1497514298",
  "geo" : { },
  "id_str" : "1497529061",
  "in_reply_to_user_id" : 18380653,
  "text" : "@Mollyinfolode do you see labels staying afloat via 360 deals?",
  "id" : 1497529061,
  "in_reply_to_status_id" : 1497514298,
  "created_at" : "2009-04-11 00:00:00 +0000",
  "in_reply_to_screen_name" : "Mollyinfolode",
  "in_reply_to_user_id_str" : "18380653",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Molly",
      "screen_name" : "Mollyinfolode",
      "indices" : [ 0, 14 ],
      "id_str" : "18380653",
      "id" : 18380653
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "1497562873",
  "geo" : { },
  "id_str" : "1497580898",
  "in_reply_to_user_id" : 18380653,
  "text" : "@Mollyinfolode I'm skeptical as not 1 record went platinum Q1 2009. If you have follow suggestions, pass along please.",
  "id" : 1497580898,
  "in_reply_to_status_id" : 1497562873,
  "created_at" : "2009-04-11 00:00:00 +0000",
  "in_reply_to_screen_name" : "Mollyinfolode",
  "in_reply_to_user_id_str" : "18380653",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "1488178421",
  "text" : "Interested to see what Vevo will be like, and if it can help this music industry find footing.  http:\/\/is.gd\/rI4Z",
  "id" : 1488178421,
  "created_at" : "2009-04-10 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "1485348932",
  "text" : "not surprised that Amazon and Walmart followed iTunes to variable pricing.",
  "id" : 1485348932,
  "created_at" : "2009-04-09 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
} ]