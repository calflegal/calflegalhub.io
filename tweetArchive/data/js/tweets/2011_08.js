Grailbird.data.tweets_2011_08 = 
 [ {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Wayne Twittaker",
      "screen_name" : "WayneTwittaker",
      "indices" : [ 0, 15 ],
      "id_str" : "2300470586",
      "id" : 2300470586
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "108839086768394240",
  "text" : "@WayneTwittaker \"They aint ready.\" I'm on the scene!",
  "id" : 108839086768394240,
  "created_at" : "2011-08-31 09:50:35 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "108554211633012737",
  "text" : "The problem with working on calculus is that you don't feel like you have much to contribute to a 400 year-old field.",
  "id" : 108554211633012737,
  "created_at" : "2011-08-30 14:58:35 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "MIT Media Lab",
      "screen_name" : "medialab",
      "indices" : [ 3, 12 ],
      "id_str" : "13982132",
      "id" : 13982132
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 90 ],
      "url" : "http:\/\/t.co\/dlt4ped",
      "expanded_url" : "http:\/\/bo.st\/nh4rB5",
      "display_url" : "bo.st\/nh4rB5"
    } ]
  },
  "geo" : { },
  "id_str" : "108549002244538368",
  "text" : "RT @medialab: LuminAR, A 'Lamp' that Turns Your Desktop into an iPad:  http:\/\/t.co\/dlt4ped",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitterfeed.com\" rel=\"nofollow\"\u003Etwitterfeed\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 57, 76 ],
        "url" : "http:\/\/t.co\/dlt4ped",
        "expanded_url" : "http:\/\/bo.st\/nh4rB5",
        "display_url" : "bo.st\/nh4rB5"
      } ]
    },
    "geo" : { },
    "id_str" : "108548271861022720",
    "text" : "LuminAR, A 'Lamp' that Turns Your Desktop into an iPad:  http:\/\/t.co\/dlt4ped",
    "id" : 108548271861022720,
    "created_at" : "2011-08-30 14:34:59 +0000",
    "user" : {
      "name" : "MIT Media Lab",
      "screen_name" : "medialab",
      "protected" : false,
      "id_str" : "13982132",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/524255035077566464\/vSun8cBu_normal.png",
      "id" : 13982132,
      "verified" : true
    }
  },
  "id" : 108549002244538368,
  "created_at" : "2011-08-30 14:37:53 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "60s",
      "indices" : [ 45, 49 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "108546713941319680",
  "text" : "Mmm they doin' that wiki wah pedal nonsense. #60s",
  "id" : 108546713941319680,
  "created_at" : "2011-08-30 14:28:47 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "108546501889900544",
  "text" : "Frankie Valli & The Four Seasons in the earbuds",
  "id" : 108546501889900544,
  "created_at" : "2011-08-30 14:27:57 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kate Breton",
      "screen_name" : "katebreton",
      "indices" : [ 0, 11 ],
      "id_str" : "29812449",
      "id" : 29812449
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "108530990275764224",
  "geo" : { },
  "id_str" : "108532117532704768",
  "in_reply_to_user_id" : 29812449,
  "text" : "@katebreton I'm sorry that was super mean!",
  "id" : 108532117532704768,
  "in_reply_to_status_id" : 108530990275764224,
  "created_at" : "2011-08-30 13:30:47 +0000",
  "in_reply_to_screen_name" : "katebreton",
  "in_reply_to_user_id_str" : "29812449",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kate Breton",
      "screen_name" : "katebreton",
      "indices" : [ 0, 11 ],
      "id_str" : "29812449",
      "id" : 29812449
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "108500369490972672",
  "geo" : { },
  "id_str" : "108525795726733314",
  "in_reply_to_user_id" : 29812449,
  "text" : "@katebreton oink",
  "id" : 108525795726733314,
  "in_reply_to_status_id" : 108500369490972672,
  "created_at" : "2011-08-30 13:05:40 +0000",
  "in_reply_to_screen_name" : "katebreton",
  "in_reply_to_user_id_str" : "29812449",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "girlytweeter",
      "indices" : [ 26, 39 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "108340533327437824",
  "in_reply_to_user_id" : 188548988,
  "text" : "@kikiminajjj rock n roll! #girlytweeter",
  "id" : 108340533327437824,
  "created_at" : "2011-08-30 00:49:30 +0000",
  "in_reply_to_screen_name" : "chiefkeeks",
  "in_reply_to_user_id_str" : "188548988",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Spotify",
      "screen_name" : "Spotify",
      "indices" : [ 16, 24 ],
      "id_str" : "17230018",
      "id" : 17230018
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "108192641723797506",
  "text" : "Now that I have @spotify , I might have to try some Nine Inch Nails out.",
  "id" : 108192641723797506,
  "created_at" : "2011-08-29 15:01:50 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Elizabeth McNamee",
      "screen_name" : "mcfjord",
      "indices" : [ 0, 8 ],
      "id_str" : "242988676",
      "id" : 242988676
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 25, 44 ],
      "url" : "http:\/\/t.co\/2DWXIgW",
      "expanded_url" : "http:\/\/serendipitor.net\/",
      "display_url" : "serendipitor.net"
    } ]
  },
  "geo" : { },
  "id_str" : "108180747042566144",
  "in_reply_to_user_id" : 242988676,
  "text" : "@mcfjord you'd love this http:\/\/t.co\/2DWXIgW",
  "id" : 108180747042566144,
  "created_at" : "2011-08-29 14:14:34 +0000",
  "in_reply_to_screen_name" : "mcfjord",
  "in_reply_to_user_id_str" : "242988676",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u13F4",
      "screen_name" : "brian_justie",
      "indices" : [ 3, 16 ],
      "id_str" : "19979495",
      "id" : 19979495
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "108178289134284800",
  "text" : "RT @brian_justie: pitchfork redesign looks like a free wordpress template.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "108045589698199553",
    "text" : "pitchfork redesign looks like a free wordpress template.",
    "id" : 108045589698199553,
    "created_at" : "2011-08-29 05:17:30 +0000",
    "user" : {
      "name" : "\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u13F4",
      "screen_name" : "brian_justie",
      "protected" : false,
      "id_str" : "19979495",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2662898436\/b7087a4fed59098ecba045a3cb96f3c9_normal.jpeg",
      "id" : 19979495,
      "verified" : false
    }
  },
  "id" : 108178289134284800,
  "created_at" : "2011-08-29 14:04:48 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Parris Ballentine",
      "screen_name" : "ParrisBtine",
      "indices" : [ 0, 12 ],
      "id_str" : "90442463",
      "id" : 90442463
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "108176696225705984",
  "geo" : { },
  "id_str" : "108177914050260992",
  "in_reply_to_user_id" : 90442463,
  "text" : "@ParrisBtine 3 day week?",
  "id" : 108177914050260992,
  "in_reply_to_status_id" : 108176696225705984,
  "created_at" : "2011-08-29 14:03:19 +0000",
  "in_reply_to_screen_name" : "ParrisBtine",
  "in_reply_to_user_id_str" : "90442463",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Parris Ballentine",
      "screen_name" : "ParrisBtine",
      "indices" : [ 0, 12 ],
      "id_str" : "90442463",
      "id" : 90442463
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "108176696225705984",
  "geo" : { },
  "id_str" : "108177873956904962",
  "in_reply_to_user_id" : 90442463,
  "text" : "@ParrisBtine damnnn boii",
  "id" : 108177873956904962,
  "in_reply_to_status_id" : 108176696225705984,
  "created_at" : "2011-08-29 14:03:09 +0000",
  "in_reply_to_screen_name" : "ParrisBtine",
  "in_reply_to_user_id_str" : "90442463",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ryan Foote",
      "screen_name" : "footedesign",
      "indices" : [ 0, 12 ],
      "id_str" : "48887172",
      "id" : 48887172
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "107557322787786752",
  "geo" : { },
  "id_str" : "107620556068757504",
  "in_reply_to_user_id" : 48887172,
  "text" : "@footedesign not yet!",
  "id" : 107620556068757504,
  "in_reply_to_status_id" : 107557322787786752,
  "created_at" : "2011-08-28 01:08:34 +0000",
  "in_reply_to_screen_name" : "footedesign",
  "in_reply_to_user_id_str" : "48887172",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "107308707087519744",
  "text" : "If I can get nerdy, The Next Episode remixes are tight on friday night",
  "id" : 107308707087519744,
  "created_at" : "2011-08-27 04:29:24 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Parris Ballentine",
      "screen_name" : "ParrisBtine",
      "indices" : [ 30, 42 ],
      "id_str" : "90442463",
      "id" : 90442463
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "107290088442040320",
  "text" : "Cookin' up arduino ideas with @ParrisBtine in portsmouth",
  "id" : 107290088442040320,
  "created_at" : "2011-08-27 03:15:25 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "107203006524702720",
  "text" : "Watching a movie with my 5 year old sis. \"You'll like these guys. They're mean just like you.\" Ouch.",
  "id" : 107203006524702720,
  "created_at" : "2011-08-26 21:29:23 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jane",
      "screen_name" : "janieflegal",
      "indices" : [ 0, 12 ],
      "id_str" : "197449091",
      "id" : 197449091
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "107198895049814016",
  "geo" : { },
  "id_str" : "107199148406747136",
  "in_reply_to_user_id" : 197449091,
  "text" : "@janieflegal no I called mommy",
  "id" : 107199148406747136,
  "in_reply_to_status_id" : 107198895049814016,
  "created_at" : "2011-08-26 21:14:03 +0000",
  "in_reply_to_screen_name" : "janieflegal",
  "in_reply_to_user_id_str" : "197449091",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kate Breton",
      "screen_name" : "katebreton",
      "indices" : [ 0, 11 ],
      "id_str" : "29812449",
      "id" : 29812449
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "106297394097946624",
  "geo" : { },
  "id_str" : "107071238551248896",
  "in_reply_to_user_id" : 29812449,
  "text" : "@katebreton watch that Stanford speech if you haven't",
  "id" : 107071238551248896,
  "in_reply_to_status_id" : 106297394097946624,
  "created_at" : "2011-08-26 12:45:47 +0000",
  "in_reply_to_screen_name" : "katebreton",
  "in_reply_to_user_id_str" : "29812449",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jane",
      "screen_name" : "janieflegal",
      "indices" : [ 0, 12 ],
      "id_str" : "197449091",
      "id" : 197449091
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "106772217748140034",
  "geo" : { },
  "id_str" : "106922899033694208",
  "in_reply_to_user_id" : 197449091,
  "text" : "@janieflegal ?",
  "id" : 106922899033694208,
  "in_reply_to_status_id" : 106772217748140034,
  "created_at" : "2011-08-26 02:56:20 +0000",
  "in_reply_to_screen_name" : "janieflegal",
  "in_reply_to_user_id_str" : "197449091",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "E-603",
      "screen_name" : "E603",
      "indices" : [ 3, 8 ],
      "id_str" : "65137195",
      "id" : 65137195
    }, {
      "name" : "Klout",
      "screen_name" : "klout",
      "indices" : [ 84, 90 ],
      "id_str" : "15134782",
      "id" : 15134782
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "106722276027793408",
  "text" : "Yo @E603 on Klout.com you are influential about basketball, pizza, and homelesness. @Klout",
  "id" : 106722276027793408,
  "created_at" : "2011-08-25 13:39:08 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "106721607422181377",
  "text" : "audio capture should be as easy as screen capture on the iphone.",
  "id" : 106721607422181377,
  "created_at" : "2011-08-25 13:36:28 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eric Bolton",
      "screen_name" : "walkbolton",
      "indices" : [ 0, 11 ],
      "id_str" : "121598461",
      "id" : 121598461
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "106570245220732928",
  "geo" : { },
  "id_str" : "106709896975757312",
  "in_reply_to_user_id" : 121598461,
  "text" : "@walkbolton oh I did don't you worry",
  "id" : 106709896975757312,
  "in_reply_to_status_id" : 106570245220732928,
  "created_at" : "2011-08-25 12:49:56 +0000",
  "in_reply_to_screen_name" : "walkbolton",
  "in_reply_to_user_id_str" : "121598461",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "106709323677315072",
  "text" : "I wish I owned Apple stock so I could sell now and re-buy in a few trading days.  The loss of Jobs as CEO won't matter THAT soon!",
  "id" : 106709323677315072,
  "created_at" : "2011-08-25 12:47:39 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "tryingtochange",
      "indices" : [ 58, 73 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "106566494380494849",
  "text" : "But I guess there world can't just be egomaniacs like me. #tryingtochange",
  "id" : 106566494380494849,
  "created_at" : "2011-08-25 03:20:06 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "106566220530200576",
  "text" : "There are times when I have such anger about them that I really think they should be denied.",
  "id" : 106566220530200576,
  "created_at" : "2011-08-25 03:19:01 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "106565847971135488",
  "text" : "You know there's a question on a dating site asking if stupid people should be allowed to have kids. Of course they should but...",
  "id" : 106565847971135488,
  "created_at" : "2011-08-25 03:17:32 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "106545218509217792",
  "text" : "Watchin the Little League World Series before bed.",
  "id" : 106545218509217792,
  "created_at" : "2011-08-25 01:55:34 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "BREAKING",
      "indices" : [ 22, 31 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "106498930921054208",
  "text" : "STEVE JOBS RESIGNED?! #BREAKING",
  "id" : 106498930921054208,
  "created_at" : "2011-08-24 22:51:38 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Klout",
      "screen_name" : "klout",
      "indices" : [ 43, 49 ],
      "id_str" : "15134782",
      "id" : 15134782
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "106468834571063296",
  "text" : "By the way, for the rest of you, check out @Klout. It's a really interesting way to measure your internet social network strength",
  "id" : 106468834571063296,
  "created_at" : "2011-08-24 20:52:02 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kid Stuff",
      "screen_name" : "thomasjsays",
      "indices" : [ 0, 12 ],
      "id_str" : "257111774",
      "id" : 257111774
    }, {
      "name" : "Klout",
      "screen_name" : "klout",
      "indices" : [ 20, 26 ],
      "id_str" : "15134782",
      "id" : 15134782
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "106466807338123265",
  "geo" : { },
  "id_str" : "106468420228362240",
  "in_reply_to_user_id" : 257111774,
  "text" : "@thomasjsays  ps my @klout score just beatchu",
  "id" : 106468420228362240,
  "in_reply_to_status_id" : 106466807338123265,
  "created_at" : "2011-08-24 20:50:24 +0000",
  "in_reply_to_screen_name" : "thomasjsays",
  "in_reply_to_user_id_str" : "257111774",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kid Stuff",
      "screen_name" : "thomasjsays",
      "indices" : [ 0, 12 ],
      "id_str" : "257111774",
      "id" : 257111774
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "106466807338123265",
  "geo" : { },
  "id_str" : "106468245221019648",
  "in_reply_to_user_id" : 257111774,
  "text" : "@thomasjsays reasonable I'd say!",
  "id" : 106468245221019648,
  "in_reply_to_status_id" : 106466807338123265,
  "created_at" : "2011-08-24 20:49:42 +0000",
  "in_reply_to_screen_name" : "thomasjsays",
  "in_reply_to_user_id_str" : "257111774",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "106095955669032960",
  "text" : "My mommy and sis were in the DC quake. They aight tho!",
  "id" : 106095955669032960,
  "created_at" : "2011-08-23 20:10:21 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "shreddedpaper",
      "indices" : [ 68, 82 ]
    }, {
      "text" : "wontgodown",
      "indices" : [ 83, 94 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "106019316624916481",
  "text" : "Nothin' worse than a number 2 experience on a low-horsepower toilet #shreddedpaper #wontgodown",
  "id" : 106019316624916481,
  "created_at" : "2011-08-23 15:05:49 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Emily Lindemer",
      "screen_name" : "EmilyLindy",
      "indices" : [ 0, 11 ],
      "id_str" : "333134363",
      "id" : 333134363
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "105985336747892736",
  "geo" : { },
  "id_str" : "105986288708104194",
  "in_reply_to_user_id" : 333134363,
  "text" : "@EmilyLindy that seems premature",
  "id" : 105986288708104194,
  "in_reply_to_status_id" : 105985336747892736,
  "created_at" : "2011-08-23 12:54:35 +0000",
  "in_reply_to_screen_name" : "EmilyLindy",
  "in_reply_to_user_id_str" : "333134363",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Forrst",
      "screen_name" : "forrst",
      "indices" : [ 6, 13 ],
      "id_str" : "2596652364",
      "id" : 2596652364
    }, {
      "name" : "Old Abram Brown",
      "screen_name" : "oldabrambrown",
      "indices" : [ 30, 44 ],
      "id_str" : "54112377",
      "id" : 54112377
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "105662966669647872",
  "text" : "Also, @forrst is really cool. @oldabrambrown (Erik) You on there yet??",
  "id" : 105662966669647872,
  "created_at" : "2011-08-22 15:29:49 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Wayne Twittaker",
      "screen_name" : "WayneTwittaker",
      "indices" : [ 0, 15 ],
      "id_str" : "2300470586",
      "id" : 2300470586
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "105653585974525952",
  "text" : "@WayneTwittaker This weather is beautiful!",
  "id" : 105653585974525952,
  "created_at" : "2011-08-22 14:52:32 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "105650054144065537",
  "text" : "It's funny to listen to Kanye's \"Monster\" on Apple earbuds. You miss the entire bangin bass drum track.",
  "id" : 105650054144065537,
  "created_at" : "2011-08-22 14:38:30 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "105144675920379904",
  "text" : "Portsmouth was so fun it wasn't even reasonable.",
  "id" : 105144675920379904,
  "created_at" : "2011-08-21 05:10:18 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "strategy",
      "indices" : [ 72, 81 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "105044238424735744",
  "text" : "Avoid storing numbers so your friends don't know who you're talking to. #strategy",
  "id" : 105044238424735744,
  "created_at" : "2011-08-20 22:31:12 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "104910397060034560",
  "text" : "Officially switched to Google Chrome. Really excited about it.",
  "id" : 104910397060034560,
  "created_at" : "2011-08-20 13:39:22 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "BABY DADDY",
      "screen_name" : "diplo",
      "indices" : [ 3, 9 ],
      "id_str" : "17174309",
      "id" : 17174309
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "104725293738098689",
  "text" : "RT @diplo: gawd damn i was just almost hit by lightning n shit i dont fux with that",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "104698546816688130",
    "text" : "gawd damn i was just almost hit by lightning n shit i dont fux with that",
    "id" : 104698546816688130,
    "created_at" : "2011-08-19 23:37:33 +0000",
    "user" : {
      "name" : "BABY DADDY",
      "screen_name" : "diplo",
      "protected" : false,
      "id_str" : "17174309",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/416239925332152320\/y2GwXqKC_normal.jpeg",
      "id" : 17174309,
      "verified" : true
    }
  },
  "id" : 104725293738098689,
  "created_at" : "2011-08-20 01:23:50 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "104667889520480256",
  "text" : "I must be really uncool nobody will hang out with me tonight!",
  "id" : 104667889520480256,
  "created_at" : "2011-08-19 21:35:44 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kate Breton",
      "screen_name" : "katebreton",
      "indices" : [ 0, 11 ],
      "id_str" : "29812449",
      "id" : 29812449
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "103302811084787712",
  "geo" : { },
  "id_str" : "103304257813495808",
  "in_reply_to_user_id" : 29812449,
  "text" : "@katebreton yeah. You are.",
  "id" : 103304257813495808,
  "in_reply_to_status_id" : 103302811084787712,
  "created_at" : "2011-08-16 03:17:09 +0000",
  "in_reply_to_screen_name" : "katebreton",
  "in_reply_to_user_id_str" : "29812449",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "103301504336801792",
  "text" : "\"Tweet successfully deleted\"",
  "id" : 103301504336801792,
  "created_at" : "2011-08-16 03:06:12 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "103293934859595776",
  "text" : "@kmonalisaa smoke you!",
  "id" : 103293934859595776,
  "created_at" : "2011-08-16 02:36:07 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "CBS News",
      "screen_name" : "CBSNews",
      "indices" : [ 3, 11 ],
      "id_str" : "15012486",
      "id" : 15012486
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 106, 125 ],
      "url" : "http:\/\/t.co\/Sg8ra1U",
      "expanded_url" : "http:\/\/bit.ly\/nx3EHO",
      "display_url" : "bit.ly\/nx3EHO"
    } ]
  },
  "geo" : { },
  "id_str" : "103131500190515202",
  "text" : "RT @CBSNews: In an op-ed Warren Buffett urges politicians in Washington to raise taxes on the \"mega-rich\" http:\/\/t.co\/Sg8ra1U (via @cbsn ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 93, 112 ],
        "url" : "http:\/\/t.co\/Sg8ra1U",
        "expanded_url" : "http:\/\/bit.ly\/nx3EHO",
        "display_url" : "bit.ly\/nx3EHO"
      } ]
    },
    "geo" : { },
    "id_str" : "103120174663729152",
    "text" : "In an op-ed Warren Buffett urges politicians in Washington to raise taxes on the \"mega-rich\" http:\/\/t.co\/Sg8ra1U (via @cbsnewshotsheet)",
    "id" : 103120174663729152,
    "created_at" : "2011-08-15 15:05:40 +0000",
    "user" : {
      "name" : "CBS News",
      "screen_name" : "CBSNews",
      "protected" : false,
      "id_str" : "15012486",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/461209318159679488\/8b-i8DCP_normal.jpeg",
      "id" : 15012486,
      "verified" : true
    }
  },
  "id" : 103131500190515202,
  "created_at" : "2011-08-15 15:50:40 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "102475284284055552",
  "text" : "Why isn't TV more interactive? Why should I have to wait to see the scoreboard? Why can't I drag it to where I like it?",
  "id" : 102475284284055552,
  "created_at" : "2011-08-13 20:23:06 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "102451072542584832",
  "text" : "All yall are a bunch of DWEEBS",
  "id" : 102451072542584832,
  "created_at" : "2011-08-13 18:46:53 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "101811426594783232",
  "text" : "PS really into the new Michelle Branch single. Got that SilversunPickup guitar thing goin' on",
  "id" : 101811426594783232,
  "created_at" : "2011-08-12 00:25:10 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "101811176035467265",
  "text" : "\"Oh yeah I know youu! You're that super average person nobody would notice\" hahahaha",
  "id" : 101811176035467265,
  "created_at" : "2011-08-12 00:24:10 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ClassicCompressors",
      "indices" : [ 27, 46 ]
    }, {
      "text" : "nerdtweet",
      "indices" : [ 47, 57 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "101801167960555520",
  "text" : "Also this is tweet # 1176. #ClassicCompressors #nerdtweet",
  "id" : 101801167960555520,
  "created_at" : "2011-08-11 23:44:24 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "101801062373134336",
  "text" : "The stock market is like a damn yo-yo right now.",
  "id" : 101801062373134336,
  "created_at" : "2011-08-11 23:43:59 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "101674235084288000",
  "text" : "Maybe it's better for you, but generally it's just cooler.",
  "id" : 101674235084288000,
  "created_at" : "2011-08-11 15:20:01 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "101674133875728384",
  "text" : "Whatever you use, TextMate or other editor, it's important to  code with a black background so you look cooler to onlookers.",
  "id" : 101674133875728384,
  "created_at" : "2011-08-11 15:19:37 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "101483246210129920",
  "text" : "@LuckyJacque NO",
  "id" : 101483246210129920,
  "created_at" : "2011-08-11 02:41:06 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "101476692471717888",
  "text" : "The summer is the best. At the garden.",
  "id" : 101476692471717888,
  "created_at" : "2011-08-11 02:15:03 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "amarlo",
      "screen_name" : "amarlo2",
      "indices" : [ 0, 8 ],
      "id_str" : "1649119020",
      "id" : 1649119020
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "100949621785886721",
  "geo" : { },
  "id_str" : "100950072476446720",
  "in_reply_to_user_id" : 68614037,
  "text" : "@amarlo2  I totaly missed your Kreayshawn tweet reference until just now",
  "id" : 100950072476446720,
  "in_reply_to_status_id" : 100949621785886721,
  "created_at" : "2011-08-09 15:22:27 +0000",
  "in_reply_to_screen_name" : "annalocke_",
  "in_reply_to_user_id_str" : "68614037",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Old Abram Brown",
      "screen_name" : "oldabrambrown",
      "indices" : [ 13, 27 ],
      "id_str" : "54112377",
      "id" : 54112377
    }, {
      "name" : "Eric Bolton",
      "screen_name" : "walkbolton",
      "indices" : [ 28, 39 ],
      "id_str" : "121598461",
      "id" : 121598461
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "practicemakesperfect",
      "indices" : [ 81, 102 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "100772068370554880",
  "text" : "@LuckyJacque @oldabrambrown @walkbolton  actually i may be outta town next week. #practicemakesperfect",
  "id" : 100772068370554880,
  "created_at" : "2011-08-09 03:35:08 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "practicemakesperfect",
      "indices" : [ 47, 68 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "100771870156132352",
  "text" : "@LuckyJacque  GUY ARE YOU COMING NEXT WEEK?!!  #practicemakesperfect",
  "id" : 100771870156132352,
  "created_at" : "2011-08-09 03:34:20 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Candlepin",
      "indices" : [ 33, 43 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "100701896129519616",
  "text" : "Monday night bowling? Yeah yeah! #Candlepin",
  "id" : 100701896129519616,
  "created_at" : "2011-08-08 22:56:17 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "crankitup",
      "indices" : [ 52, 62 ]
    }, {
      "text" : "slammers",
      "indices" : [ 63, 72 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "100596938767994880",
  "text" : "Little Muse and Mars Volta as a soundtrack to work. #crankitup #slammers",
  "id" : 100596938767994880,
  "created_at" : "2011-08-08 15:59:13 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Wall Street Journal",
      "screen_name" : "WSJ",
      "indices" : [ 17, 21 ],
      "id_str" : "3108351",
      "id" : 3108351
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "100590729205788672",
  "text" : "ay.  no good. RT @WSJ: Bank of America is down 15.2% this morning as bank stocks take a beating http:\/\/is.gd\/RzUisy",
  "id" : 100590729205788672,
  "created_at" : "2011-08-08 15:34:33 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lauren Swidler",
      "screen_name" : "LaurenSwid",
      "indices" : [ 0, 11 ],
      "id_str" : "283846658",
      "id" : 283846658
    }, {
      "name" : "MA Gill",
      "screen_name" : "mag91944",
      "indices" : [ 12, 21 ],
      "id_str" : "159086370",
      "id" : 159086370
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "100190740382093312",
  "in_reply_to_user_id" : 283846658,
  "text" : "@LaurenSwid @mag91944 big bowl of cocoa krispies to be sure I have a sugar high crash in a little bit!",
  "id" : 100190740382093312,
  "created_at" : "2011-08-07 13:05:08 +0000",
  "in_reply_to_screen_name" : "LaurenSwid",
  "in_reply_to_user_id_str" : "283846658",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "99875465078124544",
  "text" : "PS Nicki Minaj on Good Morning America was one of the worst performances I have ever seen. The Nip Slip offered no redemption.",
  "id" : 99875465078124544,
  "created_at" : "2011-08-06 16:12:21 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Los Angeles Times",
      "screen_name" : "latimes",
      "indices" : [ 3, 11 ],
      "id_str" : "16664681",
      "id" : 16664681
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "99838762032443393",
  "text" : "RT @latimes: Google's self-driving car is in an accident http:\/\/lat.ms\/qVYHBx",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "99710043607416832",
    "text" : "Google's self-driving car is in an accident http:\/\/lat.ms\/qVYHBx",
    "id" : 99710043607416832,
    "created_at" : "2011-08-06 05:15:01 +0000",
    "user" : {
      "name" : "Los Angeles Times",
      "screen_name" : "latimes",
      "protected" : false,
      "id_str" : "16664681",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1894745045\/LAT_normal.jpg",
      "id" : 16664681,
      "verified" : true
    }
  },
  "id" : 99838762032443393,
  "created_at" : "2011-08-06 13:46:30 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Boston Globe",
      "screen_name" : "BostonGlobe",
      "indices" : [ 3, 15 ],
      "id_str" : "95431448",
      "id" : 95431448
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "99838253120757761",
  "text" : "RT @BostonGlobe: Would you drop out of college in exchange for $100K in start-up funds? Foundation is getting students to do that - http ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "99835875575349248",
    "text" : "Would you drop out of college in exchange for $100K in start-up funds? Foundation is getting students to do that - http:\/\/bo.st\/pVk9wo",
    "id" : 99835875575349248,
    "created_at" : "2011-08-06 13:35:02 +0000",
    "user" : {
      "name" : "The Boston Globe",
      "screen_name" : "BostonGlobe",
      "protected" : false,
      "id_str" : "95431448",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1357463919\/bGlobe_normal.jpg",
      "id" : 95431448,
      "verified" : true
    }
  },
  "id" : 99838253120757761,
  "created_at" : "2011-08-06 13:44:29 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "99716265563795456",
  "text" : "Horrible nightmare where vicious gum was growing rampantly in my mouth clinging to my teeth . I could neither pry it off nor slow its pace.",
  "id" : 99716265563795456,
  "created_at" : "2011-08-06 05:39:45 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "99676739315314688",
  "text" : "And they don't even show the sicko stuff that makes the concept intriguing",
  "id" : 99676739315314688,
  "created_at" : "2011-08-06 03:02:41 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "99676536055140352",
  "text" : "Nobody actually thinks it's really interesting, they just like the summer spirit associated with it.",
  "id" : 99676536055140352,
  "created_at" : "2011-08-06 03:01:52 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "99676400843358208",
  "text" : "Shark week is a worse hype machine product than Rebecca Black.",
  "id" : 99676400843358208,
  "created_at" : "2011-08-06 03:01:20 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Old Abram Brown",
      "screen_name" : "oldabrambrown",
      "indices" : [ 0, 14 ],
      "id_str" : "54112377",
      "id" : 54112377
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "99301992144449536",
  "geo" : { },
  "id_str" : "99320704184500224",
  "in_reply_to_user_id" : 54112377,
  "text" : "@oldabrambrown I was there!",
  "id" : 99320704184500224,
  "in_reply_to_status_id" : 99301992144449536,
  "created_at" : "2011-08-05 03:27:55 +0000",
  "in_reply_to_screen_name" : "oldabrambrown",
  "in_reply_to_user_id_str" : "54112377",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Parris Ballentine",
      "screen_name" : "ParrisBtine",
      "indices" : [ 0, 12 ],
      "id_str" : "90442463",
      "id" : 90442463
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "98930392417042432",
  "geo" : { },
  "id_str" : "98930628212428800",
  "in_reply_to_user_id" : 90442463,
  "text" : "@ParrisBtine already lost em",
  "id" : 98930628212428800,
  "in_reply_to_status_id" : 98930392417042432,
  "created_at" : "2011-08-04 01:37:54 +0000",
  "in_reply_to_screen_name" : "ParrisBtine",
  "in_reply_to_user_id_str" : "90442463",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "98850432088150016",
  "text" : "Great fortune! http:\/\/twitpic.com\/60ho88",
  "id" : 98850432088150016,
  "created_at" : "2011-08-03 20:19:14 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Parris Ballentine",
      "screen_name" : "ParrisBtine",
      "indices" : [ 0, 12 ],
      "id_str" : "90442463",
      "id" : 90442463
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "98822248588255232",
  "in_reply_to_user_id" : 90442463,
  "text" : "@ParrisBtine this is us man! http:\/\/twitpic.com\/60gagg",
  "id" : 98822248588255232,
  "created_at" : "2011-08-03 18:27:14 +0000",
  "in_reply_to_screen_name" : "ParrisBtine",
  "in_reply_to_user_id_str" : "90442463",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "98653697059471360",
  "text" : "sometimes when you wake up in the middle of the night it's best to get up and start over.",
  "id" : 98653697059471360,
  "created_at" : "2011-08-03 07:17:28 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lindsay Ferris",
      "screen_name" : "Lindsay_Ferris",
      "indices" : [ 0, 15 ],
      "id_str" : "326910285",
      "id" : 326910285
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "98542063104167936",
  "geo" : { },
  "id_str" : "98564160002064384",
  "in_reply_to_user_id" : 326910285,
  "text" : "@Lindsay_Ferris dayum",
  "id" : 98564160002064384,
  "in_reply_to_status_id" : 98542063104167936,
  "created_at" : "2011-08-03 01:21:41 +0000",
  "in_reply_to_screen_name" : "Lindsay_Ferris",
  "in_reply_to_user_id_str" : "326910285",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eric Bolton",
      "screen_name" : "walkbolton",
      "indices" : [ 0, 11 ],
      "id_str" : "121598461",
      "id" : 121598461
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "98514972484714496",
  "geo" : { },
  "id_str" : "98515522827726849",
  "in_reply_to_user_id" : 121598461,
  "text" : "@walkbolton gibson.",
  "id" : 98515522827726849,
  "in_reply_to_status_id" : 98514972484714496,
  "created_at" : "2011-08-02 22:08:25 +0000",
  "in_reply_to_screen_name" : "walkbolton",
  "in_reply_to_user_id_str" : "121598461",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Wayne Twittaker",
      "screen_name" : "WayneTwittaker",
      "indices" : [ 53, 68 ],
      "id_str" : "2300470586",
      "id" : 2300470586
    }, {
      "name" : "Eric Bolton",
      "screen_name" : "walkbolton",
      "indices" : [ 69, 80 ],
      "id_str" : "121598461",
      "id" : 121598461
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "nocleanclothes",
      "indices" : [ 37, 52 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "98393329045217280",
  "text" : "Goin real deep into b-squad laundry. #nocleanclothes @WayneTwittaker @walkbolton",
  "id" : 98393329045217280,
  "created_at" : "2011-08-02 14:02:52 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chad Johnson",
      "screen_name" : "ochocinco",
      "indices" : [ 3, 13 ],
      "id_str" : "40519997",
      "id" : 40519997
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "wickedpissah",
      "indices" : [ 30, 43 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "98091350519590912",
  "text" : "RT @ochocinco: Man Foxboro is #wickedpissah",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.echofon.com\/\" rel=\"nofollow\"\u003EEchofon\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "wickedpissah",
        "indices" : [ 15, 28 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "98087245881417728",
    "text" : "Man Foxboro is #wickedpissah",
    "id" : 98087245881417728,
    "created_at" : "2011-08-01 17:46:36 +0000",
    "user" : {
      "name" : "Chad Johnson",
      "screen_name" : "ochocinco",
      "protected" : false,
      "id_str" : "40519997",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/525245636002983936\/QV3ntazP_normal.jpeg",
      "id" : 40519997,
      "verified" : true
    }
  },
  "id" : 98091350519590912,
  "created_at" : "2011-08-01 18:02:55 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Old Abram Brown",
      "screen_name" : "oldabrambrown",
      "indices" : [ 0, 14 ],
      "id_str" : "54112377",
      "id" : 54112377
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "98067433734156289",
  "geo" : { },
  "id_str" : "98067704820416512",
  "in_reply_to_user_id" : 54112377,
  "text" : "@oldabrambrown right now guy",
  "id" : 98067704820416512,
  "in_reply_to_status_id" : 98067433734156289,
  "created_at" : "2011-08-01 16:28:57 +0000",
  "in_reply_to_screen_name" : "oldabrambrown",
  "in_reply_to_user_id_str" : "54112377",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kid Stuff",
      "screen_name" : "thomasjsays",
      "indices" : [ 0, 12 ],
      "id_str" : "257111774",
      "id" : 257111774
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "97851332400136192",
  "geo" : { },
  "id_str" : "97887068444827648",
  "in_reply_to_user_id" : 257111774,
  "text" : "@thomasjsays Nice BLL",
  "id" : 97887068444827648,
  "in_reply_to_status_id" : 97851332400136192,
  "created_at" : "2011-08-01 04:31:10 +0000",
  "in_reply_to_screen_name" : "thomasjsays",
  "in_reply_to_user_id_str" : "257111774",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
} ]