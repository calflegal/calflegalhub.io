Grailbird.data.tweets_2012_11 = 
 [ {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "274690221998751745",
  "text" : "Never been more excited for delivery food.",
  "id" : 274690221998751745,
  "created_at" : "2012-12-01 01:43:47 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "274533613700259840",
  "text" : "The future of web apps is very bright, although still somewhat distant. I'll keep  on it though, won't hurt to be ready when it arrives.",
  "id" : 274533613700259840,
  "created_at" : "2012-11-30 15:21:28 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "jessie",
      "screen_name" : "yesseeca",
      "indices" : [ 0, 9 ],
      "id_str" : "200635528",
      "id" : 200635528
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "274527375960702976",
  "geo" : { },
  "id_str" : "274532902438588417",
  "in_reply_to_user_id" : 200635528,
  "text" : "@yesseeca man I wish",
  "id" : 274532902438588417,
  "in_reply_to_status_id" : 274527375960702976,
  "created_at" : "2012-11-30 15:18:39 +0000",
  "in_reply_to_screen_name" : "yesseeca",
  "in_reply_to_user_id_str" : "200635528",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "elliotkember",
      "screen_name" : "elliotkember",
      "indices" : [ 45, 58 ],
      "id_str" : "16947321",
      "id" : 16947321
    }, {
      "name" : "Greg Raiz",
      "screen_name" : "graiz",
      "indices" : [ 59, 65 ],
      "id_str" : "6674162",
      "id" : 6674162
    }, {
      "name" : "Daniel Eden",
      "screen_name" : "_dte",
      "indices" : [ 66, 71 ],
      "id_str" : "23082430",
      "id" : 23082430
    }, {
      "name" : "Michael",
      "screen_name" : "michaelw90",
      "indices" : [ 72, 83 ],
      "id_str" : "154648125",
      "id" : 154648125
    } ],
    "media" : [ {
      "expanded_url" : "http:\/\/twitter.com\/calflegal\/status\/273491164731158529\/photo\/1",
      "indices" : [ 84, 104 ],
      "url" : "http:\/\/t.co\/26PxfNGF",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/A8uiw-uCMAAXLx2.png",
      "id_str" : "273491164739547136",
      "id" : 273491164739547136,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/A8uiw-uCMAAXLx2.png",
      "sizes" : [ {
        "h" : 32,
        "resize" : "fit",
        "w" : 677
      }, {
        "h" : 32,
        "resize" : "fit",
        "w" : 677
      }, {
        "h" : 28,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 16,
        "resize" : "fit",
        "w" : 340
      }, {
        "h" : 32,
        "resize" : "crop",
        "w" : 150
      } ],
      "display_url" : "pic.twitter.com\/26PxfNGF"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "273491164731158529",
  "text" : "Now this is a quality software recruiting ad @elliotkember @graiz @_dte @michaelw90 http:\/\/t.co\/26PxfNGF",
  "id" : 273491164731158529,
  "created_at" : "2012-11-27 18:19:09 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "breaktime",
      "indices" : [ 106, 116 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "273464187785707520",
  "text" : "I love when my teachers do calculus on the board because it takes a while and is usually straight-forward #breaktime",
  "id" : 273464187785707520,
  "created_at" : "2012-11-27 16:31:57 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Wayne Twittaker",
      "screen_name" : "WayneTwittaker",
      "indices" : [ 0, 15 ],
      "id_str" : "2300470586",
      "id" : 2300470586
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "271982218761613312",
  "text" : "@WayneTwittaker haha thanks man miss you too.",
  "id" : 271982218761613312,
  "created_at" : "2012-11-23 14:23:08 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lauren Swidler",
      "screen_name" : "LaurenSwid",
      "indices" : [ 0, 11 ],
      "id_str" : "283846658",
      "id" : 283846658
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "269658710761172992",
  "geo" : { },
  "id_str" : "269662045409013760",
  "in_reply_to_user_id" : 283846658,
  "text" : "@LaurenSwid it lives.",
  "id" : 269662045409013760,
  "in_reply_to_status_id" : 269658710761172992,
  "created_at" : "2012-11-17 04:43:36 +0000",
  "in_reply_to_screen_name" : "LaurenSwid",
  "in_reply_to_user_id_str" : "283846658",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "268843828482568193",
  "text" : "Why do areas with multiple elevators use the same bing sound for any elevator going up or down? Wouldn't it be more accessible if not?",
  "id" : 268843828482568193,
  "created_at" : "2012-11-14 22:32:18 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Elliott  Kember",
      "screen_name" : "elliottkember",
      "indices" : [ 3, 17 ],
      "id_str" : "903351",
      "id" : 903351
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "268712792482279424",
  "text" : "RT @elliottkember: Free as in slow",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/itunes.apple.com\/us\/app\/twitter\/id409789998?mt=12\" rel=\"nofollow\"\u003ETwitter for Mac\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "268702861259599872",
    "text" : "Free as in slow",
    "id" : 268702861259599872,
    "created_at" : "2012-11-14 13:12:09 +0000",
    "user" : {
      "name" : "Elliott  Kember",
      "screen_name" : "elliottkember",
      "protected" : false,
      "id_str" : "903351",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/527343390195585024\/tBxCTKxd_normal.jpeg",
      "id" : 903351,
      "verified" : false
    }
  },
  "id" : 268712792482279424,
  "created_at" : "2012-11-14 13:51:36 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "win",
      "indices" : [ 38, 42 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "266536388273508352",
  "text" : "Fresh box of vanilla almond clusters. #win",
  "id" : 266536388273508352,
  "created_at" : "2012-11-08 13:43:21 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "265865669818601472",
  "text" : "Whereas tweeting about people tweeting about people tweeting about the election is just meta and hence worthwhile.",
  "id" : 265865669818601472,
  "created_at" : "2012-11-06 17:18:09 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "265865495914364928",
  "text" : "Tweeting about people tweeting about the election seems like a waste of time.",
  "id" : 265865495914364928,
  "created_at" : "2012-11-06 17:17:28 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
} ]