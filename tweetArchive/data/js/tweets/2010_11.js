Grailbird.data.tweets_2010_11 = 
 [ {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Aaron Nevezie",
      "screen_name" : "aaronnevezie",
      "indices" : [ 3, 16 ],
      "id_str" : "28129799",
      "id" : 28129799
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "9832555876454400",
  "text" : "RT @aaronnevezie: Two more Neve 1084s added to the Bunker collection today.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "9730573190828032",
    "text" : "Two more Neve 1084s added to the Bunker collection today.",
    "id" : 9730573190828032,
    "created_at" : "2010-11-30 22:08:44 +0000",
    "user" : {
      "name" : "Aaron Nevezie",
      "screen_name" : "aaronnevezie",
      "protected" : false,
      "id_str" : "28129799",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1069086535\/image_normal.jpg",
      "id" : 28129799,
      "verified" : false
    }
  },
  "id" : 9832555876454400,
  "created_at" : "2010-12-01 04:53:58 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Red Pill Music",
      "screen_name" : "redpillmusic",
      "indices" : [ 0, 13 ],
      "id_str" : "43556970",
      "id" : 43556970
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "9318901095596032",
  "geo" : { },
  "id_str" : "9357824630984704",
  "in_reply_to_user_id" : 43556970,
  "text" : "@redpillmusic haha valid enough!",
  "id" : 9357824630984704,
  "in_reply_to_status_id" : 9318901095596032,
  "created_at" : "2010-11-29 21:27:33 +0000",
  "in_reply_to_screen_name" : "redpillmusic",
  "in_reply_to_user_id_str" : "43556970",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "winterwonderland",
      "indices" : [ 33, 50 ]
    } ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "9157198089297920",
  "geo" : { },
  "id_str" : "9307503913345024",
  "in_reply_to_user_id" : 31189024,
  "text" : "@HiMyNameisDina  that's amazing! #winterwonderland",
  "id" : 9307503913345024,
  "in_reply_to_status_id" : 9157198089297920,
  "created_at" : "2010-11-29 18:07:36 +0000",
  "in_reply_to_screen_name" : "dinataye",
  "in_reply_to_user_id_str" : "31189024",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Stereogum",
      "screen_name" : "stereogum",
      "indices" : [ 19, 29 ],
      "id_str" : "5135521",
      "id" : 5135521
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "9307246219501568",
  "text" : "oh i bet --&gt; RT @stereogum: Vampire Weekend's \"Holiday\" soundtracking all sorts of holiday commercials: http:\/\/bit.ly\/f9icmj",
  "id" : 9307246219501568,
  "created_at" : "2010-11-29 18:06:35 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Red Pill Music",
      "screen_name" : "redpillmusic",
      "indices" : [ 0, 13 ],
      "id_str" : "43556970",
      "id" : 43556970
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "9304650243121152",
  "geo" : { },
  "id_str" : "9307109187391488",
  "in_reply_to_user_id" : 43556970,
  "text" : "@redpillmusic  shouldn't gear sluts be \"gearslutz\"?",
  "id" : 9307109187391488,
  "in_reply_to_status_id" : 9304650243121152,
  "created_at" : "2010-11-29 18:06:02 +0000",
  "in_reply_to_screen_name" : "redpillmusic",
  "in_reply_to_user_id_str" : "43556970",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "9306926915526656",
  "text" : "Also R.I.P. Leslie Nielsen.",
  "id" : 9306926915526656,
  "created_at" : "2010-11-29 18:05:18 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "9306695536738304",
  "text" : "if you have a face that has a \"mushed\" quality at a young age, there's bad news: that's your face forever those types don't change.",
  "id" : 9306695536738304,
  "created_at" : "2010-11-29 18:04:23 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.cloudhopper.com\/\" rel=\"nofollow\"\u003ECloudhopper\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "9028309203550208",
  "text" : "the truth is i hate shopping too much to find great deals, especially on clothes. just give me good stuff at a decent price.",
  "id" : 9028309203550208,
  "created_at" : "2010-11-28 23:38:11 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.cloudhopper.com\/\" rel=\"nofollow\"\u003ECloudhopper\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "7479037794459648",
  "text" : "this dunkin donuts bathroom toilet flushes like a typhoon",
  "id" : 7479037794459648,
  "created_at" : "2010-11-24 17:01:56 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Steven Bone",
      "screen_name" : "stevenbone",
      "indices" : [ 0, 11 ],
      "id_str" : "14324362",
      "id" : 14324362
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "7272424223539200",
  "geo" : { },
  "id_str" : "7273685131988992",
  "in_reply_to_user_id" : 17532645,
  "text" : "@stevenbone  i like them too but not because  they sound good",
  "id" : 7273685131988992,
  "in_reply_to_status_id" : 7272424223539200,
  "created_at" : "2010-11-24 03:25:56 +0000",
  "in_reply_to_screen_name" : "stovebean",
  "in_reply_to_user_id_str" : "17532645",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ew",
      "indices" : [ 137, 140 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "7263019130163200",
  "text" : "rolling off some lows from my Yamaha HS-80Ms has helped tighten them up in this small room, but they sound a little more like NS-10s now #ew",
  "id" : 7263019130163200,
  "created_at" : "2010-11-24 02:43:33 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "7261956423553024",
  "text" : "@mmck031 sometimes you just get lucky",
  "id" : 7261956423553024,
  "created_at" : "2010-11-24 02:39:19 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "7101667883352065",
  "text" : "puttin on my serous developer hat",
  "id" : 7101667883352065,
  "created_at" : "2010-11-23 16:02:24 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.cloudhopper.com\/\" rel=\"nofollow\"\u003ECloudhopper\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "6849317553836032",
  "text" : "dude just asked me what borough he is in... uhh",
  "id" : 6849317553836032,
  "created_at" : "2010-11-22 23:19:39 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.cloudhopper.com\/\" rel=\"nofollow\"\u003ECloudhopper\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "6472341085224960",
  "text" : "i also just saw a mouse in the wild in the park. that happens? is he a runaway?",
  "id" : 6472341085224960,
  "created_at" : "2010-11-21 22:21:41 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.cloudhopper.com\/\" rel=\"nofollow\"\u003ECloudhopper\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "6471407630286848",
  "text" : "oh man i just smelled apple pie on my walk home. i need some very soon",
  "id" : 6471407630286848,
  "created_at" : "2010-11-21 22:17:58 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.cloudhopper.com\/\" rel=\"nofollow\"\u003ECloudhopper\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "6457402278805505",
  "text" : "the atlantic terminal target is a micrososm of hell",
  "id" : 6457402278805505,
  "created_at" : "2010-11-21 21:22:19 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "5949145734975488",
  "text" : "3 hours of sleep into a 13-hour workday is not exactly a blast",
  "id" : 5949145734975488,
  "created_at" : "2010-11-20 11:42:41 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "5505983971332096",
  "text" : "when u get sick u should just do lines of airborne.",
  "id" : 5505983971332096,
  "created_at" : "2010-11-19 06:21:43 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.cloudhopper.com\/\" rel=\"nofollow\"\u003ECloudhopper\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "iamqueensboulevard",
      "indices" : [ 51, 70 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "5463912644354049",
  "text" : "just got called a nerd in queens by a passing car. #iamqueensboulevard",
  "id" : 5463912644354049,
  "created_at" : "2010-11-19 03:34:32 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "5304906269327361",
  "geo" : { },
  "id_str" : "5308175272116225",
  "in_reply_to_user_id" : 43336898,
  "text" : "@annpolaneczky  got any people i should follow?",
  "id" : 5308175272116225,
  "in_reply_to_status_id" : 5304906269327361,
  "created_at" : "2010-11-18 17:15:42 +0000",
  "in_reply_to_screen_name" : "annpolan",
  "in_reply_to_user_id_str" : "43336898",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "5304906269327361",
  "geo" : { },
  "id_str" : "5308112009437185",
  "in_reply_to_user_id" : 43336898,
  "text" : "@annpolaneczky unfortunately in music it is kind of STILL the standard. I want to change that.",
  "id" : 5308112009437185,
  "in_reply_to_status_id" : 5304906269327361,
  "created_at" : "2010-11-18 17:15:27 +0000",
  "in_reply_to_screen_name" : "annpolan",
  "in_reply_to_user_id_str" : "43336898",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "5304466869846016",
  "text" : "If so -- engage me -- let's talk about the new super-hip myspace logo \"my _______\".  Do they even know anything about what's cool?",
  "id" : 5304466869846016,
  "created_at" : "2010-11-18 17:00:58 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Wayne Twittaker",
      "screen_name" : "WayneTwittaker",
      "indices" : [ 54, 69 ],
      "id_str" : "2300470586",
      "id" : 2300470586
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "5303405354422272",
  "text" : "Does anybody out there even read my tweets except for @waynetwittaker ?",
  "id" : 5303405354422272,
  "created_at" : "2010-11-18 16:56:45 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Liz Longley",
      "screen_name" : "Liz_Longley",
      "indices" : [ 65, 77 ],
      "id_str" : "2586626227",
      "id" : 2586626227
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "5272709357371392",
  "text" : "The show last night was great. Great people and great performers @liz_longley",
  "id" : 5272709357371392,
  "created_at" : "2010-11-18 14:54:46 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Liz Longley",
      "screen_name" : "Liz_Longley",
      "indices" : [ 0, 12 ],
      "id_str" : "2586626227",
      "id" : 2586626227
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "4952390650298370",
  "geo" : { },
  "id_str" : "4955858060451840",
  "in_reply_to_user_id" : 31582212,
  "text" : "@Liz_Longley  I think I will",
  "id" : 4955858060451840,
  "in_reply_to_status_id" : 4952390650298370,
  "created_at" : "2010-11-17 17:55:43 +0000",
  "in_reply_to_screen_name" : "lizlongley",
  "in_reply_to_user_id_str" : "31582212",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Liz Longley",
      "screen_name" : "Liz_Longley",
      "indices" : [ 0, 12 ],
      "id_str" : "2586626227",
      "id" : 2586626227
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "4749984415092737",
  "geo" : { },
  "id_str" : "4942782225448960",
  "in_reply_to_user_id" : 31582212,
  "text" : "@Liz_Longley so are you playing tonight in NYC?",
  "id" : 4942782225448960,
  "in_reply_to_status_id" : 4749984415092737,
  "created_at" : "2010-11-17 17:03:45 +0000",
  "in_reply_to_screen_name" : "lizlongley",
  "in_reply_to_user_id_str" : "31582212",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "K MacDuff",
      "screen_name" : "K_M20",
      "indices" : [ 0, 6 ],
      "id_str" : "320952711",
      "id" : 320952711
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "4942545649930240",
  "text" : "@K_m20  really likin' the new Kings, huh?",
  "id" : 4942545649930240,
  "created_at" : "2010-11-17 17:02:49 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Liz Fohl",
      "screen_name" : "lizfohl",
      "indices" : [ 0, 8 ],
      "id_str" : "20460448",
      "id" : 20460448
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "4795761397080064",
  "geo" : { },
  "id_str" : "4942395481268224",
  "in_reply_to_user_id" : 20460448,
  "text" : "@lizfohl cept next time mention me later so i can steal your followers.",
  "id" : 4942395481268224,
  "in_reply_to_status_id" : 4795761397080064,
  "created_at" : "2010-11-17 17:02:13 +0000",
  "in_reply_to_screen_name" : "lizfohl",
  "in_reply_to_user_id_str" : "20460448",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "4940353350139904",
  "text" : "The  spoon I use to scoop coffee is the same one I use to eat my cereal, so I get that extra jolt.",
  "id" : 4940353350139904,
  "created_at" : "2010-11-17 16:54:06 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "4937738574893056",
  "text" : "I dare you to listen to this song and not like it http:\/\/is.gd\/hijlK",
  "id" : 4937738574893056,
  "created_at" : "2010-11-17 16:43:43 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Liz Fohl",
      "screen_name" : "lizfohl",
      "indices" : [ 0, 8 ],
      "id_str" : "20460448",
      "id" : 20460448
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "4795761397080064",
  "geo" : { },
  "id_str" : "4937553757081600",
  "in_reply_to_user_id" : 20460448,
  "text" : "@lizfohl  k great now gimme the goods.",
  "id" : 4937553757081600,
  "in_reply_to_status_id" : 4795761397080064,
  "created_at" : "2010-11-17 16:42:59 +0000",
  "in_reply_to_screen_name" : "lizfohl",
  "in_reply_to_user_id_str" : "20460448",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "4790136009138176",
  "text" : "not replying to tweets @ you is sometimes rude. Don't do it.",
  "id" : 4790136009138176,
  "created_at" : "2010-11-17 06:57:12 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.cloudhopper.com\/\" rel=\"nofollow\"\u003ECloudhopper\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "4662608414117888",
  "text" : "my American dream: hot sauce at every table",
  "id" : 4662608414117888,
  "created_at" : "2010-11-16 22:30:27 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Old Abram Brown",
      "screen_name" : "oldabrambrown",
      "indices" : [ 15, 29 ],
      "id_str" : "54112377",
      "id" : 54112377
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "4637950977507329",
  "text" : "absolutely. RT @oldabrambrown: Anyone think a duck could eat a whole loaf of bread?",
  "id" : 4637950977507329,
  "created_at" : "2010-11-16 20:52:28 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "AnythingButDoubleDigitsAgain",
      "indices" : [ 41, 70 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "4353633474846720",
  "text" : "Oh snap I lost a follower! Back to 100.  #AnythingButDoubleDigitsAgain",
  "id" : 4353633474846720,
  "created_at" : "2010-11-16 02:02:41 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Old Abram Brown",
      "screen_name" : "oldabrambrown",
      "indices" : [ 0, 14 ],
      "id_str" : "54112377",
      "id" : 54112377
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "4273777126940672",
  "geo" : { },
  "id_str" : "4352793863262208",
  "in_reply_to_user_id" : 54112377,
  "text" : "@oldabrambrown a kick drum?",
  "id" : 4352793863262208,
  "in_reply_to_status_id" : 4273777126940672,
  "created_at" : "2010-11-16 01:59:21 +0000",
  "in_reply_to_screen_name" : "oldabrambrown",
  "in_reply_to_user_id_str" : "54112377",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Liz Fohl",
      "screen_name" : "lizfohl",
      "indices" : [ 0, 8 ],
      "id_str" : "20460448",
      "id" : 20460448
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "4269141741535232",
  "geo" : { },
  "id_str" : "4349778875916288",
  "in_reply_to_user_id" : 20460448,
  "text" : "@lizfohl your twitter picture bozo!",
  "id" : 4349778875916288,
  "in_reply_to_status_id" : 4269141741535232,
  "created_at" : "2010-11-16 01:47:22 +0000",
  "in_reply_to_screen_name" : "lizfohl",
  "in_reply_to_user_id_str" : "20460448",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Liz Fohl",
      "screen_name" : "lizfohl",
      "indices" : [ 0, 8 ],
      "id_str" : "20460448",
      "id" : 20460448
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "4246733202661377",
  "in_reply_to_user_id" : 20460448,
  "text" : "@lizfohl awesome pichture. I guess my service offer wasn't good enough huh?",
  "id" : 4246733202661377,
  "created_at" : "2010-11-15 18:57:54 +0000",
  "in_reply_to_screen_name" : "lizfohl",
  "in_reply_to_user_id_str" : "20460448",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.cloudhopper.com\/\" rel=\"nofollow\"\u003ECloudhopper\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "4236036947320832",
  "text" : "pro tools 9 also has a multitrack beat detective. they really provided a good daw to non HD people at last.",
  "id" : 4236036947320832,
  "created_at" : "2010-11-15 18:15:24 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.cloudhopper.com\/\" rel=\"nofollow\"\u003ECloudhopper\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "3983291921207296",
  "text" : "nyc is crazy. i feel like its a weird collection of strangely disconnected areas that are easy to access but very different",
  "id" : 3983291921207296,
  "created_at" : "2010-11-15 01:31:05 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "3092188724469760",
  "geo" : { },
  "id_str" : "3104619899133952",
  "in_reply_to_user_id" : 19353468,
  "text" : "@xomaddybabiox I feel that",
  "id" : 3104619899133952,
  "in_reply_to_status_id" : 3092188724469760,
  "created_at" : "2010-11-12 15:19:33 +0000",
  "in_reply_to_screen_name" : "maddy_tee",
  "in_reply_to_user_id_str" : "19353468",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.cloudhopper.com\/\" rel=\"nofollow\"\u003ECloudhopper\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "2760493731155969",
  "text" : "now this is some weather",
  "id" : 2760493731155969,
  "created_at" : "2010-11-11 16:32:07 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kate Breton",
      "screen_name" : "katebreton",
      "indices" : [ 0, 11 ],
      "id_str" : "29812449",
      "id" : 29812449
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "2745566928834560",
  "geo" : { },
  "id_str" : "2753874855206912",
  "in_reply_to_user_id" : 29812449,
  "text" : "@katebreton espresso expression I mean",
  "id" : 2753874855206912,
  "in_reply_to_status_id" : 2745566928834560,
  "created_at" : "2010-11-11 16:05:49 +0000",
  "in_reply_to_screen_name" : "katebreton",
  "in_reply_to_user_id_str" : "29812449",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kate Breton",
      "screen_name" : "katebreton",
      "indices" : [ 0, 11 ],
      "id_str" : "29812449",
      "id" : 29812449
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "2745566928834560",
  "geo" : { },
  "id_str" : "2753758949806080",
  "in_reply_to_user_id" : 29812449,
  "text" : "@katebreton straight to the face is the best expression",
  "id" : 2753758949806080,
  "in_reply_to_status_id" : 2745566928834560,
  "created_at" : "2010-11-11 16:05:21 +0000",
  "in_reply_to_screen_name" : "katebreton",
  "in_reply_to_user_id_str" : "29812449",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ron Shpindler",
      "screen_name" : "troneldine",
      "indices" : [ 3, 14 ],
      "id_str" : "19259021",
      "id" : 19259021
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "2467989773357057",
  "text" : "RT @troneldine: No biggy... or no tupac?",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "2465082843865088",
    "text" : "No biggy... or no tupac?",
    "id" : 2465082843865088,
    "created_at" : "2010-11-10 20:58:16 +0000",
    "user" : {
      "name" : "Ron Shpindler",
      "screen_name" : "troneldine",
      "protected" : false,
      "id_str" : "19259021",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/378800000084605942\/0767cd9391f34f12f15e93e52beb548b_normal.jpeg",
      "id" : 19259021,
      "verified" : false
    }
  },
  "id" : 2467989773357057,
  "created_at" : "2010-11-10 21:09:49 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Wayne Twittaker",
      "screen_name" : "WayneTwittaker",
      "indices" : [ 0, 15 ],
      "id_str" : "2300470586",
      "id" : 2300470586
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "2465120332546048",
  "text" : "@WayneTwittaker no! Those meanies.",
  "id" : 2465120332546048,
  "created_at" : "2010-11-10 20:58:25 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.cloudhopper.com\/\" rel=\"nofollow\"\u003ECloudhopper\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "2454933030830080",
  "text" : "the great thing about the apple store is that when they cant understand your problems they pay for them",
  "id" : 2454933030830080,
  "created_at" : "2010-11-10 20:17:56 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "2130563326550016",
  "text" : "So the new kanye album is going to be awesome. Any thoughts on the new kid cudi?",
  "id" : 2130563326550016,
  "created_at" : "2010-11-09 22:49:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "1838670340431872",
  "text" : "I think there's an Apogee Duet in my future.",
  "id" : 1838670340431872,
  "created_at" : "2010-11-09 03:29:07 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.cloudhopper.com\/\" rel=\"nofollow\"\u003ECloudhopper\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "1678301466726401",
  "text" : "which is weirder: fluff or powdered coffee mate?",
  "id" : 1678301466726401,
  "created_at" : "2010-11-08 16:51:52 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "1126847622418432",
  "text" : "okay tonight rocked i think?",
  "id" : 1126847622418432,
  "created_at" : "2010-11-07 04:20:36 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "1123588954390529",
  "text" : "@mmck031 ahahaha. actually, if what i just had was a date, it was awesome I think.",
  "id" : 1123588954390529,
  "created_at" : "2010-11-07 04:07:39 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.cloudhopper.com\/\" rel=\"nofollow\"\u003ECloudhopper\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "1056707916603392",
  "text" : "what constitutes a date exactly?",
  "id" : 1056707916603392,
  "created_at" : "2010-11-06 23:41:53 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.cloudhopper.com\/\" rel=\"nofollow\"\u003ECloudhopper\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "739682988068864",
  "text" : "another celtics nail biter",
  "id" : 739682988068864,
  "created_at" : "2010-11-06 02:42:08 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "667858140921856",
  "text" : "glad i didn't buy pt 8",
  "id" : 667858140921856,
  "created_at" : "2010-11-05 21:56:44 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "667787030695937",
  "text" : "pro tools 9 with delay compensation. finally. oh yeah, and any interface you want. that rox.",
  "id" : 667787030695937,
  "created_at" : "2010-11-05 21:56:27 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "29672572802",
  "text" : "10 days later this cold is almost truly gone.",
  "id" : 29672572802,
  "created_at" : "2010-11-04 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.cloudhopper.com\/\" rel=\"nofollow\"\u003ECloudhopper\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "29425686625",
  "text" : "just went to a free advance screening of due date. dont waste your money",
  "id" : 29425686625,
  "created_at" : "2010-11-02 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "29478045823",
  "text" : "After one more night of putting together dysfunctional and incomplete ikea furniture, my new room will be functional, w or w\/o all parts.",
  "id" : 29478045823,
  "created_at" : "2010-11-02 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "29478147461",
  "text" : "Also I now have 100 followers woop woop y'all rock.",
  "id" : 29478147461,
  "created_at" : "2010-11-02 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
} ]