Grailbird.data.tweets_2013_02 = 
 [ {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "306783083405930497",
  "text" : "Couldn't be more excited to be working in Cyber Systems and Technology at MIT Lincoln Lab this summer.",
  "id" : 306783083405930497,
  "created_at" : "2013-02-27 15:09:21 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "306479117275127808",
  "text" : "Don't have a boredom and creative proliferation problem",
  "id" : 306479117275127808,
  "created_at" : "2013-02-26 19:01:30 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "FabulousAniyyah",
      "screen_name" : "hnycombinator",
      "indices" : [ 19, 33 ],
      "id_str" : "2560186998",
      "id" : 2560186998
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 116, 138 ],
      "url" : "http:\/\/t.co\/7mqNTfEeIM",
      "expanded_url" : "http:\/\/goo.gl\/fb\/UXfk8",
      "display_url" : "goo.gl\/fb\/UXfk8"
    } ]
  },
  "in_reply_to_status_id_str" : "305087200733839360",
  "geo" : { },
  "id_str" : "305106168899051520",
  "in_reply_to_user_id" : 15042473,
  "text" : "Pick me picke me! \u201C@hnycombinator: Google reveals plans for 1.1M square foot, 4 floor \u2018Bay View\u2019 campus near SF Bay http:\/\/t.co\/7mqNTfEeIM\u201D",
  "id" : 305106168899051520,
  "in_reply_to_status_id" : 305087200733839360,
  "created_at" : "2013-02-23 00:05:53 +0000",
  "in_reply_to_screen_name" : "newsycbot",
  "in_reply_to_user_id_str" : "15042473",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "303733127044546561",
  "text" : "A good way to dirty your freshly-washed clothes is to use the bag of them to hold your door in place every night.",
  "id" : 303733127044546561,
  "created_at" : "2013-02-19 05:09:55 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 73, 93 ],
      "url" : "http:\/\/t.co\/Yk7MGR9t",
      "expanded_url" : "http:\/\/goo.gl\/fb\/Rax4N",
      "display_url" : "goo.gl\/fb\/Rax4N"
    } ]
  },
  "geo" : { },
  "id_str" : "300268470032019459",
  "text" : "RT @hnycombinator: The Star Wars Route: Do a traceroute to 216.81.59.173 http:\/\/t.co\/Yk7MGR9t",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.google.com\/\" rel=\"nofollow\"\u003EGoogle\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 54, 74 ],
        "url" : "http:\/\/t.co\/Yk7MGR9t",
        "expanded_url" : "http:\/\/goo.gl\/fb\/Rax4N",
        "display_url" : "goo.gl\/fb\/Rax4N"
      } ]
    },
    "geo" : { },
    "id_str" : "300258141419999235",
    "text" : "The Star Wars Route: Do a traceroute to 216.81.59.173 http:\/\/t.co\/Yk7MGR9t",
    "id" : 300258141419999235,
    "created_at" : "2013-02-09 15:01:34 +0000",
    "user" : {
      "name" : "Hacker News YC",
      "screen_name" : "newsycbot",
      "protected" : false,
      "id_str" : "15042473",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/526110783822774272\/_WLdrxCb_normal.png",
      "id" : 15042473,
      "verified" : false
    }
  },
  "id" : 300268470032019459,
  "created_at" : "2013-02-09 15:42:36 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "297944893102374912",
  "text" : "Homemade mousetrap. Not my first, either. My last one worked so lets hope this one does too.",
  "id" : 297944893102374912,
  "created_at" : "2013-02-03 05:49:32 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Patrick Robertson",
      "screen_name" : "patricksroberts",
      "indices" : [ 0, 16 ],
      "id_str" : "46661605",
      "id" : 46661605
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "297731789735219202",
  "in_reply_to_user_id" : 46661605,
  "text" : "@patricksroberts disabled the vim arrow keys...",
  "id" : 297731789735219202,
  "created_at" : "2013-02-02 15:42:44 +0000",
  "in_reply_to_screen_name" : "patricksroberts",
  "in_reply_to_user_id_str" : "46661605",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
} ]