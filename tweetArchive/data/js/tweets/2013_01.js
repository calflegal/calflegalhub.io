Grailbird.data.tweets_2013_01 = 
 [ {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Hugh MacLeod ",
      "screen_name" : "gapingvoid",
      "indices" : [ 3, 14 ],
      "id_str" : "50193",
      "id" : 50193
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "296345369854480384",
  "text" : "RT @gapingvoid: \"You spend half your life being bored by your job. And the other half telling people how interesting your job is.\"",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.echofon.com\/\" rel=\"nofollow\"\u003EEchofon\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "296343457121857537",
    "text" : "\"You spend half your life being bored by your job. And the other half telling people how interesting your job is.\"",
    "id" : 296343457121857537,
    "created_at" : "2013-01-29 19:46:00 +0000",
    "user" : {
      "name" : "Hugh MacLeod ",
      "screen_name" : "gapingvoid",
      "protected" : false,
      "id_str" : "50193",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/378800000498901173\/fda9cc16f7ba64a24303bd7cd5647edb_normal.jpeg",
      "id" : 50193,
      "verified" : true
    }
  },
  "id" : 296345369854480384,
  "created_at" : "2013-01-29 19:53:36 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "TechCrunch",
      "screen_name" : "TechCrunch",
      "indices" : [ 3, 14 ],
      "id_str" : "816653",
      "id" : 816653
    }, {
      "name" : "Frederic Lardinois",
      "screen_name" : "fredericl",
      "indices" : [ 139, 140 ],
      "id_str" : "2132641",
      "id" : 2132641
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 139, 140 ],
      "url" : "http:\/\/t.co\/wnqB2FlN",
      "expanded_url" : "http:\/\/tcrn.ch\/UxAfoM",
      "display_url" : "tcrn.ch\/UxAfoM"
    } ]
  },
  "geo" : { },
  "id_str" : "295952270137389056",
  "text" : "RT @TechCrunch: Google Makes Using Analytics Easier With New Solution Gallery For Dashboards, Segments &amp; Custom Reports http:\/\/t.co\/ ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/vip.wordpress.com\/hosting\" rel=\"nofollow\"\u003EWordPress.com VIP\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Frederic Lardinois",
        "screen_name" : "fredericl",
        "indices" : [ 132, 142 ],
        "id_str" : "2132641",
        "id" : 2132641
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 108, 128 ],
        "url" : "http:\/\/t.co\/wnqB2FlN",
        "expanded_url" : "http:\/\/tcrn.ch\/UxAfoM",
        "display_url" : "tcrn.ch\/UxAfoM"
      } ]
    },
    "geo" : { },
    "id_str" : "295951942771949568",
    "text" : "Google Makes Using Analytics Easier With New Solution Gallery For Dashboards, Segments &amp; Custom Reports http:\/\/t.co\/wnqB2FlN by @fredericl",
    "id" : 295951942771949568,
    "created_at" : "2013-01-28 17:50:16 +0000",
    "user" : {
      "name" : "TechCrunch",
      "screen_name" : "TechCrunch",
      "protected" : false,
      "id_str" : "816653",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
      "id" : 816653,
      "verified" : true
    }
  },
  "id" : 295952270137389056,
  "created_at" : "2013-01-28 17:51:34 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "xwle",
      "screen_name" : "xwle",
      "indices" : [ 3, 8 ],
      "id_str" : "271765772",
      "id" : 271765772
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "295928207524368384",
  "text" : "RT @xwle: a forest of binary trees",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for iOS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "295920875662884865",
    "text" : "a forest of binary trees",
    "id" : 295920875662884865,
    "created_at" : "2013-01-28 15:46:49 +0000",
    "user" : {
      "name" : "xwle",
      "screen_name" : "xwle",
      "protected" : false,
      "id_str" : "271765772",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/457026540006813696\/9G4qxmMt_normal.jpeg",
      "id" : 271765772,
      "verified" : false
    }
  },
  "id" : 295928207524368384,
  "created_at" : "2013-01-28 16:15:57 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "294953030401531904",
  "text" : "The first clip of jOBS is bad. Really bad.",
  "id" : 294953030401531904,
  "created_at" : "2013-01-25 23:40:57 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "294878563205775360",
  "text" : "Say what you want but the Verizon LTE network gets things done.",
  "id" : 294878563205775360,
  "created_at" : "2013-01-25 18:45:02 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dan Shipper",
      "screen_name" : "danshipper",
      "indices" : [ 1, 12 ],
      "id_str" : "19829693",
      "id" : 19829693
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 71 ],
      "url" : "http:\/\/t.co\/aGjGZXCv",
      "expanded_url" : "http:\/\/buff.ly\/13UHMlk",
      "display_url" : "buff.ly\/13UHMlk"
    } ]
  },
  "geo" : { },
  "id_str" : "293726770216181760",
  "text" : "\u201C@danshipper: Powerful stuff: You Are Going to Die http:\/\/t.co\/aGjGZXCv\u201D amazing writing too.",
  "id" : 293726770216181760,
  "created_at" : "2013-01-22 14:28:13 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "292365015808352256",
  "text" : "I'm not sure what I hate more than these in-app webviews. It's not the same just get me to Safari.",
  "id" : 292365015808352256,
  "created_at" : "2013-01-18 20:17:06 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "H. Scott Flegal",
      "screen_name" : "hscottflegal",
      "indices" : [ 125, 138 ],
      "id_str" : "168505798",
      "id" : 168505798
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 103, 123 ],
      "url" : "http:\/\/t.co\/b6X45iGC",
      "expanded_url" : "http:\/\/www.nashuatelegraph.com\/news\/990498-469\/science-cafe-nhs-nashua-debut-big-hit.html",
      "display_url" : "nashuatelegraph.com\/news\/990498-46\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "291909720095203330",
  "text" : "If you weren't at Science Cafe's Nashua debut last nightt: 100 people came to learn about 3D printing. http:\/\/t.co\/b6X45iGC\u201D @hscottflegal",
  "id" : 291909720095203330,
  "created_at" : "2013-01-17 14:07:55 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Remy Bach",
      "screen_name" : "remybach",
      "indices" : [ 130, 139 ],
      "id_str" : "21319665",
      "id" : 21319665
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 104, 125 ],
      "url" : "https:\/\/t.co\/NzPPFYWJ",
      "expanded_url" : "https:\/\/github.com\/jf8073\/sublime-snake",
      "display_url" : "github.com\/jf8073\/sublime\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "291909206251040768",
  "text" : "Might need to use ST2 now. Want to eat all your code in frustration? With SublimeText 2 Snake, you can. https:\/\/t.co\/NzPPFYWJ \/ht @remybach\u201D",
  "id" : 291909206251040768,
  "created_at" : "2013-01-17 14:05:52 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eric Bolton",
      "screen_name" : "walkbolton",
      "indices" : [ 51, 62 ],
      "id_str" : "121598461",
      "id" : 121598461
    }, {
      "name" : "Wayne Twittaker",
      "screen_name" : "WayneTwittaker",
      "indices" : [ 63, 78 ],
      "id_str" : "2300470586",
      "id" : 2300470586
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "291903592917512193",
  "text" : "Got a new robe with HCF embroidered look out world @walkbolton @WayneTwittaker",
  "id" : 291903592917512193,
  "created_at" : "2013-01-17 13:43:34 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christopher Sopher",
      "screen_name" : "cksopher",
      "indices" : [ 3, 12 ],
      "id_str" : "17642729",
      "id" : 17642729
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 139, 140 ],
      "url" : "http:\/\/t.co\/Ng78bLB3",
      "expanded_url" : "http:\/\/thinkprogress.org\/justice\/2013\/01\/14\/1441211\/killers-slavers-and-bank-robbers-all-face-less-severe-prison-terms-than-aaron-swartz-did\/?mobile=nc",
      "display_url" : "thinkprogress.org\/justice\/2013\/0\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "291572445809344513",
  "text" : "RT @cksopher: Aaron Swartz faced longer prison term than people who rob banks, sell slaves or help terrorists build nuclear weapons. htt ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/software\/tweetbot\/mac\" rel=\"nofollow\"\u003ETweetbot for Mac\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 119, 139 ],
        "url" : "http:\/\/t.co\/Ng78bLB3",
        "expanded_url" : "http:\/\/thinkprogress.org\/justice\/2013\/01\/14\/1441211\/killers-slavers-and-bank-robbers-all-face-less-severe-prison-terms-than-aaron-swartz-did\/?mobile=nc",
        "display_url" : "thinkprogress.org\/justice\/2013\/0\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "291570301555331074",
    "text" : "Aaron Swartz faced longer prison term than people who rob banks, sell slaves or help terrorists build nuclear weapons. http:\/\/t.co\/Ng78bLB3",
    "id" : 291570301555331074,
    "created_at" : "2013-01-16 15:39:11 +0000",
    "user" : {
      "name" : "Christopher Sopher",
      "screen_name" : "cksopher",
      "protected" : false,
      "id_str" : "17642729",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/378800000054882341\/b55499bb1cab139131eac51a6ba4159c_normal.jpeg",
      "id" : 17642729,
      "verified" : false
    }
  },
  "id" : 291572445809344513,
  "created_at" : "2013-01-16 15:47:42 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "291042945640062977",
  "text" : "Also I hate that about.me is all cluttered with ads &amp; bs now. I'll move my homepage away soon.",
  "id" : 291042945640062977,
  "created_at" : "2013-01-15 04:43:40 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "291042589157752832",
  "text" : "english version: being mindful of all things organizational pays off eventually.",
  "id" : 291042589157752832,
  "created_at" : "2013-01-15 04:42:15 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "291041937635565569",
  "text" : "Also, ALWAYS keep your designer mockups and assets, just bailed me out in a migration.",
  "id" : 291041937635565569,
  "created_at" : "2013-01-15 04:39:39 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "291041477440724992",
  "text" : "It's official I'm all moved off my shared host (even outdated stuff).",
  "id" : 291041477440724992,
  "created_at" : "2013-01-15 04:37:50 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "http:\/\/twitter.com\/calflegal\/status\/290129363767263233\/photo\/1",
      "indices" : [ 12, 32 ],
      "url" : "http:\/\/t.co\/8tlOBi2c",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/BAa_HoKCAAADceV.jpg",
      "id_str" : "290129363771457536",
      "id" : 290129363771457536,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/BAa_HoKCAAADceV.jpg",
      "sizes" : [ {
        "h" : 255,
        "resize" : "fit",
        "w" : 340
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 450,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      } ],
      "display_url" : "pic.twitter.com\/8tlOBi2c"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "290129363767263233",
  "text" : "These guys! http:\/\/t.co\/8tlOBi2c",
  "id" : 290129363767263233,
  "created_at" : "2013-01-12 16:13:25 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ryan Foote",
      "screen_name" : "footedesign",
      "indices" : [ 0, 12 ],
      "id_str" : "48887172",
      "id" : 48887172
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "289123272241590272",
  "geo" : { },
  "id_str" : "289123634491035648",
  "in_reply_to_user_id" : 48887172,
  "text" : "@footedesign I'm with you though ruby is the jam",
  "id" : 289123634491035648,
  "in_reply_to_status_id" : 289123272241590272,
  "created_at" : "2013-01-09 21:37:00 +0000",
  "in_reply_to_screen_name" : "footedesign",
  "in_reply_to_user_id_str" : "48887172",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ryan Foote",
      "screen_name" : "footedesign",
      "indices" : [ 0, 12 ],
      "id_str" : "48887172",
      "id" : 48887172
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "289123272241590272",
  "geo" : { },
  "id_str" : "289123589028978689",
  "in_reply_to_user_id" : 48887172,
  "text" : "@footedesign nice. I'm not blogging right now but I want to keep my little music blog out there as it shows my interests.",
  "id" : 289123589028978689,
  "in_reply_to_status_id" : 289123272241590272,
  "created_at" : "2013-01-09 21:36:49 +0000",
  "in_reply_to_screen_name" : "footedesign",
  "in_reply_to_user_id_str" : "48887172",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ryan Foote",
      "screen_name" : "footedesign",
      "indices" : [ 0, 12 ],
      "id_str" : "48887172",
      "id" : 48887172
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "289122718824144896",
  "geo" : { },
  "id_str" : "289123136497123328",
  "in_reply_to_user_id" : 48887172,
  "text" : "@footedesign Although I'm pretty bored of wordpress and php.",
  "id" : 289123136497123328,
  "in_reply_to_status_id" : 289122718824144896,
  "created_at" : "2013-01-09 21:35:02 +0000",
  "in_reply_to_screen_name" : "footedesign",
  "in_reply_to_user_id_str" : "48887172",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ryan Foote",
      "screen_name" : "footedesign",
      "indices" : [ 0, 12 ],
      "id_str" : "48887172",
      "id" : 48887172
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "289122718824144896",
  "geo" : { },
  "id_str" : "289123050899791872",
  "in_reply_to_user_id" : 48887172,
  "text" : "@footedesign I'll need to call you soon about migrating some sites to wordpress.",
  "id" : 289123050899791872,
  "in_reply_to_status_id" : 289122718824144896,
  "created_at" : "2013-01-09 21:34:41 +0000",
  "in_reply_to_screen_name" : "footedesign",
  "in_reply_to_user_id_str" : "48887172",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ryan Foote",
      "screen_name" : "footedesign",
      "indices" : [ 0, 12 ],
      "id_str" : "48887172",
      "id" : 48887172
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "289122718824144896",
  "geo" : { },
  "id_str" : "289122922721849344",
  "in_reply_to_user_id" : 48887172,
  "text" : "@footedesign Well for now its just my site and stuff. It's actually pretty easy to do wordpress on heroku too",
  "id" : 289122922721849344,
  "in_reply_to_status_id" : 289122718824144896,
  "created_at" : "2013-01-09 21:34:11 +0000",
  "in_reply_to_screen_name" : "footedesign",
  "in_reply_to_user_id_str" : "48887172",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "CloudFlare",
      "screen_name" : "CloudFlare",
      "indices" : [ 55, 66 ],
      "id_str" : "32499999",
      "id" : 32499999
    }, {
      "name" : "Heroku",
      "screen_name" : "heroku",
      "indices" : [ 71, 78 ],
      "id_str" : "10257182",
      "id" : 10257182
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "289120696431435776",
  "text" : "For better or worse I'm basically selling my e-soul to @CloudFlare and @heroku",
  "id" : 289120696431435776,
  "created_at" : "2013-01-09 21:25:20 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "289119009985667073",
  "text" : "So close to being fully liberated from shared hosting and the 90's internet.",
  "id" : 289119009985667073,
  "created_at" : "2013-01-09 21:18:38 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "CloudFlare",
      "screen_name" : "CloudFlare",
      "indices" : [ 8, 19 ],
      "id_str" : "32499999",
      "id" : 32499999
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "288878564017831937",
  "text" : "I &lt;3 @cloudflare",
  "id" : 288878564017831937,
  "created_at" : "2013-01-09 05:23:11 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "pourmecoffee",
      "screen_name" : "pourmecoffee",
      "indices" : [ 3, 16 ],
      "id_str" : "16906137",
      "id" : 16906137
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "288483483356127232",
  "text" : "RT @pourmecoffee: Halftime is just twenty minutes of Brent Musburger looking at Playboys and telling everyone who he likes.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "288469872181141504",
    "text" : "Halftime is just twenty minutes of Brent Musburger looking at Playboys and telling everyone who he likes.",
    "id" : 288469872181141504,
    "created_at" : "2013-01-08 02:19:11 +0000",
    "user" : {
      "name" : "pourmecoffee",
      "screen_name" : "pourmecoffee",
      "protected" : false,
      "id_str" : "16906137",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/528514130257657856\/Ki8MgmBV_normal.jpeg",
      "id" : 16906137,
      "verified" : true
    }
  },
  "id" : 288483483356127232,
  "created_at" : "2013-01-08 03:13:16 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Philip Gourevitch",
      "screen_name" : "PGourevitch",
      "indices" : [ 3, 15 ],
      "id_str" : "340441440",
      "id" : 340441440
    }, {
      "name" : "Lily",
      "screen_name" : "LilyLivingstone",
      "indices" : [ 57, 73 ],
      "id_str" : "328159411",
      "id" : 328159411
    } ],
    "media" : [ {
      "expanded_url" : "http:\/\/twitter.com\/sbadsgood\/status\/287718835254067200\/photo\/1",
      "indices" : [ 139, 140 ],
      "url" : "http:\/\/t.co\/luU74omi",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/A_4uwZsCAAAcm1m.jpg",
      "id_str" : "287718835262455808",
      "id" : 287718835262455808,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/A_4uwZsCAAAcm1m.jpg",
      "sizes" : [ {
        "h" : 451,
        "resize" : "fit",
        "w" : 340
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 700,
        "resize" : "fit",
        "w" : 527
      }, {
        "h" : 700,
        "resize" : "fit",
        "w" : 527
      }, {
        "h" : 700,
        "resize" : "fit",
        "w" : 527
      } ],
      "display_url" : "pic.twitter.com\/luU74omi"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "287958593481879552",
  "text" : "RT @PGourevitch: How our gadgets will look in 57 yrs? MT @LilyLivingstone: 5 megabyte hard drive from 1956, being forklifted onto plane. ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Lily",
        "screen_name" : "LilyLivingstone",
        "indices" : [ 40, 56 ],
        "id_str" : "328159411",
        "id" : 328159411
      } ],
      "media" : [ {
        "expanded_url" : "http:\/\/twitter.com\/sbadsgood\/status\/287718835254067200\/photo\/1",
        "indices" : [ 120, 140 ],
        "url" : "http:\/\/t.co\/luU74omi",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/A_4uwZsCAAAcm1m.jpg",
        "id_str" : "287718835262455808",
        "id" : 287718835262455808,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/A_4uwZsCAAAcm1m.jpg",
        "sizes" : [ {
          "h" : 451,
          "resize" : "fit",
          "w" : 340
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 700,
          "resize" : "fit",
          "w" : 527
        }, {
          "h" : 700,
          "resize" : "fit",
          "w" : 527
        }, {
          "h" : 700,
          "resize" : "fit",
          "w" : 527
        } ],
        "display_url" : "pic.twitter.com\/luU74omi"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "287935670348038144",
    "text" : "How our gadgets will look in 57 yrs? MT @LilyLivingstone: 5 megabyte hard drive from 1956, being forklifted onto plane. http:\/\/t.co\/luU74omi",
    "id" : 287935670348038144,
    "created_at" : "2013-01-06 14:56:28 +0000",
    "user" : {
      "name" : "Philip Gourevitch",
      "screen_name" : "PGourevitch",
      "protected" : false,
      "id_str" : "340441440",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/453433288485781504\/6DWyq4SV_normal.jpeg",
      "id" : 340441440,
      "verified" : false
    }
  },
  "id" : 287958593481879552,
  "created_at" : "2013-01-06 16:27:33 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
} ]