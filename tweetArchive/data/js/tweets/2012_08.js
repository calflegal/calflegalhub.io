Grailbird.data.tweets_2012_08 = 
 [ {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "stephdub",
      "screen_name" : "stephdub",
      "indices" : [ 0, 9 ],
      "id_str" : "14936359",
      "id" : 14936359
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "241371844379295745",
  "in_reply_to_user_id" : 14936359,
  "text" : "@stephdub what's the word on your project?",
  "id" : 241371844379295745,
  "created_at" : "2012-08-31 03:08:26 +0000",
  "in_reply_to_screen_name" : "stephdub",
  "in_reply_to_user_id_str" : "14936359",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Simon Sarris",
      "screen_name" : "simonsarris",
      "indices" : [ 0, 12 ],
      "id_str" : "66761201",
      "id" : 66761201
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "239597545179975680",
  "in_reply_to_user_id" : 66761201,
  "text" : "@simonsarris Y Combinator popular tweet led me to you! Congrats!",
  "id" : 239597545179975680,
  "created_at" : "2012-08-26 05:38:00 +0000",
  "in_reply_to_screen_name" : "simonsarris",
  "in_reply_to_user_id_str" : "66761201",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bob Lefsetz",
      "screen_name" : "Lefsetz",
      "indices" : [ 3, 11 ],
      "id_str" : "27702153",
      "id" : 27702153
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 46, 66 ],
      "url" : "http:\/\/t.co\/mEY46485",
      "expanded_url" : "http:\/\/nyti.ms\/R2AAtu",
      "display_url" : "nyti.ms\/R2AAtu"
    } ]
  },
  "geo" : { },
  "id_str" : "239217001288052736",
  "text" : "RT @Lefsetz: Buying Their Way to Twitter Fame http:\/\/t.co\/mEY46485",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 33, 53 ],
        "url" : "http:\/\/t.co\/mEY46485",
        "expanded_url" : "http:\/\/nyti.ms\/R2AAtu",
        "display_url" : "nyti.ms\/R2AAtu"
      } ]
    },
    "geo" : { },
    "id_str" : "239181703250325505",
    "text" : "Buying Their Way to Twitter Fame http:\/\/t.co\/mEY46485",
    "id" : 239181703250325505,
    "created_at" : "2012-08-25 02:05:36 +0000",
    "user" : {
      "name" : "Bob Lefsetz",
      "screen_name" : "Lefsetz",
      "protected" : false,
      "id_str" : "27702153",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/127502013\/author_normal.jpg",
      "id" : 27702153,
      "verified" : true
    }
  },
  "id" : 239217001288052736,
  "created_at" : "2012-08-25 04:25:52 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "239216884338270208",
  "text" : "If Status People shows that you have less than 50% good followers, I'm thinking you paid for it.",
  "id" : 239216884338270208,
  "created_at" : "2012-08-25 04:25:24 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "239215559781277696",
  "text" : "StatusPeople is a great tool for seeing how many fake followers somebody has. It can be astonishing.",
  "id" : 239215559781277696,
  "created_at" : "2012-08-25 04:20:08 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "238694508261421057",
  "text" : "Yup it's much better! Update your Facebook app iPhone \/ iPad people!",
  "id" : 238694508261421057,
  "created_at" : "2012-08-23 17:49:40 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "238693631299555328",
  "text" : "New Facebook iOS app. I'm guessing it's way way faster.",
  "id" : 238693631299555328,
  "created_at" : "2012-08-23 17:46:11 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "H. Scott Flegal",
      "screen_name" : "hscottflegal",
      "indices" : [ 0, 13 ],
      "id_str" : "168505798",
      "id" : 168505798
    }, {
      "name" : "Justin Koscher",
      "screen_name" : "koscherj1984",
      "indices" : [ 14, 27 ],
      "id_str" : "168219396",
      "id" : 168219396
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "238644080714846209",
  "geo" : { },
  "id_str" : "238644328627597312",
  "in_reply_to_user_id" : 168505798,
  "text" : "@hscottflegal @koscherj1984 haha you forgot a space and your tweet barely worked!",
  "id" : 238644328627597312,
  "in_reply_to_status_id" : 238644080714846209,
  "created_at" : "2012-08-23 14:30:16 +0000",
  "in_reply_to_screen_name" : "hscottflegal",
  "in_reply_to_user_id_str" : "168505798",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "PayPal",
      "screen_name" : "PayPal",
      "indices" : [ 18, 25 ],
      "id_str" : "30018058",
      "id" : 30018058
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "238638591901851648",
  "text" : "I really like the @paypal homepage look and feel. That is all.",
  "id" : 238638591901851648,
  "created_at" : "2012-08-23 14:07:28 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "stephdub",
      "screen_name" : "stephdub",
      "indices" : [ 0, 9 ],
      "id_str" : "14936359",
      "id" : 14936359
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "238331270197833728",
  "geo" : { },
  "id_str" : "238331483486556162",
  "in_reply_to_user_id" : 14936359,
  "text" : "@stephdub I am not yet no. Very cool service but haven't worked with it much in detail.",
  "id" : 238331483486556162,
  "in_reply_to_status_id" : 238331270197833728,
  "created_at" : "2012-08-22 17:47:08 +0000",
  "in_reply_to_screen_name" : "stephdub",
  "in_reply_to_user_id_str" : "14936359",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "stephdub",
      "screen_name" : "stephdub",
      "indices" : [ 0, 9 ],
      "id_str" : "14936359",
      "id" : 14936359
    }, {
      "name" : "Miranda Degnan",
      "screen_name" : "ay_dios_miranda",
      "indices" : [ 10, 26 ],
      "id_str" : "36298099",
      "id" : 36298099
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "238329905488740352",
  "geo" : { },
  "id_str" : "238330202541940736",
  "in_reply_to_user_id" : 14936359,
  "text" : "@stephdub @ay_dios_miranda well nobody likes saying only dev but yes I mainly dev.",
  "id" : 238330202541940736,
  "in_reply_to_status_id" : 238329905488740352,
  "created_at" : "2012-08-22 17:42:02 +0000",
  "in_reply_to_screen_name" : "stephdub",
  "in_reply_to_user_id_str" : "14936359",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "stephdub",
      "screen_name" : "stephdub",
      "indices" : [ 0, 9 ],
      "id_str" : "14936359",
      "id" : 14936359
    }, {
      "name" : "Miranda Degnan",
      "screen_name" : "ay_dios_miranda",
      "indices" : [ 10, 26 ],
      "id_str" : "36298099",
      "id" : 36298099
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "238127401178124289",
  "geo" : { },
  "id_str" : "238252131671478272",
  "in_reply_to_user_id" : 14936359,
  "text" : "@stephdub @ay_dios_miranda yes yes Miranda's my favorite!",
  "id" : 238252131671478272,
  "in_reply_to_status_id" : 238127401178124289,
  "created_at" : "2012-08-22 12:31:49 +0000",
  "in_reply_to_screen_name" : "stephdub",
  "in_reply_to_user_id_str" : "14936359",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "D. Keith Robinson",
      "screen_name" : "dkr",
      "indices" : [ 0, 4 ],
      "id_str" : "10877",
      "id" : 10877
    }, {
      "name" : "stephdub",
      "screen_name" : "stephdub",
      "indices" : [ 5, 14 ],
      "id_str" : "14936359",
      "id" : 14936359
    }, {
      "name" : "SoundCloud",
      "screen_name" : "SoundCloud",
      "indices" : [ 15, 26 ],
      "id_str" : "5943942",
      "id" : 5943942
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "237951928288563200",
  "geo" : { },
  "id_str" : "237986646480523264",
  "in_reply_to_user_id" : 10877,
  "text" : "@dkr @stephdub @soundcloud what kind of project? feel free to track down my email or reply \/ DM",
  "id" : 237986646480523264,
  "in_reply_to_status_id" : 237951928288563200,
  "created_at" : "2012-08-21 18:56:52 +0000",
  "in_reply_to_screen_name" : "dkr",
  "in_reply_to_user_id_str" : "10877",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "237583555990024192",
  "text" : "StackOverflow is easily the most important website of my generation that 98% of people will never have heard of.",
  "id" : 237583555990024192,
  "created_at" : "2012-08-20 16:15:08 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mashable",
      "screen_name" : "mashable",
      "indices" : [ 3, 12 ],
      "id_str" : "972651",
      "id" : 972651
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 60, 80 ],
      "url" : "http:\/\/t.co\/NOu1SA49",
      "expanded_url" : "http:\/\/on.mash.to\/TOwRnl",
      "display_url" : "on.mash.to\/TOwRnl"
    } ]
  },
  "geo" : { },
  "id_str" : "237388719299108864",
  "text" : "RT @mashable: Fifty Shades Generator Spices Up Dummy Text - http:\/\/t.co\/NOu1SA49",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 46, 66 ],
        "url" : "http:\/\/t.co\/NOu1SA49",
        "expanded_url" : "http:\/\/on.mash.to\/TOwRnl",
        "display_url" : "on.mash.to\/TOwRnl"
      } ]
    },
    "geo" : { },
    "id_str" : "237373272067547136",
    "text" : "Fifty Shades Generator Spices Up Dummy Text - http:\/\/t.co\/NOu1SA49",
    "id" : 237373272067547136,
    "created_at" : "2012-08-20 02:19:32 +0000",
    "user" : {
      "name" : "Mashable",
      "screen_name" : "mashable",
      "protected" : false,
      "id_str" : "972651",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/528344637757337600\/uD0-5J7h_normal.png",
      "id" : 972651,
      "verified" : true
    }
  },
  "id" : 237388719299108864,
  "created_at" : "2012-08-20 03:20:55 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Michelle Flegal",
      "screen_name" : "sheilapuorto",
      "indices" : [ 3, 16 ],
      "id_str" : "700111027",
      "id" : 700111027
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "234284694760484864",
  "text" : "RT @sheilapuorto: mitt robme",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "233749339384713218",
    "text" : "mitt robme",
    "id" : 233749339384713218,
    "created_at" : "2012-08-10 02:19:20 +0000",
    "user" : {
      "name" : "Michelle Flegal",
      "screen_name" : "sheilapuorto",
      "protected" : true,
      "id_str" : "700111027",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2404456982\/d33enpsw3mfkauat24vm_normal.jpeg",
      "id" : 700111027,
      "verified" : false
    }
  },
  "id" : 234284694760484864,
  "created_at" : "2012-08-11 13:46:38 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Michelle Flegal",
      "screen_name" : "sheilapuorto",
      "indices" : [ 3, 16 ],
      "id_str" : "700111027",
      "id" : 700111027
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "234284681330307073",
  "text" : "RT @sheilapuorto: Who are all these jamokes I am following?",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "230504417617645568",
    "text" : "Who are all these jamokes I am following?",
    "id" : 230504417617645568,
    "created_at" : "2012-08-01 03:25:10 +0000",
    "user" : {
      "name" : "Michelle Flegal",
      "screen_name" : "sheilapuorto",
      "protected" : true,
      "id_str" : "700111027",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2404456982\/d33enpsw3mfkauat24vm_normal.jpeg",
      "id" : 700111027,
      "verified" : false
    }
  },
  "id" : 234284681330307073,
  "created_at" : "2012-08-11 13:46:35 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "http:\/\/twitter.com\/calflegal\/status\/233920713856278528\/photo\/1",
      "indices" : [ 100, 120 ],
      "url" : "http:\/\/t.co\/YU7zFGnG",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Az8NpmCCYAAE2UT.jpg",
      "id_str" : "233920713881444352",
      "id" : 233920713881444352,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Az8NpmCCYAAE2UT.jpg",
      "sizes" : [ {
        "h" : 453,
        "resize" : "fit",
        "w" : 340
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 800,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      } ],
      "display_url" : "pic.twitter.com\/YU7zFGnG"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "233920713856278528",
  "text" : "Okay the nickel was child's play but the quarter performance wasn't half bad if I do say so myself. http:\/\/t.co\/YU7zFGnG",
  "id" : 233920713856278528,
  "created_at" : "2012-08-10 13:40:19 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Scott Raynor",
      "screen_name" : "bunchofguy",
      "indices" : [ 3, 14 ],
      "id_str" : "169118836",
      "id" : 169118836
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "233916212864753664",
  "text" : "RT @bunchofguy: Be honest with yourself: You don't watch \"Shark Week.\"",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "233740938860711936",
    "text" : "Be honest with yourself: You don't watch \"Shark Week.\"",
    "id" : 233740938860711936,
    "created_at" : "2012-08-10 01:45:57 +0000",
    "user" : {
      "name" : "Scott Raynor",
      "screen_name" : "bunchofguy",
      "protected" : true,
      "id_str" : "169118836",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1840045387\/John-Havlicek_normal.jpg",
      "id" : 169118836,
      "verified" : false
    }
  },
  "id" : 233916212864753664,
  "created_at" : "2012-08-10 13:22:25 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Stereogum",
      "screen_name" : "stereogum",
      "indices" : [ 3, 13 ],
      "id_str" : "5135521",
      "id" : 5135521
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 75, 95 ],
      "url" : "http:\/\/t.co\/PXkYSyxW",
      "expanded_url" : "http:\/\/bit.ly\/MwnoMp",
      "display_url" : "bit.ly\/MwnoMp"
    } ]
  },
  "geo" : { },
  "id_str" : "233270104639561729",
  "text" : "RT @stereogum: Beck's new album will only be released in sheet-music form. http:\/\/t.co\/PXkYSyxW",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 60, 80 ],
        "url" : "http:\/\/t.co\/PXkYSyxW",
        "expanded_url" : "http:\/\/bit.ly\/MwnoMp",
        "display_url" : "bit.ly\/MwnoMp"
      } ]
    },
    "geo" : { },
    "id_str" : "233260472240050177",
    "text" : "Beck's new album will only be released in sheet-music form. http:\/\/t.co\/PXkYSyxW",
    "id" : 233260472240050177,
    "created_at" : "2012-08-08 17:56:45 +0000",
    "user" : {
      "name" : "Stereogum",
      "screen_name" : "stereogum",
      "protected" : false,
      "id_str" : "5135521",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/726880073\/stereogum-twitter-icon_normal.jpg",
      "id" : 5135521,
      "verified" : true
    }
  },
  "id" : 233270104639561729,
  "created_at" : "2012-08-08 18:35:01 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Old Abram Brown",
      "screen_name" : "oldabrambrown",
      "indices" : [ 3, 17 ],
      "id_str" : "54112377",
      "id" : 54112377
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "231853587167338496",
  "text" : "RT @oldabrambrown: We're playing Pianos in Manhattan tonight at 8 sharp so get there early!",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.cloudhopper.com\/\" rel=\"nofollow\"\u003ECloudhopper\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "231837388194844673",
    "text" : "We're playing Pianos in Manhattan tonight at 8 sharp so get there early!",
    "id" : 231837388194844673,
    "created_at" : "2012-08-04 19:41:55 +0000",
    "user" : {
      "name" : "Old Abram Brown",
      "screen_name" : "oldabrambrown",
      "protected" : false,
      "id_str" : "54112377",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3253803463\/d96f11c379f8a335d92731931813c135_normal.jpeg",
      "id" : 54112377,
      "verified" : false
    }
  },
  "id" : 231853587167338496,
  "created_at" : "2012-08-04 20:46:17 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "scott vener",
      "screen_name" : "brokemogul",
      "indices" : [ 0, 11 ],
      "id_str" : "19543777",
      "id" : 19543777
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "231852156972568576",
  "geo" : { },
  "id_str" : "231853034647457792",
  "in_reply_to_user_id" : 19543777,
  "text" : "@brokemogul no generally not. Mac can read certain of formats but pc won't generally without macdrive or similar.",
  "id" : 231853034647457792,
  "in_reply_to_status_id" : 231852156972568576,
  "created_at" : "2012-08-04 20:44:05 +0000",
  "in_reply_to_screen_name" : "brokemogul",
  "in_reply_to_user_id_str" : "19543777",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 3, 11 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 69, 89 ],
      "url" : "http:\/\/t.co\/Oz8qAInK",
      "expanded_url" : "http:\/\/yfrog.com\/h62kbup",
      "display_url" : "yfrog.com\/h62kbup"
    } ]
  },
  "geo" : { },
  "id_str" : "231467324002160640",
  "text" : "RT @codepo8: Just when I thought I got tired of fails\u2026  Pahahahahaa\n http:\/\/t.co\/Oz8qAInK",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/itunes.apple.com\/us\/app\/twitter\/id409789998?mt=12\" rel=\"nofollow\"\u003ETwitter for Mac\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 56, 76 ],
        "url" : "http:\/\/t.co\/Oz8qAInK",
        "expanded_url" : "http:\/\/yfrog.com\/h62kbup",
        "display_url" : "yfrog.com\/h62kbup"
      } ]
    },
    "geo" : { },
    "id_str" : "231428525691920386",
    "text" : "Just when I thought I got tired of fails\u2026  Pahahahahaa\n http:\/\/t.co\/Oz8qAInK",
    "id" : 231428525691920386,
    "created_at" : "2012-08-03 16:37:14 +0000",
    "user" : {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "protected" : false,
      "id_str" : "13567",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1666904408\/codepo8_normal.png",
      "id" : 13567,
      "verified" : false
    }
  },
  "id" : 231467324002160640,
  "created_at" : "2012-08-03 19:11:25 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 3, 11 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "231353518114607104",
  "text" : "RT @codepo8: Earth without Art is just Eh.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/itunes.apple.com\/us\/app\/twitter\/id409789998?mt=12\" rel=\"nofollow\"\u003ETwitter for Mac\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "231224745729863681",
    "text" : "Earth without Art is just Eh.",
    "id" : 231224745729863681,
    "created_at" : "2012-08-03 03:07:29 +0000",
    "user" : {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "protected" : false,
      "id_str" : "13567",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1666904408\/codepo8_normal.png",
      "id" : 13567,
      "verified" : false
    }
  },
  "id" : 231353518114607104,
  "created_at" : "2012-08-03 11:39:11 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "231193991490719744",
  "text" : "I don't go to the bar nearly enough. Real nice now and then. Not that Saturday nonsense.",
  "id" : 231193991490719744,
  "created_at" : "2012-08-03 01:05:17 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "giles",
      "screen_name" : "gilesgoatboy",
      "indices" : [ 14, 27 ],
      "id_str" : "1341781",
      "id" : 1341781
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 108, 128 ],
      "url" : "http:\/\/t.co\/3uzrDuaJ",
      "expanded_url" : "http:\/\/www.extremetech.com\/computing\/133448-black-hat-hacker-gains-access-to-4-million-hotel-rooms-with-arduino-microcontroller",
      "display_url" : "extremetech.com\/computing\/1334\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "231169040272457728",
  "text" : "holy smokes! \u201C@gilesgoatboy: jesus! hacker gains access to 4 million hotel rooms with virtually zero effort http:\/\/t.co\/3uzrDuaJ\u201D",
  "id" : 231169040272457728,
  "created_at" : "2012-08-02 23:26:08 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Nina Bean",
      "screen_name" : "Nina_bean11",
      "indices" : [ 0, 12 ],
      "id_str" : "381821052",
      "id" : 381821052
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "wat",
      "indices" : [ 13, 17 ]
    } ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "231163469448237056",
  "geo" : { },
  "id_str" : "231166652841066496",
  "in_reply_to_user_id" : 381821052,
  "text" : "@Nina_bean11 #wat",
  "id" : 231166652841066496,
  "in_reply_to_status_id" : 231163469448237056,
  "created_at" : "2012-08-02 23:16:39 +0000",
  "in_reply_to_screen_name" : "Nina_bean11",
  "in_reply_to_user_id_str" : "381821052",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
} ]