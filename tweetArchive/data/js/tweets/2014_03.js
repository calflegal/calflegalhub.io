Grailbird.data.tweets_2014_03 = 
 [ {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 76, 98 ],
      "url" : "http:\/\/t.co\/BT1FnDXpR9",
      "expanded_url" : "http:\/\/reut.rs\/1hpYAIr",
      "display_url" : "reut.rs\/1hpYAIr"
    }, {
      "indices" : [ 105, 127 ],
      "url" : "http:\/\/t.co\/QGprgCmzDO",
      "expanded_url" : "http:\/\/bit.ly\/1iQgkMU",
      "display_url" : "bit.ly\/1iQgkMU"
    } ]
  },
  "geo" : { },
  "id_str" : "450581491534557184",
  "text" : "RT @hnycombinator: U.S. stock markets are rigged, says author Michael Lewis http:\/\/t.co\/BT1FnDXpR9 (cmts http:\/\/t.co\/QGprgCmzDO)",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 57, 79 ],
        "url" : "http:\/\/t.co\/BT1FnDXpR9",
        "expanded_url" : "http:\/\/reut.rs\/1hpYAIr",
        "display_url" : "reut.rs\/1hpYAIr"
      }, {
        "indices" : [ 86, 108 ],
        "url" : "http:\/\/t.co\/QGprgCmzDO",
        "expanded_url" : "http:\/\/bit.ly\/1iQgkMU",
        "display_url" : "bit.ly\/1iQgkMU"
      } ]
    },
    "geo" : { },
    "id_str" : "450475127822893056",
    "text" : "U.S. stock markets are rigged, says author Michael Lewis http:\/\/t.co\/BT1FnDXpR9 (cmts http:\/\/t.co\/QGprgCmzDO)",
    "id" : 450475127822893056,
    "created_at" : "2014-03-31 03:30:14 +0000",
    "user" : {
      "name" : "Hacker News YC",
      "screen_name" : "newsycbot",
      "protected" : false,
      "id_str" : "15042473",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/526110783822774272\/_WLdrxCb_normal.png",
      "id" : 15042473,
      "verified" : false
    }
  },
  "id" : 450581491534557184,
  "created_at" : "2014-03-31 10:32:53 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dave Arel",
      "screen_name" : "davearel",
      "indices" : [ 3, 12 ],
      "id_str" : "460481487",
      "id" : 460481487
    } ],
    "media" : [ {
      "expanded_url" : "http:\/\/twitter.com\/davearel\/status\/449301269627146240\/photo\/1",
      "indices" : [ 68, 90 ],
      "url" : "http:\/\/t.co\/vKS3oFI61Y",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Bjw9H8QCYAAZw_6.jpg",
      "id_str" : "449301269471977472",
      "id" : 449301269471977472,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Bjw9H8QCYAAZw_6.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 562,
        "resize" : "fit",
        "w" : 500
      }, {
        "h" : 382,
        "resize" : "fit",
        "w" : 340
      }, {
        "h" : 562,
        "resize" : "fit",
        "w" : 499
      }, {
        "h" : 562,
        "resize" : "fit",
        "w" : 499
      } ],
      "display_url" : "pic.twitter.com\/vKS3oFI61Y"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 45, 67 ],
      "url" : "http:\/\/t.co\/LHGKzNuXRr",
      "expanded_url" : "http:\/\/www.wired.com\/underwire\/2014\/03\/oculus-facebook-memes\/",
      "display_url" : "wired.com\/underwire\/2014\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "450399021333753856",
  "text" : "RT @davearel: A million dollars isn\u2019t cool.\n\nhttp:\/\/t.co\/LHGKzNuXRr http:\/\/t.co\/vKS3oFI61Y",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "http:\/\/twitter.com\/davearel\/status\/449301269627146240\/photo\/1",
        "indices" : [ 54, 76 ],
        "url" : "http:\/\/t.co\/vKS3oFI61Y",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Bjw9H8QCYAAZw_6.jpg",
        "id_str" : "449301269471977472",
        "id" : 449301269471977472,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Bjw9H8QCYAAZw_6.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 562,
          "resize" : "fit",
          "w" : 500
        }, {
          "h" : 382,
          "resize" : "fit",
          "w" : 340
        }, {
          "h" : 562,
          "resize" : "fit",
          "w" : 499
        }, {
          "h" : 562,
          "resize" : "fit",
          "w" : 499
        } ],
        "display_url" : "pic.twitter.com\/vKS3oFI61Y"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 31, 53 ],
        "url" : "http:\/\/t.co\/LHGKzNuXRr",
        "expanded_url" : "http:\/\/www.wired.com\/underwire\/2014\/03\/oculus-facebook-memes\/",
        "display_url" : "wired.com\/underwire\/2014\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "449301269627146240",
    "text" : "A million dollars isn\u2019t cool.\n\nhttp:\/\/t.co\/LHGKzNuXRr http:\/\/t.co\/vKS3oFI61Y",
    "id" : 449301269627146240,
    "created_at" : "2014-03-27 21:45:44 +0000",
    "user" : {
      "name" : "Dave Arel",
      "screen_name" : "davearel",
      "protected" : false,
      "id_str" : "460481487",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/449250902709239808\/pZt7v5Vs_normal.jpeg",
      "id" : 460481487,
      "verified" : false
    }
  },
  "id" : 450399021333753856,
  "created_at" : "2014-03-30 22:27:49 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Walsh",
      "screen_name" : "davidwalshblog",
      "indices" : [ 3, 18 ],
      "id_str" : "15759583",
      "id" : 15759583
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "450305871411965952",
  "text" : "RT @davidwalshblog: \"Your video will start after this 30 second advertisement.\u201D\n\nNo it won\u2019t, I\u2019ll be gone.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "450305275522600960",
    "text" : "\"Your video will start after this 30 second advertisement.\u201D\n\nNo it won\u2019t, I\u2019ll be gone.",
    "id" : 450305275522600960,
    "created_at" : "2014-03-30 16:15:18 +0000",
    "user" : {
      "name" : "David Walsh",
      "screen_name" : "davidwalshblog",
      "protected" : false,
      "id_str" : "15759583",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2587630901\/6gk0dqubt5512yk18a6o_normal.png",
      "id" : 15759583,
      "verified" : false
    }
  },
  "id" : 450305871411965952,
  "created_at" : "2014-03-30 16:17:40 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "449540939166781440",
  "text" : "Autocorrect just changed a missed 'get' into HDTV, as in, \"I'm gonna HDTV...\"",
  "id" : 449540939166781440,
  "created_at" : "2014-03-28 13:38:06 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "449508102673825792",
  "text" : "See this is a reasonable spring morning",
  "id" : 449508102673825792,
  "created_at" : "2014-03-28 11:27:37 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "449346752655921152",
  "text" : "Tonight's rails achievement was refactoring an active record query from 20s to 1.2s",
  "id" : 449346752655921152,
  "created_at" : "2014-03-28 00:46:28 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ernie Miller",
      "screen_name" : "erniemiller",
      "indices" : [ 3, 15 ],
      "id_str" : "16143891",
      "id" : 16143891
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "protip",
      "indices" : [ 22, 29 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "449156412313444352",
  "text" : "RT @erniemiller: Bash #protip: put this in \/etc\/bashrc. Your sysadmin will thank you.\n\nfunction cat() \u007B\n  echo \"=^.^=\"\n\u007D",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/itunes.apple.com\/us\/app\/twitter\/id409789998?mt=12\" rel=\"nofollow\"\u003ETwitter for Mac\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "protip",
        "indices" : [ 5, 12 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "448892002633924608",
    "text" : "Bash #protip: put this in \/etc\/bashrc. Your sysadmin will thank you.\n\nfunction cat() \u007B\n  echo \"=^.^=\"\n\u007D",
    "id" : 448892002633924608,
    "created_at" : "2014-03-26 18:39:28 +0000",
    "user" : {
      "name" : "Ernie Miller",
      "screen_name" : "erniemiller",
      "protected" : false,
      "id_str" : "16143891",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/460499533571428352\/EATOinkq_normal.png",
      "id" : 16143891,
      "verified" : false
    }
  },
  "id" : 449156412313444352,
  "created_at" : "2014-03-27 12:10:08 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Orca Clarkson",
      "screen_name" : "sparkyclarkson",
      "indices" : [ 3, 18 ],
      "id_str" : "18478184",
      "id" : 18478184
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "448589098849427456",
  "text" : "RT @sparkyclarkson: Now Facebook will completely cover your actual face.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "448576391551127553",
    "text" : "Now Facebook will completely cover your actual face.",
    "id" : 448576391551127553,
    "created_at" : "2014-03-25 21:45:20 +0000",
    "user" : {
      "name" : "Orca Clarkson",
      "screen_name" : "sparkyclarkson",
      "protected" : false,
      "id_str" : "18478184",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/524748029484470273\/Ei-XDqBR_normal.jpeg",
      "id" : 18478184,
      "verified" : false
    }
  },
  "id" : 448589098849427456,
  "created_at" : "2014-03-25 22:35:50 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ana Nelson",
      "screen_name" : "ananelson",
      "indices" : [ 3, 13 ],
      "id_str" : "7381872",
      "id" : 7381872
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "447369458542473217",
  "text" : "RT @ananelson: Deleting dead code, singing \"if you liked it then you should have put a test on it\".",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "445705093199826944",
    "text" : "Deleting dead code, singing \"if you liked it then you should have put a test on it\".",
    "id" : 445705093199826944,
    "created_at" : "2014-03-17 23:35:49 +0000",
    "user" : {
      "name" : "Ana Nelson",
      "screen_name" : "ananelson",
      "protected" : false,
      "id_str" : "7381872",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3109945232\/22fce18e5b61ea9516b173fc263236f3_normal.jpeg",
      "id" : 7381872,
      "verified" : false
    }
  },
  "id" : 447369458542473217,
  "created_at" : "2014-03-22 13:49:25 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "445751205332271104",
  "text" : "That moment where you change the JS to win that 2048 tile game.",
  "id" : 445751205332271104,
  "created_at" : "2014-03-18 02:39:03 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Elliott  Kember",
      "screen_name" : "elliottkember",
      "indices" : [ 3, 17 ],
      "id_str" : "903351",
      "id" : 903351
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 65 ],
      "url" : "http:\/\/t.co\/7sPOSNiD6d",
      "expanded_url" : "http:\/\/ov3y.github.io\/2048-AI\/",
      "display_url" : "ov3y.github.io\/2048-AI\/"
    } ]
  },
  "geo" : { },
  "id_str" : "444153624978268160",
  "text" : "RT @elliottkember: Don't get stuck playing http:\/\/t.co\/7sPOSNiD6d like I have",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/itunes.apple.com\/us\/app\/twitter\/id409789998?mt=12\" rel=\"nofollow\"\u003ETwitter for Mac\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 24, 46 ],
        "url" : "http:\/\/t.co\/7sPOSNiD6d",
        "expanded_url" : "http:\/\/ov3y.github.io\/2048-AI\/",
        "display_url" : "ov3y.github.io\/2048-AI\/"
      } ]
    },
    "geo" : { },
    "id_str" : "444151961970294785",
    "text" : "Don't get stuck playing http:\/\/t.co\/7sPOSNiD6d like I have",
    "id" : 444151961970294785,
    "created_at" : "2014-03-13 16:44:14 +0000",
    "user" : {
      "name" : "Elliott  Kember",
      "screen_name" : "elliottkember",
      "protected" : false,
      "id_str" : "903351",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/527343390195585024\/tBxCTKxd_normal.jpeg",
      "id" : 903351,
      "verified" : false
    }
  },
  "id" : 444153624978268160,
  "created_at" : "2014-03-13 16:50:50 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "http:\/\/twitter.com\/calflegal\/status\/444097582181019649\/photo\/1",
      "indices" : [ 103, 125 ],
      "url" : "http:\/\/t.co\/AcHrDtzZ6J",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/BinAZegCcAEiQER.png",
      "id_str" : "444097582189408257",
      "id" : 444097582189408257,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/BinAZegCcAEiQER.png",
      "sizes" : [ {
        "h" : 212,
        "resize" : "fit",
        "w" : 340
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 563,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 375,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 563,
        "resize" : "fit",
        "w" : 900
      } ],
      "display_url" : "pic.twitter.com\/AcHrDtzZ6J"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "444097582181019649",
  "text" : "Re: my d3 party: I made this heatmap of energy usage for a space. Look carefully for the tiny hexagons http:\/\/t.co\/AcHrDtzZ6J",
  "id" : 444097582181019649,
  "created_at" : "2014-03-13 13:08:09 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "444087703639179264",
  "text" : "Oh \"feels like 7\", how I've missed you.",
  "id" : 444087703639179264,
  "created_at" : "2014-03-13 12:28:53 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "443838118547185664",
  "text" : "partying super hard with d3.js",
  "id" : 443838118547185664,
  "created_at" : "2014-03-12 19:57:08 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003EiOS\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "brian del vecchio",
      "screen_name" : "Hybernaut",
      "indices" : [ 42, 52 ],
      "id_str" : "4678",
      "id" : 4678
    } ],
    "media" : [ {
      "expanded_url" : "http:\/\/twitter.com\/calflegal\/status\/441954860746178560\/photo\/1",
      "indices" : [ 53, 75 ],
      "url" : "http:\/\/t.co\/qae4RbvajN",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/BiIjmqaIMAAP5j3.jpg",
      "id_str" : "441954860561608704",
      "id" : 441954860561608704,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/BiIjmqaIMAAP5j3.jpg",
      "sizes" : [ {
        "h" : 453,
        "resize" : "fit",
        "w" : 340
      }, {
        "h" : 1136,
        "resize" : "fit",
        "w" : 852
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1136,
        "resize" : "fit",
        "w" : 852
      }, {
        "h" : 800,
        "resize" : "fit",
        "w" : 600
      } ],
      "display_url" : "pic.twitter.com\/qae4RbvajN"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "441954860746178560",
  "text" : "Moroccan scramble at five leaves Brooklyn @Hybernaut http:\/\/t.co\/qae4RbvajN",
  "id" : 441954860746178560,
  "created_at" : "2014-03-07 15:13:44 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "441907653795934208",
  "text" : "I never thought listening to somebody brush their teeth would remind me of the tuning of the string section",
  "id" : 441907653795934208,
  "created_at" : "2014-03-07 12:06:09 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "441907404331290624",
  "text" : "In particularly hard bathrooms, soniccare harmonics really come to life.",
  "id" : 441907404331290624,
  "created_at" : "2014-03-07 12:05:10 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "441802582810697728",
  "text" : "NYC baby I'm here",
  "id" : 441802582810697728,
  "created_at" : "2014-03-07 05:08:38 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "441704564367319041",
  "text" : "My growing but still limited rails experience causes an incredible love \/ hate relationship in my life.",
  "id" : 441704564367319041,
  "created_at" : "2014-03-06 22:39:09 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "http:\/\/twitter.com\/calflegal\/status\/440859357043900417\/photo\/1",
      "indices" : [ 62, 84 ],
      "url" : "http:\/\/t.co\/6HWNsienS9",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Bh4_P91CMAEYBmJ.jpg",
      "id_str" : "440859357056479233",
      "id" : 440859357056479233,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Bh4_P91CMAEYBmJ.jpg",
      "sizes" : [ {
        "h" : 192,
        "resize" : "fit",
        "w" : 340
      }, {
        "h" : 1158,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 339,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 579,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      } ],
      "display_url" : "pic.twitter.com\/6HWNsienS9"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "440859357043900417",
  "text" : "A co-worker took this picture on his way home from our office http:\/\/t.co\/6HWNsienS9",
  "id" : 440859357043900417,
  "created_at" : "2014-03-04 14:40:36 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dustin MacKay",
      "screen_name" : "Cali_Scuzz",
      "indices" : [ 0, 11 ],
      "id_str" : "87314130",
      "id" : 87314130
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "440843067500023808",
  "geo" : { },
  "id_str" : "440844634693328896",
  "in_reply_to_user_id" : 87314130,
  "text" : "@Cali_Scuzz that looks cool! You got a copy I can borrow??",
  "id" : 440844634693328896,
  "in_reply_to_status_id" : 440843067500023808,
  "created_at" : "2014-03-04 13:42:06 +0000",
  "in_reply_to_screen_name" : "Cali_Scuzz",
  "in_reply_to_user_id_str" : "87314130",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "440836982953238528",
  "text" : "I want a book about the history of US banking. Specifically, a great one.",
  "id" : 440836982953238528,
  "created_at" : "2014-03-04 13:11:41 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "440132065947054080",
  "text" : "I wish ios had a \"back\" action for app switcher. Triple clock home button to go back to what I was looking at?",
  "id" : 440132065947054080,
  "created_at" : "2014-03-02 14:30:36 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
} ]