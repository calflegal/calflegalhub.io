Grailbird.data.tweets_2013_08 = 
 [ {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "TestFlight",
      "screen_name" : "testflightapp",
      "indices" : [ 0, 14 ],
      "id_str" : "158117892",
      "id" : 158117892
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "366958697760563200",
  "geo" : { },
  "id_str" : "366958993287020545",
  "in_reply_to_user_id" : 158117892,
  "text" : "@testflightapp no. It was only when I had no apps or devices. I couldn't add my device because I'm running ios 7 beta on my phone.",
  "id" : 366958993287020545,
  "in_reply_to_status_id" : 366958697760563200,
  "created_at" : "2013-08-12 16:26:56 +0000",
  "in_reply_to_screen_name" : "testflightapp",
  "in_reply_to_user_id_str" : "158117892",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "TestFlight",
      "screen_name" : "testflightapp",
      "indices" : [ 0, 14 ],
      "id_str" : "158117892",
      "id" : 158117892
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "366634638204088320",
  "in_reply_to_user_id" : 158117892,
  "text" : "@testflightapp I was getting 500 server errors on dashboard before activating any devices \/ uploading any builds...",
  "id" : 366634638204088320,
  "created_at" : "2013-08-11 18:58:04 +0000",
  "in_reply_to_screen_name" : "testflightapp",
  "in_reply_to_user_id_str" : "158117892",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "TestFlight",
      "screen_name" : "testflightapp",
      "indices" : [ 4, 18 ],
      "id_str" : "158117892",
      "id" : 158117892
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "366634422470053889",
  "text" : "The @testflightapp is pretty awesome.",
  "id" : 366634422470053889,
  "created_at" : "2013-08-11 18:57:12 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "365796755570298882",
  "text" : "Anybody else notice the long touch required in iOS 7 beta to change twitter screens (UItabViews)?",
  "id" : 365796755570298882,
  "created_at" : "2013-08-09 11:28:37 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jane",
      "screen_name" : "janieflegal",
      "indices" : [ 0, 12 ],
      "id_str" : "197449091",
      "id" : 197449091
    }, {
      "name" : "H. Scott Flegal",
      "screen_name" : "hscottflegal",
      "indices" : [ 13, 26 ],
      "id_str" : "168505798",
      "id" : 168505798
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "362718105128026112",
  "geo" : { },
  "id_str" : "363683114578485249",
  "in_reply_to_user_id" : 197449091,
  "text" : "@janieflegal @hscottflegal oh it was cranking up mine too I pwned those n00bz",
  "id" : 363683114578485249,
  "in_reply_to_status_id" : 362718105128026112,
  "created_at" : "2013-08-03 15:29:46 +0000",
  "in_reply_to_screen_name" : "janieflegal",
  "in_reply_to_user_id_str" : "197449091",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
} ]