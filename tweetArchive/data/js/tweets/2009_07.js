Grailbird.data.tweets_2009_07 = 
 [ {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "2912609259",
  "text" : "Labels pitch music revolution: Interactive iTunes albums arrive in September http:\/\/is.gd\/1SV2T",
  "id" : 2912609259,
  "created_at" : "2009-07-29 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Artists House Music",
      "screen_name" : "artistshouse",
      "indices" : [ 0, 13 ],
      "id_str" : "16610491",
      "id" : 16610491
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "2912718537",
  "in_reply_to_user_id" : 16610491,
  "text" : "@artistshouse surely you've seen this: http:\/\/is.gd\/1SV2T",
  "id" : 2912718537,
  "created_at" : "2009-07-29 00:00:00 +0000",
  "in_reply_to_screen_name" : "artistshouse",
  "in_reply_to_user_id_str" : "16610491",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bob Lefsetz",
      "screen_name" : "Lefsetz",
      "indices" : [ 28, 36 ],
      "id_str" : "27702153",
      "id" : 27702153
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "2768617575",
  "text" : "Great newsletter today from @Lefsetz about apple's quick moves across markets, versus the music  industry's (big labels) dreadful crawl.",
  "id" : 2768617575,
  "created_at" : "2009-07-22 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chris Carlson",
      "screen_name" : "CLCmusic",
      "indices" : [ 0, 9 ],
      "id_str" : "15249194",
      "id" : 15249194
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "2772004853",
  "geo" : { },
  "id_str" : "2772062513",
  "in_reply_to_user_id" : 15249194,
  "text" : "@CLCmusic  the second - the color composition is so neat!",
  "id" : 2772062513,
  "in_reply_to_status_id" : 2772004853,
  "created_at" : "2009-07-22 00:00:00 +0000",
  "in_reply_to_screen_name" : "CLCmusic",
  "in_reply_to_user_id_str" : "15249194",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chris Carlson",
      "screen_name" : "CLCmusic",
      "indices" : [ 0, 9 ],
      "id_str" : "15249194",
      "id" : 15249194
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "2772140480",
  "geo" : { },
  "id_str" : "2780598806",
  "in_reply_to_user_id" : 15249194,
  "text" : "@CLCmusic very well. I really don't know much about video\/graphics, only what seems to appeal to me. I'll check 'em out again.",
  "id" : 2780598806,
  "in_reply_to_status_id" : 2772140480,
  "created_at" : "2009-07-22 00:00:00 +0000",
  "in_reply_to_screen_name" : "CLCmusic",
  "in_reply_to_user_id_str" : "15249194",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Artists House Music",
      "screen_name" : "artistshouse",
      "indices" : [ 3, 16 ],
      "id_str" : "16610491",
      "id" : 16610491
    }, {
      "name" : "Bandize",
      "screen_name" : "bandize",
      "indices" : [ 21, 29 ],
      "id_str" : "16135813",
      "id" : 16135813
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "2787588512",
  "text" : "RT @artistshouse: RT @bandize: Just wanted to let you guys know that after a year and a half, we went public today! http:\/\/bandize.com",
  "id" : 2787588512,
  "created_at" : "2009-07-22 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "2690039905",
  "text" : "bus back to boston. Lots to do this weekend.",
  "id" : 2690039905,
  "created_at" : "2009-07-17 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ryan Van Etten",
      "screen_name" : "ryanve",
      "indices" : [ 0, 7 ],
      "id_str" : "16042571",
      "id" : 16042571
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "FollowFriday",
      "indices" : [ 31, 44 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "2690069924",
  "in_reply_to_user_id" : 16042571,
  "text" : "@ryanve thanks so much for the #FollowFriday. Grabbing some of the other names from you to follow.",
  "id" : 2690069924,
  "created_at" : "2009-07-17 00:00:00 +0000",
  "in_reply_to_screen_name" : "ryanve",
  "in_reply_to_user_id_str" : "16042571",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bob Baker",
      "screen_name" : "MrBuzzFactor",
      "indices" : [ 3, 16 ],
      "id_str" : "2903111",
      "id" : 2903111
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "2690709851",
  "text" : "RT @MrBuzzFactor: Good Mashable post on how to make money giving stuff away for FREE http:\/\/bit.ly\/QO50w",
  "id" : 2690709851,
  "created_at" : "2009-07-17 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "2692506488",
  "text" : "holy shit I hate myspace. Can't wait for its demise. slow, clunky, unorthodox and stupid",
  "id" : 2692506488,
  "created_at" : "2009-07-17 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Artists House Music",
      "screen_name" : "artistshouse",
      "indices" : [ 0, 13 ],
      "id_str" : "16610491",
      "id" : 16610491
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "2600321839",
  "geo" : { },
  "id_str" : "2601683744",
  "in_reply_to_user_id" : 16610491,
  "text" : "@artistshouse that deal will ruin him; followers will lose respect as he becomes a promo machine.",
  "id" : 2601683744,
  "in_reply_to_status_id" : 2600321839,
  "created_at" : "2009-07-12 00:00:00 +0000",
  "in_reply_to_screen_name" : "artistshouse",
  "in_reply_to_user_id_str" : "16610491",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ryan Van Etten",
      "screen_name" : "ryanve",
      "indices" : [ 0, 7 ],
      "id_str" : "16042571",
      "id" : 16042571
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "2545233717",
  "geo" : { },
  "id_str" : "2549904082",
  "in_reply_to_user_id" : 16042571,
  "text" : "@ryanve  Hey there, thanks for the rt. Ha music production, web design, wilco -- we are too similar!",
  "id" : 2549904082,
  "in_reply_to_status_id" : 2545233717,
  "created_at" : "2009-07-09 00:00:00 +0000",
  "in_reply_to_screen_name" : "ryanve",
  "in_reply_to_user_id_str" : "16042571",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/soundcloud.com\" rel=\"nofollow\"\u003ESoundCloud\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "soundcloud",
      "indices" : [ 49, 60 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "2517734301",
  "text" : "New track: Just To Say http:\/\/bit.ly\/14ERO4\n via #soundcloud",
  "id" : 2517734301,
  "created_at" : "2009-07-07 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Artists House Music",
      "screen_name" : "artistshouse",
      "indices" : [ 0, 13 ],
      "id_str" : "16610491",
      "id" : 16610491
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "2516579778",
  "geo" : { },
  "id_str" : "2517998118",
  "in_reply_to_user_id" : 16610491,
  "text" : "@artistshouse  have you looked into this soundcloud buzz yet? Interested in your thoughts.",
  "id" : 2517998118,
  "in_reply_to_status_id" : 2516579778,
  "created_at" : "2009-07-07 00:00:00 +0000",
  "in_reply_to_screen_name" : "artistshouse",
  "in_reply_to_user_id_str" : "16610491",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "CD Baby Podcast",
      "screen_name" : "cdbaby_podcast",
      "indices" : [ 3, 18 ],
      "id_str" : "19782065",
      "id" : 19782065
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "2518026350",
  "text" : "RT @cdbaby_podcast: Are your fans telling their friends? If not, improve, don't promote. http:\/\/ow.ly\/gB1G",
  "id" : 2518026350,
  "created_at" : "2009-07-07 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "2518205986",
  "text" : "back to work",
  "id" : 2518205986,
  "created_at" : "2009-07-07 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
} ]