Grailbird.data.tweets_2012_07 = 
 [ {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "230294921783934977",
  "text" : "It's not surprising that we don't get that many great artists in this economy though. People are trying to afford lunch first.",
  "id" : 230294921783934977,
  "created_at" : "2012-07-31 13:32:42 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bob Lefsetz",
      "screen_name" : "Lefsetz",
      "indices" : [ 48, 56 ],
      "id_str" : "27702153",
      "id" : 27702153
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "230294120457981953",
  "text" : "Last night after laying in bed and reading some @Lefsetz letters, I wondered if music has been actually destroyed.",
  "id" : 230294120457981953,
  "created_at" : "2012-07-31 13:29:31 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "John Davis",
      "screen_name" : "johndavismusic",
      "indices" : [ 0, 15 ],
      "id_str" : "86152708",
      "id" : 86152708
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "228563460928184320",
  "geo" : { },
  "id_str" : "228581894407737344",
  "in_reply_to_user_id" : 86152708,
  "text" : "@johndavismusic that's because you fix it in the master!",
  "id" : 228581894407737344,
  "in_reply_to_status_id" : 228563460928184320,
  "created_at" : "2012-07-26 20:05:45 +0000",
  "in_reply_to_screen_name" : "johndavismusic",
  "in_reply_to_user_id_str" : "86152708",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "NBC News",
      "screen_name" : "NBCNews",
      "indices" : [ 3, 11 ],
      "id_str" : "14173315",
      "id" : 14173315
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 100, 120 ],
      "url" : "http:\/\/t.co\/S9nYHEId",
      "expanded_url" : "http:\/\/nbcnews.to\/N509sh",
      "display_url" : "nbcnews.to\/N509sh"
    } ]
  },
  "geo" : { },
  "id_str" : "228472405130113024",
  "text" : "RT @NBCNews: 'Rome alone': Boy, 11, slips through security, flies from UK to Italy without passport http:\/\/t.co\/S9nYHEId",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 87, 107 ],
        "url" : "http:\/\/t.co\/S9nYHEId",
        "expanded_url" : "http:\/\/nbcnews.to\/N509sh",
        "display_url" : "nbcnews.to\/N509sh"
      } ]
    },
    "geo" : { },
    "id_str" : "228471955630743552",
    "text" : "'Rome alone': Boy, 11, slips through security, flies from UK to Italy without passport http:\/\/t.co\/S9nYHEId",
    "id" : 228471955630743552,
    "created_at" : "2012-07-26 12:48:53 +0000",
    "user" : {
      "name" : "NBC News",
      "screen_name" : "NBCNews",
      "protected" : false,
      "id_str" : "14173315",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/459828346726010880\/CrshV_Oa_normal.png",
      "id" : 14173315,
      "verified" : true
    }
  },
  "id" : 228472405130113024,
  "created_at" : "2012-07-26 12:50:40 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eric Bolton",
      "screen_name" : "walkbolton",
      "indices" : [ 0, 11 ],
      "id_str" : "121598461",
      "id" : 121598461
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "228101233884422144",
  "geo" : { },
  "id_str" : "228104414064427009",
  "in_reply_to_user_id" : 121598461,
  "text" : "@walkbolton pretty rad",
  "id" : 228104414064427009,
  "in_reply_to_status_id" : 228101233884422144,
  "created_at" : "2012-07-25 12:28:24 +0000",
  "in_reply_to_screen_name" : "walkbolton",
  "in_reply_to_user_id_str" : "121598461",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Emily Chang",
      "screen_name" : "emilychangtv",
      "indices" : [ 3, 16 ],
      "id_str" : "74130577",
      "id" : 74130577
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "227892004149940225",
  "text" : "RT @emilychangtv: Tim Cook: iPhone sales more than doubled in China",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "227880968785117184",
    "text" : "Tim Cook: iPhone sales more than doubled in China",
    "id" : 227880968785117184,
    "created_at" : "2012-07-24 21:40:31 +0000",
    "user" : {
      "name" : "Emily Chang",
      "screen_name" : "emilychangtv",
      "protected" : false,
      "id_str" : "74130577",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1153501347\/emily-chang_normal.jpg",
      "id" : 74130577,
      "verified" : true
    }
  },
  "id" : 227892004149940225,
  "created_at" : "2012-07-24 22:24:22 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "TechCrunch",
      "screen_name" : "TechCrunch",
      "indices" : [ 3, 14 ],
      "id_str" : "816653",
      "id" : 816653
    }, {
      "name" : "Ingrid",
      "screen_name" : "ingridlunden",
      "indices" : [ 126, 139 ],
      "id_str" : "21662638",
      "id" : 21662638
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 102, 122 ],
      "url" : "http:\/\/t.co\/IJDCIm5A",
      "expanded_url" : "http:\/\/tcrn.ch\/N0lTp8",
      "display_url" : "tcrn.ch\/N0lTp8"
    } ]
  },
  "geo" : { },
  "id_str" : "227891972227088385",
  "text" : "RT @TechCrunch: CEO Cook On China In Q3: Apple Had $5.7B In Sales, Still Growing At 'Incredible' Rate http:\/\/t.co\/IJDCIm5A by @ingridlunden",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/vip.wordpress.com\/hosting\" rel=\"nofollow\"\u003EWordPress.com VIP\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Ingrid",
        "screen_name" : "ingridlunden",
        "indices" : [ 110, 123 ],
        "id_str" : "21662638",
        "id" : 21662638
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 86, 106 ],
        "url" : "http:\/\/t.co\/IJDCIm5A",
        "expanded_url" : "http:\/\/tcrn.ch\/N0lTp8",
        "display_url" : "tcrn.ch\/N0lTp8"
      } ]
    },
    "geo" : { },
    "id_str" : "227881546428854272",
    "text" : "CEO Cook On China In Q3: Apple Had $5.7B In Sales, Still Growing At 'Incredible' Rate http:\/\/t.co\/IJDCIm5A by @ingridlunden",
    "id" : 227881546428854272,
    "created_at" : "2012-07-24 21:42:49 +0000",
    "user" : {
      "name" : "TechCrunch",
      "screen_name" : "TechCrunch",
      "protected" : false,
      "id_str" : "816653",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
      "id" : 816653,
      "verified" : true
    }
  },
  "id" : 227891972227088385,
  "created_at" : "2012-07-24 22:24:14 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Engadget",
      "screen_name" : "engadget",
      "indices" : [ 3, 12 ],
      "id_str" : "14372486",
      "id" : 14372486
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "227891950102130690",
  "text" : "RT @engadget: Will you be upgrading to OS X 10.8 Mountain Lion tomorrow?",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/itunes.apple.com\/us\/app\/twitter\/id409789998?mt=12\" rel=\"nofollow\"\u003ETwitter for Mac\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "227889673786245120",
    "text" : "Will you be upgrading to OS X 10.8 Mountain Lion tomorrow?",
    "id" : 227889673786245120,
    "created_at" : "2012-07-24 22:15:06 +0000",
    "user" : {
      "name" : "Engadget",
      "screen_name" : "engadget",
      "protected" : false,
      "id_str" : "14372486",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/528440858517852160\/I1FWOb9q_normal.jpeg",
      "id" : 14372486,
      "verified" : true
    }
  },
  "id" : 227891950102130690,
  "created_at" : "2012-07-24 22:24:09 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Slate",
      "screen_name" : "Slate",
      "indices" : [ 16, 22 ],
      "id_str" : "15164565",
      "id" : 15164565
    }, {
      "name" : "Hacker News",
      "screen_name" : "newsycombinator",
      "indices" : [ 68, 84 ],
      "id_str" : "14335498",
      "id" : 14335498
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 97, 117 ],
      "url" : "http:\/\/t.co\/trTCdtMm",
      "expanded_url" : "http:\/\/j.mp\/tVYqGg",
      "display_url" : "j.mp\/tVYqGg"
    } ]
  },
  "geo" : { },
  "id_str" : "227106476102152192",
  "text" : "Hacker News and @slate should be embarrassed about this popularity \u201C@newsycombinator: Don't Vote http:\/\/t.co\/trTCdtMm\u201D",
  "id" : 227106476102152192,
  "created_at" : "2012-07-22 18:22:57 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kid Stuff",
      "screen_name" : "thomasjsays",
      "indices" : [ 3, 15 ],
      "id_str" : "257111774",
      "id" : 257111774
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "226099795947839488",
  "text" : "RT @thomasjsays: When singing r&amp;b in front of the mirror, it looks so much cooler when I have my shirt off.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "226099012086923266",
    "text" : "When singing r&amp;b in front of the mirror, it looks so much cooler when I have my shirt off.",
    "id" : 226099012086923266,
    "created_at" : "2012-07-19 23:39:39 +0000",
    "user" : {
      "name" : "Kid Stuff",
      "screen_name" : "thomasjsays",
      "protected" : false,
      "id_str" : "257111774",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1678384083\/6774_566319319739_35803363_33416883_1620436_n_normal.jpg",
      "id" : 257111774,
      "verified" : false
    }
  },
  "id" : 226099795947839488,
  "created_at" : "2012-07-19 23:42:46 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dave Walker",
      "screen_name" : "cinewalker",
      "indices" : [ 0, 11 ],
      "id_str" : "363397000",
      "id" : 363397000
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "226097142366863360",
  "geo" : { },
  "id_str" : "226099460344786945",
  "in_reply_to_user_id" : 363397000,
  "text" : "@cinewalker node is the hipster backend",
  "id" : 226099460344786945,
  "in_reply_to_status_id" : 226097142366863360,
  "created_at" : "2012-07-19 23:41:26 +0000",
  "in_reply_to_screen_name" : "cinewalker",
  "in_reply_to_user_id_str" : "363397000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dave Walker",
      "screen_name" : "cinewalker",
      "indices" : [ 0, 11 ],
      "id_str" : "363397000",
      "id" : 363397000
    }, {
      "name" : "Andy Maddocks",
      "screen_name" : "AndyMaddocks",
      "indices" : [ 12, 25 ],
      "id_str" : "44134250",
      "id" : 44134250
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "226032995788800000",
  "geo" : { },
  "id_str" : "226076525412376576",
  "in_reply_to_user_id" : 363397000,
  "text" : "@cinewalker @andymaddocks link ?",
  "id" : 226076525412376576,
  "in_reply_to_status_id" : 226032995788800000,
  "created_at" : "2012-07-19 22:10:18 +0000",
  "in_reply_to_screen_name" : "cinewalker",
  "in_reply_to_user_id_str" : "363397000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Parris Ballentine",
      "screen_name" : "ParrisBtine",
      "indices" : [ 0, 12 ],
      "id_str" : "90442463",
      "id" : 90442463
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "225918858433794048",
  "geo" : { },
  "id_str" : "225931293760839681",
  "in_reply_to_user_id" : 90442463,
  "text" : "@ParrisBtine had better mornings have you?",
  "id" : 225931293760839681,
  "in_reply_to_status_id" : 225918858433794048,
  "created_at" : "2012-07-19 12:33:12 +0000",
  "in_reply_to_screen_name" : "ParrisBtine",
  "in_reply_to_user_id_str" : "90442463",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 3, 11 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "225930772232683522",
  "text" : "RT @codepo8: So Mac App Store submissions now require a 1024x1024 app icon. I wonder what percentage of apps is smaller than their icon  ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/itunes.apple.com\/us\/app\/twitter\/id409789998?mt=12\" rel=\"nofollow\"\u003ETwitter for Mac\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "225896437286842369",
    "text" : "So Mac App Store submissions now require a 1024x1024 app icon. I wonder what percentage of apps is smaller than their icon in file size.",
    "id" : 225896437286842369,
    "created_at" : "2012-07-19 10:14:42 +0000",
    "user" : {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "protected" : false,
      "id_str" : "13567",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1666904408\/codepo8_normal.png",
      "id" : 13567,
      "verified" : false
    }
  },
  "id" : 225930772232683522,
  "created_at" : "2012-07-19 12:31:08 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "225656513010728960",
  "text" : "ProTip: don't have 40+ icons on your desktop. You look like a clown at cafes.",
  "id" : 225656513010728960,
  "created_at" : "2012-07-18 18:21:19 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Michelle Flegal",
      "screen_name" : "sheilapuorto",
      "indices" : [ 0, 13 ],
      "id_str" : "700111027",
      "id" : 700111027
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "225061147324321792",
  "geo" : { },
  "id_str" : "225067724248850435",
  "in_reply_to_user_id" : 700111027,
  "text" : "@sheilapuorto test success!",
  "id" : 225067724248850435,
  "in_reply_to_status_id" : 225061147324321792,
  "created_at" : "2012-07-17 03:21:41 +0000",
  "in_reply_to_screen_name" : "sheilapuorto",
  "in_reply_to_user_id_str" : "700111027",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "224332405203419138",
  "text" : "Granted my debugger experience is fairly limited, the Xcode debugger just rocked my world.",
  "id" : 224332405203419138,
  "created_at" : "2012-07-15 02:39:47 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Hacker News",
      "screen_name" : "HackerNewsYC",
      "indices" : [ 1, 14 ],
      "id_str" : "770729628",
      "id" : 770729628
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 30, 50 ],
      "url" : "http:\/\/t.co\/f7MDpFki",
      "expanded_url" : "http:\/\/goo.gl\/fb\/JmPSW",
      "display_url" : "goo.gl\/fb\/JmPSW"
    } ]
  },
  "geo" : { },
  "id_str" : "223749220866330625",
  "text" : "\u201C@HackerNewsYC: Nvidia Hacked http:\/\/t.co\/f7MDpFki\u201D",
  "id" : 223749220866330625,
  "created_at" : "2012-07-13 12:02:25 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "umair ",
      "screen_name" : "umairh",
      "indices" : [ 3, 10 ],
      "id_str" : "14321959",
      "id" : 14321959
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "223748590051405824",
  "text" : "RT @umairh: I just used the term YOLO, so someone please immediately order the drone strike.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "223747463171948545",
    "text" : "I just used the term YOLO, so someone please immediately order the drone strike.",
    "id" : 223747463171948545,
    "created_at" : "2012-07-13 11:55:26 +0000",
    "user" : {
      "name" : "umair ",
      "screen_name" : "umairh",
      "protected" : false,
      "id_str" : "14321959",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/427316127916494848\/idsuKJeo_normal.jpeg",
      "id" : 14321959,
      "verified" : true
    }
  },
  "id" : 223748590051405824,
  "created_at" : "2012-07-13 11:59:55 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "E-603",
      "screen_name" : "E603",
      "indices" : [ 0, 5 ],
      "id_str" : "65137195",
      "id" : 65137195
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "223662757826920448",
  "geo" : { },
  "id_str" : "223748210257170432",
  "in_reply_to_user_id" : 65137195,
  "text" : "@E603 and that's only as a percentage chart! I wish they shrunk the whole chart or sonething as the total revenue decreased.",
  "id" : 223748210257170432,
  "in_reply_to_status_id" : 223662757826920448,
  "created_at" : "2012-07-13 11:58:25 +0000",
  "in_reply_to_screen_name" : "E603",
  "in_reply_to_user_id_str" : "65137195",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lee Martin",
      "screen_name" : "leemartin",
      "indices" : [ 35, 45 ],
      "id_str" : "12920142",
      "id" : 12920142
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 108, 128 ],
      "url" : "http:\/\/t.co\/gwb6YDST",
      "expanded_url" : "http:\/\/bit.ly\/N4iQSF",
      "display_url" : "bit.ly\/N4iQSF"
    } ]
  },
  "geo" : { },
  "id_str" : "223614008140365824",
  "text" : "Yeah these rock they are so wrong \u201C@leemartin: A terribly uninformed post about graphic design on Gizmondo: http:\/\/t.co\/gwb6YDST ...\"",
  "id" : 223614008140365824,
  "created_at" : "2012-07-13 03:05:08 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "223576061575835648",
  "text" : "How in the world could yahoo have been storing passwords as plaintext?",
  "id" : 223576061575835648,
  "created_at" : "2012-07-13 00:34:21 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 25, 45 ],
      "url" : "http:\/\/t.co\/fTYyZH8f",
      "expanded_url" : "http:\/\/www.clickorlando.com\/-\/1637238\/15468364\/-\/pv2dhmz\/-\/index.html",
      "display_url" : "clickorlando.com\/-\/1637238\/1546\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "223518133795815424",
  "text" : "Yes! Misspelled tattoos! http:\/\/t.co\/fTYyZH8f",
  "id" : 223518133795815424,
  "created_at" : "2012-07-12 20:44:10 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "truethough",
      "indices" : [ 111, 122 ]
    }, {
      "text" : "hashTagLife",
      "indices" : [ 123, 135 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "223517017053999104",
  "text" : "Truthfully hashtags just gave the population the chance to add a legitimate booyah onto their boasts and such. #truethough #hashTagLife",
  "id" : 223517017053999104,
  "created_at" : "2012-07-12 20:39:44 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dave Walker",
      "screen_name" : "cinewalker",
      "indices" : [ 0, 11 ],
      "id_str" : "363397000",
      "id" : 363397000
    }, {
      "name" : "Daniel Eden",
      "screen_name" : "_dte",
      "indices" : [ 74, 79 ],
      "id_str" : "23082430",
      "id" : 23082430
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "223419139216244737",
  "in_reply_to_user_id" : 363397000,
  "text" : "@cinewalker  if you want to see just how much one man can love CSS follow @_dte",
  "id" : 223419139216244737,
  "created_at" : "2012-07-12 14:10:48 +0000",
  "in_reply_to_screen_name" : "cinewalker",
  "in_reply_to_user_id_str" : "363397000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 117, 137 ],
      "url" : "http:\/\/t.co\/v5Q1mwgs",
      "expanded_url" : "http:\/\/www.youtube.com\/watch?v=hQuwQjvoIpI",
      "display_url" : "youtube.com\/watch?v=hQuwQj\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "223070755519340544",
  "text" : "You all know I love KP. I believe she is better at ballads than the loud hits she is always putting out. Here's one: http:\/\/t.co\/v5Q1mwgs",
  "id" : 223070755519340544,
  "created_at" : "2012-07-11 15:06:27 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Wall Street Journal",
      "screen_name" : "WSJ",
      "indices" : [ 3, 7 ],
      "id_str" : "3108351",
      "id" : 3108351
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 139, 140 ],
      "url" : "http:\/\/t.co\/cQpGWPYS",
      "expanded_url" : "http:\/\/on.wsj.com\/O1v48S",
      "display_url" : "on.wsj.com\/O1v48S"
    } ]
  },
  "geo" : { },
  "id_str" : "222832271219105792",
  "text" : "RT @WSJ: Correction: Sitting is not more dangerous than smoking. But it's bad. More than 3 hrs\/day will cut life by 2 years. http:\/\/t.co ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 116, 136 ],
        "url" : "http:\/\/t.co\/cQpGWPYS",
        "expanded_url" : "http:\/\/on.wsj.com\/O1v48S",
        "display_url" : "on.wsj.com\/O1v48S"
      } ]
    },
    "geo" : { },
    "id_str" : "222817665713049600",
    "text" : "Correction: Sitting is not more dangerous than smoking. But it's bad. More than 3 hrs\/day will cut life by 2 years. http:\/\/t.co\/cQpGWPYS",
    "id" : 222817665713049600,
    "created_at" : "2012-07-10 22:20:45 +0000",
    "user" : {
      "name" : "Wall Street Journal",
      "screen_name" : "WSJ",
      "protected" : false,
      "id_str" : "3108351",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/480087047542038528\/KiBgRibZ_normal.jpeg",
      "id" : 3108351,
      "verified" : true
    }
  },
  "id" : 222832271219105792,
  "created_at" : "2012-07-10 23:18:48 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kid Stuff",
      "screen_name" : "thomasjsays",
      "indices" : [ 3, 15 ],
      "id_str" : "257111774",
      "id" : 257111774
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "222830514636206080",
  "text" : "RT @thomasjsays: Would anyone consider putting me in their top 8?",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "222828818879090689",
    "text" : "Would anyone consider putting me in their top 8?",
    "id" : 222828818879090689,
    "created_at" : "2012-07-10 23:05:05 +0000",
    "user" : {
      "name" : "Kid Stuff",
      "screen_name" : "thomasjsays",
      "protected" : false,
      "id_str" : "257111774",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1678384083\/6774_566319319739_35803363_33416883_1620436_n_normal.jpg",
      "id" : 257111774,
      "verified" : false
    }
  },
  "id" : 222830514636206080,
  "created_at" : "2012-07-10 23:11:49 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "john frankel",
      "screen_name" : "john_frankel",
      "indices" : [ 3, 16 ],
      "id_str" : "30322151",
      "id" : 30322151
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "221228393818107906",
  "text" : "RT @john_frankel: 39% of Americans 18\u201034 are presently living with their parents or other relatives. That is more than twice the traditi ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitterrific.com\" rel=\"nofollow\"\u003ETwitterrific\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "221200108220456960",
    "text" : "39% of Americans 18\u201034 are presently living with their parents or other relatives. That is more than twice the tradition range of 15\u201018%",
    "id" : 221200108220456960,
    "created_at" : "2012-07-06 11:13:10 +0000",
    "user" : {
      "name" : "john frankel",
      "screen_name" : "john_frankel",
      "protected" : false,
      "id_str" : "30322151",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/517293343756132352\/rag_KIXL_normal.jpeg",
      "id" : 30322151,
      "verified" : true
    }
  },
  "id" : 221228393818107906,
  "created_at" : "2012-07-06 13:05:33 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Nina Bean",
      "screen_name" : "Nina_bean11",
      "indices" : [ 0, 12 ],
      "id_str" : "381821052",
      "id" : 381821052
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "220951450191278082",
  "geo" : { },
  "id_str" : "221228136707276800",
  "in_reply_to_user_id" : 381821052,
  "text" : "@Nina_bean11 haha sure. No comm majors though!",
  "id" : 221228136707276800,
  "in_reply_to_status_id" : 220951450191278082,
  "created_at" : "2012-07-06 13:04:32 +0000",
  "in_reply_to_screen_name" : "Nina_bean11",
  "in_reply_to_user_id_str" : "381821052",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Maddy Trolan",
      "screen_name" : "maddy_tee",
      "indices" : [ 0, 10 ],
      "id_str" : "19353468",
      "id" : 19353468
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "221221676334055424",
  "geo" : { },
  "id_str" : "221227848676024320",
  "in_reply_to_user_id" : 19353468,
  "text" : "@maddy_tee Ay!",
  "id" : 221227848676024320,
  "in_reply_to_status_id" : 221221676334055424,
  "created_at" : "2012-07-06 13:03:23 +0000",
  "in_reply_to_screen_name" : "maddy_tee",
  "in_reply_to_user_id_str" : "19353468",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ryan Foote",
      "screen_name" : "footedesign",
      "indices" : [ 0, 12 ],
      "id_str" : "48887172",
      "id" : 48887172
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "220285775248490496",
  "geo" : { },
  "id_str" : "220583456487378945",
  "in_reply_to_user_id" : 48887172,
  "text" : "@footedesign who? the what?",
  "id" : 220583456487378945,
  "in_reply_to_status_id" : 220285775248490496,
  "created_at" : "2012-07-04 18:22:48 +0000",
  "in_reply_to_screen_name" : "footedesign",
  "in_reply_to_user_id_str" : "48887172",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eric Bolton",
      "screen_name" : "walkbolton",
      "indices" : [ 0, 11 ],
      "id_str" : "121598461",
      "id" : 121598461
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220583289612800000",
  "in_reply_to_user_id" : 121598461,
  "text" : "@walkbolton lets make some music dude.",
  "id" : 220583289612800000,
  "created_at" : "2012-07-04 18:22:09 +0000",
  "in_reply_to_screen_name" : "walkbolton",
  "in_reply_to_user_id_str" : "121598461",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220576070301253632",
  "text" : "I've never been ashamed of my Katy Perry one, so here's this: I prefer her slower quieter tracks (acoustic one that got away).",
  "id" : 220576070301253632,
  "created_at" : "2012-07-04 17:53:27 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220546123713101828",
  "text" : "Sometimes stackoverflow makes me sad.",
  "id" : 220546123713101828,
  "created_at" : "2012-07-04 15:54:28 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "MBTA",
      "indices" : [ 25, 30 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220314443010146305",
  "text" : "Also, to the unforgiving #MBTA driver who made me pay today when my monthly pass didn't work cause y'allz sys, u owe me an iced coffee. Ass.",
  "id" : 220314443010146305,
  "created_at" : "2012-07-04 00:33:51 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220313569135296514",
  "text" : "Dear man in \"all I do is win\" tee walking a shitty tiny rat dog on a pink leash,\n\nI think you are mistaken.",
  "id" : 220313569135296514,
  "created_at" : "2012-07-04 00:30:22 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "umair ",
      "screen_name" : "umairh",
      "indices" : [ 3, 10 ],
      "id_str" : "14321959",
      "id" : 14321959
    }, {
      "name" : "thoughtmanagement",
      "screen_name" : "thoughtmanorg",
      "indices" : [ 87, 101 ],
      "id_str" : "617051726",
      "id" : 617051726
    }, {
      "name" : "umair ",
      "screen_name" : "umairh",
      "indices" : [ 103, 110 ],
      "id_str" : "14321959",
      "id" : 14321959
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "fail",
      "indices" : [ 79, 84 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220242895419084800",
  "text" : "RT @umairh: No. I think the older generation of leaders has redefined the word #fail. \u201C@thoughtmanorg: @umairh you think the young are b ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "thoughtmanagement",
        "screen_name" : "thoughtmanorg",
        "indices" : [ 75, 89 ],
        "id_str" : "617051726",
        "id" : 617051726
      }, {
        "name" : "umair ",
        "screen_name" : "umairh",
        "indices" : [ 91, 98 ],
        "id_str" : "14321959",
        "id" : 14321959
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "fail",
        "indices" : [ 67, 72 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "220242668754702336",
    "text" : "No. I think the older generation of leaders has redefined the word #fail. \u201C@thoughtmanorg: @umairh you think the young are better managers?\"",
    "id" : 220242668754702336,
    "created_at" : "2012-07-03 19:48:38 +0000",
    "user" : {
      "name" : "umair ",
      "screen_name" : "umairh",
      "protected" : false,
      "id_str" : "14321959",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/427316127916494848\/idsuKJeo_normal.jpeg",
      "id" : 14321959,
      "verified" : true
    }
  },
  "id" : 220242895419084800,
  "created_at" : "2012-07-03 19:49:32 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220134181886377984",
  "text" : "I start every morning with a little R&amp;D in bed where I read all kinds of stuff I'm curious about. If I could just avoid distraction later..",
  "id" : 220134181886377984,
  "created_at" : "2012-07-03 12:37:33 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mbtaGM",
      "screen_name" : "mbtaGM",
      "indices" : [ 19, 26 ],
      "id_str" : "1875510546",
      "id" : 1875510546
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "OrientHeights",
      "indices" : [ 84, 98 ]
    }, {
      "text" : "mbta",
      "indices" : [ 121, 126 ]
    } ],
    "urls" : [ {
      "indices" : [ 100, 120 ],
      "url" : "http:\/\/t.co\/lC1wkynb",
      "expanded_url" : "http:\/\/bit.ly\/dm9W5B",
      "display_url" : "bit.ly\/dm9W5B"
    } ]
  },
  "geo" : { },
  "id_str" : "220127449818742784",
  "text" : "wait, blue line?? \u201C@mbtaGM: Blue Line: Experiencing delays due to signal problem at #OrientHeights. http:\/\/t.co\/lC1wkynb #mbta\u201D",
  "id" : 220127449818742784,
  "created_at" : "2012-07-03 12:10:48 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jane",
      "screen_name" : "janieflegal",
      "indices" : [ 0, 12 ],
      "id_str" : "197449091",
      "id" : 197449091
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "219894775845892096",
  "geo" : { },
  "id_str" : "219907540723580929",
  "in_reply_to_user_id" : 197449091,
  "text" : "@janieflegal good one!",
  "id" : 219907540723580929,
  "in_reply_to_status_id" : 219894775845892096,
  "created_at" : "2012-07-02 21:36:58 +0000",
  "in_reply_to_screen_name" : "janieflegal",
  "in_reply_to_user_id_str" : "197449091",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "219894398283030528",
  "text" : "German Shepards can be a little scary looking, but some them have incredibly beautiful coats.",
  "id" : 219894398283030528,
  "created_at" : "2012-07-02 20:44:44 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Leah Hughes",
      "screen_name" : "lleahhughes",
      "indices" : [ 62, 74 ],
      "id_str" : "40752359",
      "id" : 40752359
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "219801493698658304",
  "text" : "How much does a hipster weigh? An Instagram. Har Har Har. Via @lleahhughes",
  "id" : 219801493698658304,
  "created_at" : "2012-07-02 14:35:34 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
} ]