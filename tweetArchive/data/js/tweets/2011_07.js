Grailbird.data.tweets_2011_07 = 
 [ {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "96580372422332416",
  "text" : "Calculus and code. Two of my favorite things!",
  "id" : 96580372422332416,
  "created_at" : "2011-07-28 13:58:49 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kid Stuff",
      "screen_name" : "thomasjsays",
      "indices" : [ 3, 15 ],
      "id_str" : "257111774",
      "id" : 257111774
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "96580051365138434",
  "text" : "RT @thomasjsays: You know it's time to move out when you're mad because you're mom didn't make you a smoothie for you to wake up to befo ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "96574938173030400",
    "text" : "You know it's time to move out when you're mad because you're mom didn't make you a smoothie for you to wake up to before she left.",
    "id" : 96574938173030400,
    "created_at" : "2011-07-28 13:37:14 +0000",
    "user" : {
      "name" : "Kid Stuff",
      "screen_name" : "thomasjsays",
      "protected" : false,
      "id_str" : "257111774",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1678384083\/6774_566319319739_35803363_33416883_1620436_n_normal.jpg",
      "id" : 257111774,
      "verified" : false
    }
  },
  "id" : 96580051365138434,
  "created_at" : "2011-07-28 13:57:33 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Boston Globe",
      "screen_name" : "BostonGlobe",
      "indices" : [ 34, 46 ],
      "id_str" : "95431448",
      "id" : 95431448
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "96573988880384002",
  "text" : "Now that's what I'm talkin bout! \"@BostonGlobe: Today's front page: In high-tech, another kind of job crunch http:\/\/bo.st\/oyadEs\"",
  "id" : 96573988880384002,
  "created_at" : "2011-07-28 13:33:27 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "96572548468314112",
  "text" : "I slept too much last night and don't feel so good",
  "id" : 96572548468314112,
  "created_at" : "2011-07-28 13:27:44 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "CNBC",
      "screen_name" : "CNBC",
      "indices" : [ 3, 8 ],
      "id_str" : "20402945",
      "id" : 20402945
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "CNBC",
      "indices" : [ 47, 52 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "96571113882124288",
  "text" : "RT @CNBC: What's at stake? Credit Suisse tells #CNBC if US defaults, stocks fall 30%, GDP would shrink by 5% - read more here: http:\/\/bi ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "CNBC",
        "indices" : [ 37, 42 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "96552556024692736",
    "text" : "What's at stake? Credit Suisse tells #CNBC if US defaults, stocks fall 30%, GDP would shrink by 5% - read more here: http:\/\/bit.ly\/naUgCQ",
    "id" : 96552556024692736,
    "created_at" : "2011-07-28 12:08:17 +0000",
    "user" : {
      "name" : "CNBC",
      "screen_name" : "CNBC",
      "protected" : false,
      "id_str" : "20402945",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/464838721645191169\/m3To6mMS_normal.jpeg",
      "id" : 20402945,
      "verified" : true
    }
  },
  "id" : 96571113882124288,
  "created_at" : "2011-07-28 13:22:02 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alex Kenyon",
      "screen_name" : "alexkenyon",
      "indices" : [ 0, 11 ],
      "id_str" : "27738121",
      "id" : 27738121
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "96395004934750210",
  "geo" : { },
  "id_str" : "96410906895986689",
  "in_reply_to_user_id" : 27738121,
  "text" : "@alexkenyon make that cheddah dude!",
  "id" : 96410906895986689,
  "in_reply_to_status_id" : 96395004934750210,
  "created_at" : "2011-07-28 02:45:26 +0000",
  "in_reply_to_screen_name" : "alexkenyon",
  "in_reply_to_user_id_str" : "27738121",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Marc A. Paradise",
      "screen_name" : "MarcParadise",
      "indices" : [ 0, 13 ],
      "id_str" : "133822136",
      "id" : 133822136
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "95163161170280448",
  "geo" : { },
  "id_str" : "95164802988638208",
  "in_reply_to_user_id" : 113159490,
  "text" : "@marcparadise thanks for the tip my man! I didn't know",
  "id" : 95164802988638208,
  "in_reply_to_status_id" : 95163161170280448,
  "created_at" : "2011-07-24 16:13:51 +0000",
  "in_reply_to_screen_name" : "marcparadise88",
  "in_reply_to_user_id_str" : "113159490",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kid Stuff",
      "screen_name" : "thomasjsays",
      "indices" : [ 15, 27 ],
      "id_str" : "257111774",
      "id" : 257111774
    }, {
      "name" : "Calvin Flegal",
      "screen_name" : "calflegal",
      "indices" : [ 29, 39 ],
      "id_str" : "28146296",
      "id" : 28146296
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "95154179500294144",
  "text" : "sorry, brah RT @thomasjsays: @calflegal can't hangout because he has to \"clean my room and do some calculus\"",
  "id" : 95154179500294144,
  "created_at" : "2011-07-24 15:31:38 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Brad Feld",
      "screen_name" : "bfeld",
      "indices" : [ 0, 6 ],
      "id_str" : "3754891",
      "id" : 3754891
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "94509704016965632",
  "geo" : { },
  "id_str" : "95139043666432002",
  "in_reply_to_user_id" : 3754891,
  "text" : "@bfeld I'd love to get a list of your top 10 tech industry people to follow.",
  "id" : 95139043666432002,
  "in_reply_to_status_id" : 94509704016965632,
  "created_at" : "2011-07-24 14:31:30 +0000",
  "in_reply_to_screen_name" : "bfeld",
  "in_reply_to_user_id_str" : "3754891",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alyssa Lerch",
      "screen_name" : "Lisslerch",
      "indices" : [ 0, 10 ],
      "id_str" : "112585647",
      "id" : 112585647
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "94966534757285888",
  "geo" : { },
  "id_str" : "94967299911581696",
  "in_reply_to_user_id" : 112585647,
  "text" : "@Lisslerch ha!",
  "id" : 94967299911581696,
  "in_reply_to_status_id" : 94966534757285888,
  "created_at" : "2011-07-24 03:09:03 +0000",
  "in_reply_to_screen_name" : "Lisslerch",
  "in_reply_to_user_id_str" : "112585647",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "94967061385715713",
  "text" : "oops, typo in that last tweet.",
  "id" : 94967061385715713,
  "created_at" : "2011-07-24 03:08:06 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "94966955810897920",
  "text" : "This will warm your heart f you haven't seen it yet: http:\/\/is.gd\/tazFmd",
  "id" : 94966955810897920,
  "created_at" : "2011-07-24 03:07:41 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jacqui Grilli",
      "screen_name" : "JacquiGrilli",
      "indices" : [ 0, 13 ],
      "id_str" : "52609478",
      "id" : 52609478
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "toolate",
      "indices" : [ 19, 27 ]
    } ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "94631466906025985",
  "geo" : { },
  "id_str" : "94633072867295232",
  "in_reply_to_user_id" : 52609478,
  "text" : "@JacquiGrilli why? #toolate",
  "id" : 94633072867295232,
  "in_reply_to_status_id" : 94631466906025985,
  "created_at" : "2011-07-23 05:00:57 +0000",
  "in_reply_to_screen_name" : "JacquiGrilli",
  "in_reply_to_user_id_str" : "52609478",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Spotify",
      "screen_name" : "Spotify",
      "indices" : [ 0, 8 ],
      "id_str" : "17230018",
      "id" : 17230018
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "93863833566842880",
  "in_reply_to_user_id" : 17230018,
  "text" : "@Spotify is the best. It's not just music. It brings people together.",
  "id" : 93863833566842880,
  "created_at" : "2011-07-21 02:04:16 +0000",
  "in_reply_to_screen_name" : "Spotify",
  "in_reply_to_user_id_str" : "17230018",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mark Exit Goodchild",
      "screen_name" : "exit1200",
      "indices" : [ 0, 9 ],
      "id_str" : "17879511",
      "id" : 17879511
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "93521233831927808",
  "geo" : { },
  "id_str" : "93534120742891520",
  "in_reply_to_user_id" : 17879511,
  "text" : "@exit1200 chronosync aint bad",
  "id" : 93534120742891520,
  "in_reply_to_status_id" : 93521233831927808,
  "created_at" : "2011-07-20 04:14:06 +0000",
  "in_reply_to_screen_name" : "exit1200",
  "in_reply_to_user_id_str" : "17879511",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "93059024974651393",
  "text" : "Spotify rocks",
  "id" : 93059024974651393,
  "created_at" : "2011-07-18 20:46:15 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "TechCrunch",
      "screen_name" : "TechCrunch",
      "indices" : [ 3, 14 ],
      "id_str" : "816653",
      "id" : 816653
    }, {
      "name" : "Dr. Serkan Toto",
      "screen_name" : "serkantoto",
      "indices" : [ 86, 97 ],
      "id_str" : "10351072",
      "id" : 10351072
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "93023275537534976",
  "text" : "RT @TechCrunch: First Dark Knight Rises Trailer Hits The Web http:\/\/tcrn.ch\/pg7Cl8 by @serkantoto",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/vip.wordpress.com\/hosting\" rel=\"nofollow\"\u003EWordPress.com VIP\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Dr. Serkan Toto",
        "screen_name" : "serkantoto",
        "indices" : [ 70, 81 ],
        "id_str" : "10351072",
        "id" : 10351072
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "93005809314840577",
    "text" : "First Dark Knight Rises Trailer Hits The Web http:\/\/tcrn.ch\/pg7Cl8 by @serkantoto",
    "id" : 93005809314840577,
    "created_at" : "2011-07-18 17:14:47 +0000",
    "user" : {
      "name" : "TechCrunch",
      "screen_name" : "TechCrunch",
      "protected" : false,
      "id_str" : "816653",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/469171480832380928\/rkZR1jIh_normal.png",
      "id" : 816653,
      "verified" : true
    }
  },
  "id" : 93023275537534976,
  "created_at" : "2011-07-18 18:24:11 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kid Stuff",
      "screen_name" : "thomasjsays",
      "indices" : [ 13, 25 ],
      "id_str" : "257111774",
      "id" : 257111774
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "safetyfirst",
      "indices" : [ 26, 38 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "92295566012329984",
  "text" : "@LuckyJacque @thomasjsays #safetyfirst very funny. U iz whiter than me though",
  "id" : 92295566012329984,
  "created_at" : "2011-07-16 18:12:32 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kid Stuff",
      "screen_name" : "thomasjsays",
      "indices" : [ 29, 41 ],
      "id_str" : "257111774",
      "id" : 257111774
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "92293071164817408",
  "text" : "@LuckyJacque on the beach yo @thomasjsays",
  "id" : 92293071164817408,
  "created_at" : "2011-07-16 18:02:37 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "MA Gill",
      "screen_name" : "mag91944",
      "indices" : [ 0, 9 ],
      "id_str" : "159086370",
      "id" : 159086370
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "92226770350247936",
  "geo" : { },
  "id_str" : "92257308167503872",
  "in_reply_to_user_id" : 159086370,
  "text" : "@mag91944 yeahhhh",
  "id" : 92257308167503872,
  "in_reply_to_status_id" : 92226770350247936,
  "created_at" : "2011-07-16 15:40:30 +0000",
  "in_reply_to_screen_name" : "mag91944",
  "in_reply_to_user_id_str" : "159086370",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Wayne Twittaker",
      "screen_name" : "WayneTwittaker",
      "indices" : [ 0, 15 ],
      "id_str" : "2300470586",
      "id" : 2300470586
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "91907919142076416",
  "text" : "@WayneTwittaker However, it seems I'll be there full-time ish as of January.",
  "id" : 91907919142076416,
  "created_at" : "2011-07-15 16:32:10 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Wayne Twittaker",
      "screen_name" : "WayneTwittaker",
      "indices" : [ 0, 15 ],
      "id_str" : "2300470586",
      "id" : 2300470586
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "91907847541104640",
  "text" : "@WayneTwittaker I'll be stopping by next Thursday eve, but I'm not sure how long I'll be there for.",
  "id" : 91907847541104640,
  "created_at" : "2011-07-15 16:31:53 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Wayne Twittaker",
      "screen_name" : "WayneTwittaker",
      "indices" : [ 0, 15 ],
      "id_str" : "2300470586",
      "id" : 2300470586
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "91903485976645632",
  "text" : "@WayneTwittaker psh what kind of e-hype machine are you?",
  "id" : 91903485976645632,
  "created_at" : "2011-07-15 16:14:33 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Wayne Twittaker",
      "screen_name" : "WayneTwittaker",
      "indices" : [ 0, 15 ],
      "id_str" : "2300470586",
      "id" : 2300470586
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "91903148431650816",
  "text" : "@WayneTwittaker Invite me then dumy!",
  "id" : 91903148431650816,
  "created_at" : "2011-07-15 16:13:12 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "91630641090789376",
  "text" : "This weather is so beautiful it's killing me.",
  "id" : 91630641090789376,
  "created_at" : "2011-07-14 22:10:21 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Stu Krolikowski",
      "screen_name" : "Stuke_Nukem",
      "indices" : [ 0, 12 ],
      "id_str" : "1644423332",
      "id" : 1644423332
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "91541465087483904",
  "text" : "@Stuke_Nukem HA!",
  "id" : 91541465087483904,
  "created_at" : "2011-07-14 16:16:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "91493173460869120",
  "text" : "Anybody got a Spotify invite?",
  "id" : 91493173460869120,
  "created_at" : "2011-07-14 13:04:07 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "90063249780916224",
  "text" : "YO CAN I GET YOUR SCREENNAME?",
  "id" : 90063249780916224,
  "created_at" : "2011-07-10 14:22:06 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Josh Mosh",
      "screen_name" : "TeenageWebmail",
      "indices" : [ 0, 15 ],
      "id_str" : "57415206",
      "id" : 57415206
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "89488506484822016",
  "geo" : { },
  "id_str" : "89495752073687040",
  "in_reply_to_user_id" : 57415206,
  "text" : "@TeenageWebmail that's not my username bozo",
  "id" : 89495752073687040,
  "in_reply_to_status_id" : 89488506484822016,
  "created_at" : "2011-07-09 00:47:04 +0000",
  "in_reply_to_screen_name" : "TeenageWebmail",
  "in_reply_to_user_id_str" : "57415206",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Daniel Pipilas",
      "screen_name" : "dpipilas",
      "indices" : [ 0, 9 ],
      "id_str" : "243015782",
      "id" : 243015782
    }, {
      "name" : "Daniel Jay Kern",
      "screen_name" : "DanielJayKern",
      "indices" : [ 39, 53 ],
      "id_str" : "151645976",
      "id" : 151645976
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "89470405110276096",
  "geo" : { },
  "id_str" : "89471526876872704",
  "in_reply_to_user_id" : 243015782,
  "text" : "@dpipilas come by riverwalk you clowns @DanielJayKern",
  "id" : 89471526876872704,
  "in_reply_to_status_id" : 89470405110276096,
  "created_at" : "2011-07-08 23:10:48 +0000",
  "in_reply_to_screen_name" : "dpipilas",
  "in_reply_to_user_id_str" : "243015782",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "E-603",
      "screen_name" : "E603",
      "indices" : [ 33, 38 ],
      "id_str" : "65137195",
      "id" : 65137195
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "89461063782903810",
  "text" : "Settin' up the site for the new  @E603 record ya heardd",
  "id" : 89461063782903810,
  "created_at" : "2011-07-08 22:29:14 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ryan Foote",
      "screen_name" : "footedesign",
      "indices" : [ 0, 12 ],
      "id_str" : "48887172",
      "id" : 48887172
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "89438225365598208",
  "geo" : { },
  "id_str" : "89441536365957120",
  "in_reply_to_user_id" : 48887172,
  "text" : "@footedesign G+ duh. Yours is tight.",
  "id" : 89441536365957120,
  "in_reply_to_status_id" : 89438225365598208,
  "created_at" : "2011-07-08 21:11:38 +0000",
  "in_reply_to_screen_name" : "footedesign",
  "in_reply_to_user_id_str" : "48887172",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ryan Foote",
      "screen_name" : "footedesign",
      "indices" : [ 27, 39 ],
      "id_str" : "48887172",
      "id" : 48887172
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "89417039059484672",
  "text" : "Got into Google+ thanks to @footedesign !",
  "id" : 89417039059484672,
  "created_at" : "2011-07-08 19:34:18 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ryan Foote",
      "screen_name" : "footedesign",
      "indices" : [ 0, 12 ],
      "id_str" : "48887172",
      "id" : 48887172
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "89413299053150208",
  "geo" : { },
  "id_str" : "89416263293616128",
  "in_reply_to_user_id" : 48887172,
  "text" : "@footedesign man this layout is boring and unclear!",
  "id" : 89416263293616128,
  "in_reply_to_status_id" : 89413299053150208,
  "created_at" : "2011-07-08 19:31:13 +0000",
  "in_reply_to_screen_name" : "footedesign",
  "in_reply_to_user_id_str" : "48887172",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ryan Foote",
      "screen_name" : "footedesign",
      "indices" : [ 0, 12 ],
      "id_str" : "48887172",
      "id" : 48887172
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "89413299053150208",
  "geo" : { },
  "id_str" : "89413931088609280",
  "in_reply_to_user_id" : 48887172,
  "text" : "@footedesign u rooooooccccckk",
  "id" : 89413931088609280,
  "in_reply_to_status_id" : 89413299053150208,
  "created_at" : "2011-07-08 19:21:57 +0000",
  "in_reply_to_screen_name" : "footedesign",
  "in_reply_to_user_id_str" : "48887172",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ryan Foote",
      "screen_name" : "footedesign",
      "indices" : [ 0, 12 ],
      "id_str" : "48887172",
      "id" : 48887172
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "89352946457919488",
  "geo" : { },
  "id_str" : "89411997485113344",
  "in_reply_to_user_id" : 48887172,
  "text" : "@footedesign lemme in man!!!",
  "id" : 89411997485113344,
  "in_reply_to_status_id" : 89352946457919488,
  "created_at" : "2011-07-08 19:14:16 +0000",
  "in_reply_to_screen_name" : "footedesign",
  "in_reply_to_user_id_str" : "48887172",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "89132508934971392",
  "text" : "Boston rox from dis boat http:\/\/twitpic.com\/5mr0u2",
  "id" : 89132508934971392,
  "created_at" : "2011-07-08 00:43:40 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "89093902178594817",
  "text" : "Bout to get on a boat in boston to see Deer Tick. Should be fun.",
  "id" : 89093902178594817,
  "created_at" : "2011-07-07 22:10:16 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Engadget",
      "screen_name" : "engadget",
      "indices" : [ 3, 12 ],
      "id_str" : "14372486",
      "id" : 14372486
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "88980777475379201",
  "text" : "RT @engadget: App Store hits 15 billion downloads, $2.5 billion paid by Apple to developers http:\/\/engt.co\/pu7YPt",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitterfeed.com\" rel=\"nofollow\"\u003Etwitterfeed\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "88963470296498178",
    "text" : "App Store hits 15 billion downloads, $2.5 billion paid by Apple to developers http:\/\/engt.co\/pu7YPt",
    "id" : 88963470296498178,
    "created_at" : "2011-07-07 13:31:58 +0000",
    "user" : {
      "name" : "Engadget",
      "screen_name" : "engadget",
      "protected" : false,
      "id_str" : "14372486",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/528440858517852160\/I1FWOb9q_normal.jpeg",
      "id" : 14372486,
      "verified" : true
    }
  },
  "id" : 88980777475379201,
  "created_at" : "2011-07-07 14:40:45 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "88442665569497088",
  "text" : "Ps these tweets have been so boring gang let's step it up.",
  "id" : 88442665569497088,
  "created_at" : "2011-07-06 03:02:29 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "88441163769577472",
  "text" : "Dude I'm over pillows.",
  "id" : 88441163769577472,
  "created_at" : "2011-07-06 02:56:31 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ryan Foote",
      "screen_name" : "footedesign",
      "indices" : [ 0, 12 ],
      "id_str" : "48887172",
      "id" : 48887172
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "86489417828667393",
  "geo" : { },
  "id_str" : "88237851069779969",
  "in_reply_to_user_id" : 48887172,
  "text" : "@footedesign it was just too much man. We need to hang out!",
  "id" : 88237851069779969,
  "in_reply_to_status_id" : 86489417828667393,
  "created_at" : "2011-07-05 13:28:37 +0000",
  "in_reply_to_screen_name" : "footedesign",
  "in_reply_to_user_id_str" : "48887172",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "88237766185455616",
  "text" : "I miss you @LuckyJacque",
  "id" : 88237766185455616,
  "created_at" : "2011-07-05 13:28:17 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
} ]