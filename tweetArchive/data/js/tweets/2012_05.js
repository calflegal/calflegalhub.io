Grailbird.data.tweets_2012_05 = 
 [ {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "208320697427177472",
  "text" : "It's amazing how much I am affected by empty places. Can't get things done. I grew up in a buzzing house so I do my best work in company.",
  "id" : 208320697427177472,
  "created_at" : "2012-05-31 22:14:59 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 86, 106 ],
      "url" : "https:\/\/t.co\/ukyZZ7s",
      "expanded_url" : "https:\/\/www.destroyallsoftware.com\/talks\/wat",
      "display_url" : "destroyallsoftware.com\/talks\/wat"
    } ]
  },
  "geo" : { },
  "id_str" : "205866940365942787",
  "text" : "Even if you aren't into code so much, if you're technical at all you might like this  https:\/\/t.co\/ukyZZ7s",
  "id" : 205866940365942787,
  "created_at" : "2012-05-25 03:44:38 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "amarlo",
      "screen_name" : "amarlo2",
      "indices" : [ 0, 8 ],
      "id_str" : "1649119020",
      "id" : 1649119020
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "205844495475019776",
  "geo" : { },
  "id_str" : "205866410080088064",
  "in_reply_to_user_id" : 68614037,
  "text" : "@amarlo2 lock or Locke har har har",
  "id" : 205866410080088064,
  "in_reply_to_status_id" : 205844495475019776,
  "created_at" : "2012-05-25 03:42:31 +0000",
  "in_reply_to_screen_name" : "annalocke_",
  "in_reply_to_user_id_str" : "68614037",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Harvard Biz Review",
      "screen_name" : "HarvardBiz",
      "indices" : [ 11, 22 ],
      "id_str" : "14800270",
      "id" : 14800270
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 55, 74 ],
      "url" : "http:\/\/t.co\/EyxINj5",
      "expanded_url" : "http:\/\/s.hbr.org\/JOGzVK",
      "display_url" : "s.hbr.org\/JOGzVK"
    } ]
  },
  "geo" : { },
  "id_str" : "205266264044744705",
  "text" : "Very cool \u201C@HarvardBiz: This is your brain on Facebook http:\/\/t.co\/EyxINj5\u201D",
  "id" : 205266264044744705,
  "created_at" : "2012-05-23 11:57:45 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "205109043126206465",
  "text" : "What a great evening.",
  "id" : 205109043126206465,
  "created_at" : "2012-05-23 01:33:01 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Los Angeles Times",
      "screen_name" : "latimes",
      "indices" : [ 10, 18 ],
      "id_str" : "16664681",
      "id" : 16664681
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 57, 76 ],
      "url" : "http:\/\/t.co\/WnUq53m",
      "expanded_url" : "http:\/\/lat.ms\/Jmuy5s",
      "display_url" : "lat.ms\/Jmuy5s"
    } ]
  },
  "geo" : { },
  "id_str" : "205046623410204672",
  "text" : "Really?! \u201C@latimes: AmEx launches FarmVille rewards card http:\/\/t.co\/WnUq53m\u201D",
  "id" : 205046623410204672,
  "created_at" : "2012-05-22 21:24:59 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "203091705715621888",
  "text" : "Getting up early is great",
  "id" : 203091705715621888,
  "created_at" : "2012-05-17 11:56:50 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "202953457357950977",
  "text" : "I don't run for trains.",
  "id" : 202953457357950977,
  "created_at" : "2012-05-17 02:47:29 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "WIRED",
      "screen_name" : "WIRED",
      "indices" : [ 3, 9 ],
      "id_str" : "1344951",
      "id" : 1344951
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 60, 80 ],
      "url" : "http:\/\/t.co\/1ihccvOM",
      "expanded_url" : "http:\/\/bit.ly\/IRqCbv",
      "display_url" : "bit.ly\/IRqCbv"
    } ]
  },
  "geo" : { },
  "id_str" : "202026615226634240",
  "text" : "RT @wired: Jokes for Nerds: HTML9 Responsive Boilerstrap JS http:\/\/t.co\/1ihccvOM",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/bitly.com\/\" rel=\"nofollow\"\u003EBitly Composer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 49, 69 ],
        "url" : "http:\/\/t.co\/1ihccvOM",
        "expanded_url" : "http:\/\/bit.ly\/IRqCbv",
        "display_url" : "bit.ly\/IRqCbv"
      } ]
    },
    "geo" : { },
    "id_str" : "202020442217775104",
    "text" : "Jokes for Nerds: HTML9 Responsive Boilerstrap JS http:\/\/t.co\/1ihccvOM",
    "id" : 202020442217775104,
    "created_at" : "2012-05-14 13:00:01 +0000",
    "user" : {
      "name" : "WIRED",
      "screen_name" : "WIRED",
      "protected" : false,
      "id_str" : "1344951",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/378800000703426078\/f0e0491c473589ad484d976ca45d712b_normal.png",
      "id" : 1344951,
      "verified" : true
    }
  },
  "id" : 202026615226634240,
  "created_at" : "2012-05-14 13:24:33 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "200979319332421633",
  "text" : "The silver line really chiefs it.",
  "id" : 200979319332421633,
  "created_at" : "2012-05-11 16:02:58 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "200600480810942464",
  "text" : "if you are asking people to share something, you should spend that energy making it irresistible. We all share things that are truly great.",
  "id" : 200600480810942464,
  "created_at" : "2012-05-10 14:57:36 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "200323434222321666",
  "text" : "In other news kanye tweets a photo of some babe climbing out of a car with DeLorean-style doors.",
  "id" : 200323434222321666,
  "created_at" : "2012-05-09 20:36:43 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "198829965825945600",
  "text" : "All these graduates made me think that in some ways I really measure my life's tempo by the ebb and flow of freedom.",
  "id" : 198829965825945600,
  "created_at" : "2012-05-05 17:42:12 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Wayne Twittaker",
      "screen_name" : "WayneTwittaker",
      "indices" : [ 0, 15 ],
      "id_str" : "2300470586",
      "id" : 2300470586
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "198424850921037825",
  "text" : "@WayneTwittaker guy you're on fire today.",
  "id" : 198424850921037825,
  "created_at" : "2012-05-04 14:52:25 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "198408894459019265",
  "text" : "I've been striking out so much on not breaking my yolks. All I want is a runny yolk on my toast...",
  "id" : 198408894459019265,
  "created_at" : "2012-05-04 13:49:01 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "CNBC",
      "screen_name" : "CNBC",
      "indices" : [ 25, 30 ],
      "id_str" : "20402945",
      "id" : 20402945
    } ],
    "media" : [ {
      "expanded_url" : "http:\/\/twitter.com\/CNBC\/status\/198396350541479937\/photo\/1",
      "indices" : [ 93, 113 ],
      "url" : "http:\/\/t.co\/dYNogU3B",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/AsDYbf8CQAAbdLk.jpg",
      "id_str" : "198396350545674240",
      "id" : 198396350545674240,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/AsDYbf8CQAAbdLk.jpg",
      "sizes" : [ {
        "h" : 255,
        "resize" : "fit",
        "w" : 340
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 450,
        "resize" : "fit",
        "w" : 600
      } ],
      "display_url" : "pic.twitter.com\/dYNogU3B"
    } ],
    "hashtags" : [ {
      "text" : "not",
      "indices" : [ 19, 23 ]
    }, {
      "text" : "FacebookIPO",
      "indices" : [ 50, 62 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "198400604844994561",
  "text" : "OMG JPMorgan rules #not \u201C@CNBC: JPMorgan even has #FacebookIPO coffee sleeves! Check it out: http:\/\/t.co\/dYNogU3B\u201D",
  "id" : 198400604844994561,
  "created_at" : "2012-05-04 13:16:04 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "198119373238575104",
  "text" : "I love making things. Today, an emailing ruby script. Later, an empire.",
  "id" : 198119373238575104,
  "created_at" : "2012-05-03 18:38:34 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "CaliforniaDreams",
      "indices" : [ 69, 86 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "197812313192013824",
  "text" : "I have begun literally dreaming about California. Think it's a sign. #CaliforniaDreams",
  "id" : 197812313192013824,
  "created_at" : "2012-05-02 22:18:25 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "197325568998514688",
  "text" : "It's funny to watch SportsCenter anchors cite tweets from athletes. Grammar mistakes everywhere.",
  "id" : 197325568998514688,
  "created_at" : "2012-05-01 14:04:16 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "E-603",
      "screen_name" : "E603",
      "indices" : [ 113, 118 ],
      "id_str" : "65137195",
      "id" : 65137195
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "197323088101257216",
  "text" : "OJ is better in shot sized servings. Then again I've always been intimidated by large glasses of juice, just ask @E603",
  "id" : 197323088101257216,
  "created_at" : "2012-05-01 13:54:24 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "197320030239735808",
  "text" : "I need a suntan gang.",
  "id" : 197320030239735808,
  "created_at" : "2012-05-01 13:42:15 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
} ]