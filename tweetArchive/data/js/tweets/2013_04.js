Grailbird.data.tweets_2013_04 = 
 [ {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Elliott  Kember",
      "screen_name" : "elliottkember",
      "indices" : [ 36, 50 ],
      "id_str" : "903351",
      "id" : 903351
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "329232828757848064",
  "text" : "That goal is in the same context as @elliottkember 's mention.",
  "id" : 329232828757848064,
  "created_at" : "2013-04-30 13:56:37 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "329232692262608896",
  "text" : "Slightly narcissistic but important goal of mine: be mentioned in the nytimes.",
  "id" : 329232692262608896,
  "created_at" : "2013-04-30 13:56:05 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "328504654088515584",
  "text" : "I hate bow ties with the fire of a thousand suns.",
  "id" : 328504654088515584,
  "created_at" : "2013-04-28 13:43:07 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "John Scott",
      "screen_name" : "JhnScttMusic",
      "indices" : [ 1, 14 ],
      "id_str" : "372240954",
      "id" : 372240954
    }, {
      "name" : "Berklee in Valencia ",
      "screen_name" : "berkleevalencia",
      "indices" : [ 15, 31 ],
      "id_str" : "90898084",
      "id" : 90898084
    }, {
      "name" : "sonicbidspanos",
      "screen_name" : "sonicbidspanos",
      "indices" : [ 106, 121 ],
      "id_str" : "2542962122",
      "id" : 2542962122
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "327760353603760128",
  "geo" : { },
  "id_str" : "327776602618486784",
  "in_reply_to_user_id" : 372240954,
  "text" : ".@JhnScttMusic @berkleevalencia wrong panos Panay twitter. That one works at Microsoft, not the Sonicbids @SonicbidsPanos",
  "id" : 327776602618486784,
  "in_reply_to_status_id" : 327760353603760128,
  "created_at" : "2013-04-26 13:30:06 +0000",
  "in_reply_to_screen_name" : "JhnScttMusic",
  "in_reply_to_user_id_str" : "372240954",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jane",
      "screen_name" : "janieflegal",
      "indices" : [ 38, 50 ],
      "id_str" : "197449091",
      "id" : 197449091
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "327041445678686209",
  "text" : "Two weeks from tomorrow I'll be in DC @janieflegal",
  "id" : 327041445678686209,
  "created_at" : "2013-04-24 12:48:51 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "327031387339227136",
  "text" : "I don't care about the celtics this year, not at all.",
  "id" : 327031387339227136,
  "created_at" : "2013-04-24 12:08:53 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kate Breton",
      "screen_name" : "katebreton",
      "indices" : [ 0, 11 ],
      "id_str" : "29812449",
      "id" : 29812449
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "326882091264507904",
  "geo" : { },
  "id_str" : "326887664697614336",
  "in_reply_to_user_id" : 29812449,
  "text" : "@katebreton turned it on but it's about building them, not pouring champagne on each other. Not extreme.",
  "id" : 326887664697614336,
  "in_reply_to_status_id" : 326882091264507904,
  "created_at" : "2013-04-24 02:37:47 +0000",
  "in_reply_to_screen_name" : "katebreton",
  "in_reply_to_user_id_str" : "29812449",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "326716800794501120",
  "text" : "Variety is the spice of life and yes I like trail mix.",
  "id" : 326716800794501120,
  "created_at" : "2013-04-23 15:18:50 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sam Rose",
      "screen_name" : "samwhoo",
      "indices" : [ 3, 11 ],
      "id_str" : "69410725",
      "id" : 69410725
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "324872169396006912",
  "text" : "RT @samwhoo: How to make your day infinitely better \/ worse: $ yes \"swag\" | xargs say -v cello",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "324847198124777473",
    "text" : "How to make your day infinitely better \/ worse: $ yes \"swag\" | xargs say -v cello",
    "id" : 324847198124777473,
    "created_at" : "2013-04-18 11:29:42 +0000",
    "user" : {
      "name" : "Sam Rose",
      "screen_name" : "samwhoo",
      "protected" : false,
      "id_str" : "69410725",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/475215746708959233\/bsuwQZPh_normal.jpeg",
      "id" : 69410725,
      "verified" : false
    }
  },
  "id" : 324872169396006912,
  "created_at" : "2013-04-18 13:08:55 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "321675215631441921",
  "text" : "Gist.vim rox",
  "id" : 321675215631441921,
  "created_at" : "2013-04-09 17:25:22 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
} ]