Grailbird.data.tweets_2010_01 = 
 [ {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "8515817486",
  "text" : "All the marketing in the world doesn\u2019t amount to a hill of beans if it doesn\u2019t elicit a response from your audience. http:\/\/is.gd\/7vs5L",
  "id" : 8515817486,
  "created_at" : "2010-02-01 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "8355738341",
  "text" : "hot pockets are both bizarre and not very delicious.",
  "id" : 8355738341,
  "created_at" : "2010-01-29 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "E-603",
      "screen_name" : "E603",
      "indices" : [ 0, 5 ],
      "id_str" : "65137195",
      "id" : 65137195
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "8036159753",
  "geo" : { },
  "id_str" : "8048681356",
  "in_reply_to_user_id" : 65137195,
  "text" : "@E603  that's what's up.",
  "id" : 8048681356,
  "in_reply_to_status_id" : 8036159753,
  "created_at" : "2010-01-22 00:00:00 +0000",
  "in_reply_to_screen_name" : "E603",
  "in_reply_to_user_id_str" : "65137195",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "michael brauer",
      "screen_name" : "mhb850",
      "indices" : [ 0, 7 ],
      "id_str" : "20834353",
      "id" : 20834353
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "7967135858",
  "geo" : { },
  "id_str" : "7976446819",
  "in_reply_to_user_id" : 20834353,
  "text" : "@mhb850  I've reached the point of frustration in mixes with low end causing my 2mix compression to go too far.  Do you remember those days?",
  "id" : 7976446819,
  "in_reply_to_status_id" : 7967135858,
  "created_at" : "2010-01-20 00:00:00 +0000",
  "in_reply_to_screen_name" : "mhb850",
  "in_reply_to_user_id_str" : "20834353",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "7995476874",
  "text" : "new solo track released here: http:\/\/is.gd\/6Gnls I played just about everything, along with all production including mastering.",
  "id" : 7995476874,
  "created_at" : "2010-01-20 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Parris Ballentine",
      "screen_name" : "ParrisBtine",
      "indices" : [ 52, 64 ],
      "id_str" : "90442463",
      "id" : 90442463
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "7926089004",
  "text" : "Just watched a UNH frat bro drink baby formula with @ParrisBtine",
  "id" : 7926089004,
  "created_at" : "2010-01-19 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Patrick Marshall",
      "screen_name" : "P_Marsh",
      "indices" : [ 3, 11 ],
      "id_str" : "89308683",
      "id" : 89308683
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "boston4haiti",
      "indices" : [ 45, 58 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "7929922176",
  "text" : "RT @P_Marsh Ann Polaneczky is the boom diggy #boston4haiti",
  "id" : 7929922176,
  "created_at" : "2010-01-19 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "7964457063",
  "text" : "If you haven't read this yet, it's a must. OK Go's open letter about their EMI deal and state of the industry: http:\/\/is.gd\/6CNnj",
  "id" : 7964457063,
  "created_at" : "2010-01-19 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "scott vener",
      "screen_name" : "brokemogul",
      "indices" : [ 3, 14 ],
      "id_str" : "19543777",
      "id" : 19543777
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "7918922504",
  "text" : "RT @brokemogul: Amazing ...  This movie looks like its gonna be sick http:\/\/tweetphoto.com\/8972904",
  "id" : 7918922504,
  "created_at" : "2010-01-18 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kaleigh Pelland",
      "screen_name" : "KaleighIsabelle",
      "indices" : [ 0, 16 ],
      "id_str" : "80191117",
      "id" : 80191117
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "7832066186",
  "geo" : { },
  "id_str" : "7832325239",
  "in_reply_to_user_id" : 80191117,
  "text" : "@KaleighIsabelle you would",
  "id" : 7832325239,
  "in_reply_to_status_id" : 7832066186,
  "created_at" : "2010-01-16 00:00:00 +0000",
  "in_reply_to_screen_name" : "KaleighIsabelle",
  "in_reply_to_user_id_str" : "80191117",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "7777338066",
  "text" : "up  in the air is fantastic. moving.",
  "id" : 7777338066,
  "created_at" : "2010-01-15 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "scott vener",
      "screen_name" : "brokemogul",
      "indices" : [ 3, 14 ],
      "id_str" : "19543777",
      "id" : 19543777
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "7733768509",
  "text" : "RT @brokemogul Never rat on your lil brother... hahaha http:\/\/bit.ly\/7cvrXd",
  "id" : 7733768509,
  "created_at" : "2010-01-14 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "7755174114",
  "text" : "I listen way too loud but I gotta feel it!",
  "id" : 7755174114,
  "created_at" : "2010-01-14 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ted Paduck",
      "screen_name" : "tpaduck",
      "indices" : [ 0, 8 ],
      "id_str" : "31306517",
      "id" : 31306517
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "7544050131",
  "geo" : { },
  "id_str" : "7559895777",
  "in_reply_to_user_id" : 31306517,
  "text" : "@tpaduck it's as popular as PBR amongst hipsters. Unfortunate, but real. oh gaga.",
  "id" : 7559895777,
  "in_reply_to_status_id" : 7544050131,
  "created_at" : "2010-01-09 00:00:00 +0000",
  "in_reply_to_screen_name" : "tpaduck",
  "in_reply_to_user_id_str" : "31306517",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "talkmusic",
      "screen_name" : "talkmusic",
      "indices" : [ 3, 13 ],
      "id_str" : "16935195",
      "id" : 16935195
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "7521120034",
  "text" : "RT @talkmusic Lady Gaga onboard as creative director at Poloroid, looks to bring instant film back:  http:\/\/bit.ly\/8foMMx",
  "id" : 7521120034,
  "created_at" : "2010-01-08 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "7525551826",
  "text" : "@WayneTwitteker whaaaat",
  "id" : 7525551826,
  "created_at" : "2010-01-08 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "7533664153",
  "text" : "@WayneTwitteker I love the communication lines of today. that's insane.",
  "id" : 7533664153,
  "created_at" : "2010-01-08 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "scott vener",
      "screen_name" : "brokemogul",
      "indices" : [ 0, 11 ],
      "id_str" : "19543777",
      "id" : 19543777
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "7460627193",
  "geo" : { },
  "id_str" : "7461143274",
  "in_reply_to_user_id" : 19543777,
  "text" : "@brokemogul my heart will go on hands down",
  "id" : 7461143274,
  "in_reply_to_status_id" : 7460627193,
  "created_at" : "2010-01-07 00:00:00 +0000",
  "in_reply_to_screen_name" : "brokemogul",
  "in_reply_to_user_id_str" : "19543777",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "E-603",
      "screen_name" : "E603",
      "indices" : [ 0, 5 ],
      "id_str" : "65137195",
      "id" : 65137195
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "7477133524",
  "in_reply_to_user_id" : 65137195,
  "text" : "@E603 lucky 7s?",
  "id" : 7477133524,
  "created_at" : "2010-01-07 00:00:00 +0000",
  "in_reply_to_screen_name" : "E603",
  "in_reply_to_user_id_str" : "65137195",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Xander Singh",
      "screen_name" : "xandersingh",
      "indices" : [ 3, 15 ],
      "id_str" : "15441140",
      "id" : 15441140
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "7485178347",
  "text" : "RT @xandersingh whaaatttt!!! http:\/\/www.mellotron.com\/digital-mellotron.html",
  "id" : 7485178347,
  "created_at" : "2010-01-07 00:00:00 +0000",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Miranda Degnan",
      "screen_name" : "ay_dios_miranda",
      "indices" : [ 0, 16 ],
      "id_str" : "36298099",
      "id" : 36298099
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "7429686325",
  "geo" : { },
  "id_str" : "7430434810",
  "in_reply_to_user_id" : 36298099,
  "text" : "@ay_dios_miranda  snuggies are good for nothing.",
  "id" : 7430434810,
  "in_reply_to_status_id" : 7429686325,
  "created_at" : "2010-01-06 00:00:00 +0000",
  "in_reply_to_screen_name" : "ay_dios_miranda",
  "in_reply_to_user_id_str" : "36298099",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ted Paduck",
      "screen_name" : "tpaduck",
      "indices" : [ 0, 8 ],
      "id_str" : "31306517",
      "id" : 31306517
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "7290747085",
  "geo" : { },
  "id_str" : "7307725280",
  "in_reply_to_user_id" : 31306517,
  "text" : "@tpaduck that rocks",
  "id" : 7307725280,
  "in_reply_to_status_id" : 7290747085,
  "created_at" : "2010-01-02 00:00:00 +0000",
  "in_reply_to_screen_name" : "tpaduck",
  "in_reply_to_user_id_str" : "31306517",
  "user" : {
    "name" : "Calvin Flegal",
    "screen_name" : "calflegal",
    "protected" : false,
    "id_str" : "28146296",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531129692237991936\/YNlBLCwu_normal.jpeg",
    "id" : 28146296,
    "verified" : false
  }
} ]