"use strict";
/* jshint ignore:start */

/* jshint ignore:end */

define('calflegal/app', ['exports', 'ember', 'ember/resolver', 'ember/load-initializers', 'calflegal/config/environment'], function (exports, _ember, _emberResolver, _emberLoadInitializers, _calflegalConfigEnvironment) {

  var App = undefined;

  _ember['default'].MODEL_FACTORY_INJECTIONS = true;

  App = _ember['default'].Application.extend({
    modulePrefix: _calflegalConfigEnvironment['default'].modulePrefix,
    podModulePrefix: _calflegalConfigEnvironment['default'].podModulePrefix,
    Resolver: _emberResolver['default']
  });

  (0, _emberLoadInitializers['default'])(App, _calflegalConfigEnvironment['default'].modulePrefix);

  exports['default'] = App;
});
define('calflegal/components/app-version', ['exports', 'ember-cli-app-version/components/app-version', 'calflegal/config/environment'], function (exports, _emberCliAppVersionComponentsAppVersion, _calflegalConfigEnvironment) {

  var name = _calflegalConfigEnvironment['default'].APP.name;
  var version = _calflegalConfigEnvironment['default'].APP.version;

  exports['default'] = _emberCliAppVersionComponentsAppVersion['default'].extend({
    version: version,
    name: name
  });
});
define('calflegal/components/nav-box', ['exports', 'ember'], function (exports, _ember) {
  var navBoxComponent = _ember['default'].Component.extend({
    didInsertElement: function didInsertElement() {
      _ember['default'].$(document).ready(function () {
        _ember['default'].$('#js-centered-navigation-menu').removeClass("show");
        _ember['default'].$('#js-centered-navigation-mobile-menu').on('click', function (e) {
          e.preventDefault();
          _ember['default'].$('#js-centered-navigation-menu').slideToggle(function () {
            if (_ember['default'].$('#js-centered-navigation-menu').is(':hidden')) {
              _ember['default'].$('#js-centered-navigation-menu').removeAttr('style');
            }
          });
        });
      });
    }

  });
  exports['default'] = navBoxComponent;
});
define('calflegal/controllers/array', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Controller;
});
define('calflegal/controllers/object', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Controller;
});
define('calflegal/initializers/app-version', ['exports', 'ember-cli-app-version/initializer-factory', 'calflegal/config/environment'], function (exports, _emberCliAppVersionInitializerFactory, _calflegalConfigEnvironment) {
  exports['default'] = {
    name: 'App Version',
    initialize: (0, _emberCliAppVersionInitializerFactory['default'])(_calflegalConfigEnvironment['default'].APP.name, _calflegalConfigEnvironment['default'].APP.version)
  };
});
define('calflegal/initializers/export-application-global', ['exports', 'ember', 'calflegal/config/environment'], function (exports, _ember, _calflegalConfigEnvironment) {
  exports.initialize = initialize;

  function initialize() {
    var application = arguments[1] || arguments[0];
    if (_calflegalConfigEnvironment['default'].exportApplicationGlobal !== false) {
      var value = _calflegalConfigEnvironment['default'].exportApplicationGlobal;
      var globalName;

      if (typeof value === 'string') {
        globalName = value;
      } else {
        globalName = _ember['default'].String.classify(_calflegalConfigEnvironment['default'].modulePrefix);
      }

      if (!window[globalName]) {
        window[globalName] = application;

        application.reopen({
          willDestroy: function willDestroy() {
            this._super.apply(this, arguments);
            delete window[globalName];
          }
        });
      }
    }
  }

  exports['default'] = {
    name: 'export-application-global',

    initialize: initialize
  };
});
define('calflegal/router', ['exports', 'ember', 'calflegal/config/environment'], function (exports, _ember, _calflegalConfigEnvironment) {

  var Router = _ember['default'].Router.extend({
    location: _calflegalConfigEnvironment['default'].locationType
  });

  Router.map(function () {});

  exports['default'] = Router;
});
define("calflegal/templates/application", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "revision": "Ember@1.13.11",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 2,
            "column": 0
          }
        },
        "moduleName": "calflegal/templates/application.hbs"
      },
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
        dom.insertBoundary(fragment, 0);
        return morphs;
      },
      statements: [["content", "outlet", ["loc", [null, [1, 0], [1, 10]]]]],
      locals: [],
      templates: []
    };
  })());
});
define("calflegal/templates/components/nav-box", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "revision": "Ember@1.13.11",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 10,
            "column": 0
          }
        },
        "moduleName": "calflegal/templates/components/nav-box.hbs"
      },
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("header");
        dom.setAttribute(el1, "class", "centered-navigation");
        var el2 = dom.createTextNode("\n  ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2, "class", "centered-navigation-wrapper");
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("a");
        dom.setAttribute(el3, "href", "javascript:void(0)");
        dom.setAttribute(el3, "id", "js-centered-navigation-mobile-menu");
        dom.setAttribute(el3, "class", "centered-navigation-mobile-menu");
        var el4 = dom.createTextNode("MENU");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("ul");
        dom.setAttribute(el3, "id", "js-centered-navigation-menu");
        dom.setAttribute(el3, "class", "centered-navigation-menu show");
        var el4 = dom.createTextNode("\n      ");
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("li");
        dom.setAttribute(el4, "class", "nav-link");
        var el5 = dom.createElement("a");
        dom.setAttribute(el5, "href", "/blog");
        var el6 = dom.createTextNode("Blog");
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n      ");
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("li");
        dom.setAttribute(el4, "class", "nav-link");
        var el5 = dom.createElement("a");
        dom.setAttribute(el5, "href", "/");
        var el6 = dom.createTextNode("About");
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n    ");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n  ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes() {
        return [];
      },
      statements: [],
      locals: [],
      templates: []
    };
  })());
});
define("calflegal/templates/index", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "revision": "Ember@1.13.11",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 5,
            "column": 0
          }
        },
        "moduleName": "calflegal/templates/index.hbs"
      },
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("section");
        dom.setAttribute(el1, "id", "bio");
        var el2 = dom.createTextNode("I am a software developer and wannabe designer. I primarily work as an application developer, as I thoroughly enjoy being involved with user experience. I come from a background in music prior to seeking an education in software. Right now I am a software engineer at ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("a");
        dom.setAttribute(el2, "href", "http://www.digitallumens.com/");
        dom.setAttribute(el2, "target", "_blank");
        var el3 = dom.createTextNode("Digital Lumens");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode(", where I work on whatever is necessary, though I mostly write JavaScript (react native, d3, ember, etc). Though most of my work has been in the web environment, I'm excited about mobile, and I've rolled out ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("a");
        dom.setAttribute(el2, "href", "https://itunes.apple.com/us/app/tuneup-intonation-ear-training/id884607905?ls=1&mt=8");
        dom.setAttribute(el2, "target", "_blank");
        var el3 = dom.createTextNode("tuneUp");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode(", a native iOS app. On the music side of my life, a while ago I produced, engineered, and mixed the album ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("a");
        dom.setAttribute(el2, "href", "http://oldabrambrown.bandcamp.com/album/restless-ghosts");
        dom.setAttribute(el2, "target", "_blank");
        var el3 = dom.createTextNode("\"Restless Ghosts\"");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode(" for Old Abram Brown. I have played in various groups, and also make my own music. Music is a part of my identity, and an important part of everything that I make. Music taught me that everything worth doing requires a ton of practice and work, and that iteration is extremely important. Before moving into application development I made a number of wordpress sites including: ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("a");
        dom.setAttribute(el2, "href", "http://graumanfilms.com/");
        dom.setAttribute(el2, "target", "_blank");
        var el3 = dom.createTextNode("graumanfilms.com");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode(", ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("a");
        dom.setAttribute(el2, "href", "http://myoptic.net/");
        dom.setAttribute(el2, "target", "_blank");
        var el3 = dom.createTextNode("myoptic.net");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode(", ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("a");
        dom.setAttribute(el2, "href", "http://flegal.com/");
        dom.setAttribute(el2, "target", "_blank");
        var el3 = dom.createTextNode("flegal.com");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode(", ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("a");
        dom.setAttribute(el2, "href", "http://www.e-603.com/");
        dom.setAttribute(el2, "target", "_blank");
        var el3 = dom.createTextNode("e-603.com");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode(". I love software logic and the creative process; ultimately what satisfies me most is the creation of something \"tangible\" and useful.");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n\n\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
        dom.insertBoundary(fragment, 0);
        return morphs;
      },
      statements: [["content", "nav-box", ["loc", [null, [1, 0], [1, 11]]]]],
      locals: [],
      templates: []
    };
  })());
});
/* jshint ignore:start */

/* jshint ignore:end */

/* jshint ignore:start */

define('calflegal/config/environment', ['ember'], function(Ember) {
  var prefix = 'calflegal';
/* jshint ignore:start */

try {
  var metaName = prefix + '/config/environment';
  var rawConfig = Ember['default'].$('meta[name="' + metaName + '"]').attr('content');
  var config = JSON.parse(unescape(rawConfig));

  return { 'default': config };
}
catch(err) {
  throw new Error('Could not read config from meta tag with name "' + metaName + '".');
}

/* jshint ignore:end */

});

if (!runningTests) {
  require("calflegal/app")["default"].create({"name":"calflegal","version":"0.0.0+d6ed76e6"});
}

/* jshint ignore:end */
//# sourceMappingURL=calflegal.map